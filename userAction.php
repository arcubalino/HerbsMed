<?php
include 'DB.php';
$db = new DB();
$tblName = 'member';
if(isset($_POST['action_type']) && !empty($_POST['action_type'])){
    if($_POST['action_type'] == 'data'){
        $conditions['where'] = array('mem_id'=>$_POST['id']);
        $conditions['return_type'] = 'single'; 
	   $member = $db->getRows($tblName,$conditions);
        
		echo json_encode($member);
		
    }elseif($_POST['action_type'] == 'view'){  
	
		 $member = $db->getRows($tblName,array('order_by'=>'mem_id DESC'));
    
		if(!empty($member)){
            $count = 0;
            foreach($member as $member): $count++;
                echo '<tr>';
                echo '<td>'.$member['mem_username'].'</td>';
                echo '<td>'.$member['mem_fname'].'</td>';
                echo '<td>'.$member['mem_lname'].'</td>';
				echo '<td>'.$member['mem_add'].'</td>';
				echo '<td>'.$member['mem_age'].'</td>';
				echo '<td>'.$member['mem_contact'].'</td>';
				echo '<td>'.$member['mem_email'].'</td>';
								
                echo '<td><a href="javascript:void(0);" class="glyphicon glyphicon-edit" onclick="editUser(\''.$member['mem_id'].'\')"></a><a href="javascript:void(0);" class="glyphicon glyphicon-trash" onclick="return confirm(\'Are you sure to delete data?\')?userAction(\'delete\',\''.$member['mem_id'].'\'):false;"></a></td>';
                echo '</tr>';
            endforeach;
        }else{
            echo '<tr><td colspan="5">No Content(s) found......</td></tr>';
        }
    }elseif($_POST['action_type'] == 'add'){
        
		$userData = array(
            'mem_username' => $_POST['mem_username'],
            'mem_fname' => $_POST['mem_fname'],
            'mem_lname' => $_POST['mem_lname'],
			'mem_add' => $_POST['mem_add'],
			'mem_age' => $_POST['mem_age'],
			'mem_contact' => $_POST['mem_contact'],
			'mem_email' => $_POST['mem_email']
        );
        $insert = $db->insert($tblName,$userData);
       
	   echo $insert?'ok':'err';
    
	}elseif($_POST['action_type'] == 'edit'){
        if(!empty($_POST['id'])){
           
			$userData = array(
			'mem_username' => $_POST['mem_username'],
            'mem_fname' => $_POST['mem_fname'],
            'mem_lname' => $_POST['mem_lname'],
			'mem_add' => $_POST['mem_add'],
			'mem_age' => $_POST['mem_age'],
			'mem_contact' => $_POST['mem_contact'],
			'mem_email' => $_POST['mem_email']               
            
			);
            $condition = array('mem_id' => $_POST['id']);
            $update = $db->update($tblName,$userData,$condition);
            
			echo $update?'ok':'err';
        }
    }elseif($_POST['action_type'] == 'delete'){
        if(!empty($_POST['id'])){
            $condition = array('mem_id' => $_POST['id']);
            $delete = $db->delete($tblName,$condition);
            echo $delete?'ok':'err';
        }
    }
    exit;
}
?>