<?php
  require_once("web.config.php"); //global variables for database access
  include_once('session.php');
  require_once("head.php");
  $page="one";
  include("navigation.php");
?>


<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
	</head>
	<body>
	
 
  
    </div><br><br><br><br>

   <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <center><img src="images/h2.jpg" alt="malunggay" style="height: 450px"></center>
    </div>

    <div class="item">
      <center><img src="images/slide1.jpg" alt="product" style="height: 450px"></center>
    </div>

    <div class="item">
      <center><img src="images/h3.jpg" alt="guava" style="height: 450px"></center>
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<br>
<br>
  <section class="boxes_area">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="box" style="color: black;">
                            <h3>Health Education</h3>
                            <p>Encourage and promote public health education for a better understanding about Alternative Medicine.</p>
                            
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="box" style="color: black;">
                            <h3>Health Communication</h3>
                            <p>Communicate and consult Health Professionals all over the Philippines and keep updated of the latest Health Information.</p>
                          
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="box" style="color: black">
                            <h3>Health Products and Services</h3>
                            <p>Visit nearby Clinics across the Philippines and shop Alternative Medicines suited for your Health. </p>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>

  <!-- Footer -->
    

		<!-- jQuery -->
		<script src="js/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="assets/bootstrap/js/bootstrap.min.js"></script>
		<script src="js/jquery.min.js"></script>
	<!-- bootstrap -->
	   <script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<!-- wow -->
	   <script src="assets/js/main.js"></script>
	
	</body>
</html>
	
	<footer class="w3-container w3-black w3-center w3-margin-top">
  <center><p>© 2017 Herbsmed. All rights reserved</p></a></center>
</footer>
