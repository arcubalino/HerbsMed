<?php

include('dbConfig.php');
include("function_plant.php");

if(isset($_POST["plant_id"]))
{
	$image = get_image_name($_POST["plant_id"]);
	if($image != '')
	{
		unlink("upload/" . $image);
	}
	$statement = $dbh->prepare(
		"DELETE FROM plants WHERE plant_id = :id"
	);
	$result = $statement->execute(
		array(
			':id'	=>	$_POST["plant_id"]
		)
	);
	
	if(!empty($result))
	{
		echo 'Data Deleted';
	}
}



?>