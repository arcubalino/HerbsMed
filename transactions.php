<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
	require('bundle_credentials.php');   
	$p_action = $_POST['action'];
	$g_action = $_GET['action'];

	$sess_id = $_SESSION['sess_user_id'];
	$sess_name = $_SESSION['sess_username'];
	$sess_type = $_SESSION['sess_userrole'];
	$sess_email = $_SESSION['sess_useremail'];
	
	switch($p_action){
		case "cancel_my_suggest":
			$plant_id = $_POST['plant_id'];

			$find_plant = $db->prepare("SELECT plant_image,plant_id FROM plants WHERE plant_id=?");
			$find_plant->execute(array($plant_id));
			$row = $find_plant->fetch();

			$image = "plant/".$row['plant_image'];
			if(file_exists($image)){
				unlink($image);
			}

			$cmd = $db->prepare("DELETE FROM plants WHERE plant_id=?");
			$res = $cmd->execute(array($plant_id));
			result_callback($res);
		break;

		case "chart_per_gender":
			$cmd = $db->prepare("SELECT COUNT(id) AS count,gender FROM surveys GROUP BY gender");
			$cmd->execute();
			$rows = $cmd->fetchAll();
			encode_json($rows);
		break;

		case "chart_per_satisfaction":
			$cmd = $db->prepare("SELECT COUNT(id) AS count,satisfaction_rate FROM surveys GROUP BY satisfaction_rate");
			$cmd->execute();
			$rows = $cmd->fetchAll();
			encode_json($rows);
		break;

		case "chart_per_ailment":
			$cmd = $db->prepare("SELECT COUNT(id) AS count,ailments FROM surveys GROUP BY ailments");
			$cmd->execute();
			$rows = $cmd->fetchAll();
			encode_json($rows);
		break;

		case "chart_per_like":
			$cmd = $db->prepare("SELECT COUNT(ls.plant_id) AS count,ls.plant_id,
			ps.plant_name
			FROM likes AS ls 
			LEFT JOIN plants AS ps ON ps.plant_id=ls.plant_id
			GROUP BY ls.plant_id");
			$cmd->execute();
			$rows = $cmd->fetchAll();
			encode_json($rows);
		break;
		case "remove_ass":
			$ass_id = $_POST['ass_id'];
			$cmd = $db->prepare("DELETE FROM assessments WHERE id=?");
			$res = $cmd->execute(array($ass_id));

			result_callback($res);
		break;
		case "find_ass":
			$ass_id = $_POST['ass_id'];
			$cmd  = $db->prepare("SELECT * FROM assessments WHERE id=?");
			$cmd->execute(array($ass_id));
			$rows = $cmd->fetchAll();
			encode_json($rows);
		break;
		case "submit_ass":
			$ass_id = $_POST['ass_id'];
			$ass_title = $_POST['ass_title'];
			$related_plant = $_POST['related_plant'];
			$plant_name = $_POST['plant_name'];
			$related_tutorial = $_POST['related_tutorial'];
			$video_url = $_POST['video_url'];
			$ass_content = $_POST['ass_content'];

			if(empty($ass_id)){
				$cmd = $db->prepare("INSERT INTO assessments(`title`,`related_plant`,`plant_name`,`related_tutorial`,`tutorial`,`content`,`user_id`) VALUES(:ass_title,:related_plant,:plant_name,:related_tutorial,:video_url,:ass_content,:sess_id)");
				$res = $cmd->execute(array(':ass_title'=>$ass_title,':related_plant'=>$related_plant,':plant_name'=>$plant_name,':related_tutorial'=>$related_tutorial,':video_url'=>$video_url,':ass_content'=>$ass_content,':sess_id'=>$sess_id));
			}else{
				$cmd = $db->prepare("UPDATE assessments SET title=?,related_plant=?,plant_name=?,related_tutorial=?,tutorial=?,content=? WHERE id=?");
				$res = $cmd->execute(array($ass_title,$related_plant,$plant_name,$related_tutorial,$video_url,$ass_content,$ass_id));
			}
			result_callback($res);
		break;
		case "assessment_list":
			$cmd = $db->prepare("SELECT ast.id as ass_id,ast.title,ast.related_plant,ast.plant_name,ast.related_tutorial,ast.tutorial,ast.content,ast.user_id,ast.created_at,
				mr.mem_image,mr.mem_fname,mr.mem_lname,mr.mem_type,
				ps.plant_name as name_plant,ps.plant_image,
				CASE WHEN mr.mem_type = 'ordinary' THEN 'profile'
				WHEN mr.mem_type = 'health' THEN 'healthprof' END AS image_location,
				ts.title as tutorial_title,ts.video,ts.content as tutorial_content,
				CASE WHEN ast.user_id = $sess_id THEN 'edit' ELSE 'not_authorized' END ownership
				FROM assessments as ast
				LEFT JOIN plants as ps ON ps.plant_id=ast.related_plant
				LEFT JOIN tutorials as ts ON ts.id=ast.related_tutorial
				LEFT JOIN member as mr ON mr.mem_id=ast.user_id ORDER BY ast.id DESC");
			$cmd->execute();
			$rows = $cmd->fetchAll();
			encode_json($rows);
		break;
		case "related_products":
			$plant_id = $_POST['plant_id'];
			$cmd = $db->prepare("SELECT product_id,related_plant,product_image,product_name FROM product WHERE related_plant=?");
			$cmd->execute(array($plant_id));
			$rows = $cmd->fetchAll();
			encode_json($rows);
		break;
		case "admin_approve":
			if($sess_type == "admin"){
				$product_id = $_POST['product_id'];
				
				$cmd = $db->prepare("UPDATE product SET status=? WHERE product_id=?");
				$res = $cmd->execute(array(0,$product_id));
				if($res){
					$find = $db->prepare("SELECT user_id,product_id FROM product WHERE product_id=?");
					$find->execute(array($product_id));
					$row = $find->fetch();
					$user_id = $row['user_id'];
					
					$cmd_n = $db->prepare("INSERT INTO `notifications`(`receiver`,`sender`,`type`,`content`) VALUES(:receiver,:sender,:type,:content)");
					$cmd_n->execute(array(':receiver'=>$user_id,':sender'=>$sess_id,':type'=>'3',':content'=>"Your request for product promotion has been approved. Click <a href='profile.php?pass_param=products' style='color:green;font-weight:bold;'>here</a> to see"));
				}

				result_callback($res);
			}else{
				echo"error";
			}

		break;
		case "remove_product":
			$product_id = $_POST['product_id'];

			$find = $db->prepare("SELECT product_id,product_image FROM product WHERE product_id=?");
			$find->execute(array($product_id));
			$row = $find->fetch();
			$image = "images/products/".$row['product_image'];
			if(file_exists($image)){
				unlink($image);
			}

			$cmd = $db->prepare("DELETE FROM product WHERE product_id=?");
			$res = $cmd->execute(array($product_id));
			if(!$res){
				$cmd->errorInfo();
			}
			result_callback($res);

		break;
		case "submit_product":
			$product_id = $_POST['product_id'];
			$product_name = $_POST['product_name'];
			$plant_name = $_POST['plant_name'];
			$product_desc = $_POST['product_desc'];
			$related_plant = $_POST['related_plant'];
			$upload_status = $_POST['upload_status'];


			$find_admin = $db->prepare("SELECT mem_id,mem_type,status FROM member WHERE mem_type=? AND status=?");
			$find_admin->execute(array('admin',1));
			$rox = $find_admin->fetch();
			$admin_id = $rox['mem_id'];

			if($related_plant != 0){
				$plant_name = "";
			}

			if(!empty($product_id)){
				$find = $db->prepare("SELECT product_image,product_id FROM product WHERE product_id=?");
				$find->execute(array($product_id));
				$row = $find->fetch();
				$imagex = $row['product_image'];
				$image_loc = 'images/products/'.$imagex;
			}

			if($upload_status == "upload"){

				if(!empty($product_id) && file_exists($image_loc)){
					unlink($image_loc);
				}

				$session_id = date('ymdhis');
				$file=$_FILES['file_upload']['tmp_name'];	
				$name=$_FILES['file_upload']['name'];
				$split_point = '.';
				$stringpos = strrpos($name, $split_point, -1);
				$finalstr = substr($name,0,$stringpos);
				$FinalName="".$session_id."_".$finalstr."";

				$image= addslashes(@file_get_contents($_FILES['file_upload']['tmp_name']));
				$image_name= addslashes($_FILES['file_upload']['name']);
				$image_size= @getimagesize($_FILES['file_upload']['tmp_name']);
				$splitName = explode(".", $image_name); //split the file name by the dot
				$fileExt = end($splitName); //get the file extension
				$newFileName  = ucwords($FinalName.'.'.$fileExt); //join file name and ext.
				move_uploaded_file($_FILES["file_upload"]["tmp_name"],"images/products/".$newFileName);
		
				$avatar_location=$newFileName;	
			}else{
				if(!empty($product_id)){
					$avatar_location=$imagex;
				}else{
					$avatar_location="";
				}
			}

			if(empty($product_id)){//do insert
				$cmd = $db->prepare("INSERT INTO `product` (`product_name`,`product_desc`,`user_id`,`product_image`,`plant_name`,`related_plant`,`approved_by`) VALUES(:product_name,:product_desc,:user_id,:product_image,:plant_name,:related_plant,:approved_by)");
				
				$res = $cmd->execute(array(':product_name'=>$product_name,':product_desc'=>$product_desc,':user_id'=>$sess_id,':product_image'=>$avatar_location,':plant_name'=>$plant_name,':related_plant'=>$related_plant,':approved_by'=>$admin_id));
			}else{
				$cmd = $db->prepare("UPDATE product SET `product_name`=?,`product_desc`=?,`product_image`=?,`plant_name`=?,`related_plant`=? WHERE product_id=?");
				$res = $cmd->execute(array($product_name,$product_desc,$avatar_location,$plant_name,$related_plant,$product_id));
			}

			if($res && empty($product_id)){
				$cmd_n = $db->prepare("INSERT INTO `notifications`(`receiver`,`sender`,`type`,`content`) VALUES(:receiver,:sender,:type,:content)");
				$cmd_n->execute(array(':receiver'=>$admin_id,':sender'=>$sess_id,':type'=>'3',':content'=>"A user request for product promotion approval for you to review. Click <a href='javascript:void(0)' onclick='product_process(\"admin_list\",\"\")' style='color:green;font-weight:bold;'>here</a> to see"));
			}
			result_callback($res);
		break;
		case "related_plants":
			$cmd = $db->prepare("SELECT plant_id,plant_name FROM plants WHERE status=?");
			$cmd->execute(array(0));
			$rows = $cmd->fetchAll();
			encode_json($rows);
		break;

		case "approve_products":

			$sql = "SELECT pt.product_id,pt.product_name,pt.product_desc,pt.user_id,pt.product_image,pt.plant_name,pt.status,pt.related_plant,mr.mem_image,mr.mem_fname,mr.mem_lname,mr.mem_type,
				ps.plant_name as name_plant,ps.plant_image,
				CASE WHEN mr.mem_type = 'ordinary' THEN 'profile'
				WHEN mr.mem_type = 'health' THEN 'healthprof' END AS image_location
				FROM product as pt 
				LEFT JOIN member as mr ON mr.mem_id=pt.user_id
				LEFT JOIN plants as ps ON ps.plant_id=pt.related_plant
				WHERE pt.status=? AND pt.approved_by=? ORDER BY product_id DESC";

			$cmd = $db->prepare($sql);
			$cmd->execute(array(0,$sess_id));
			//print_r($cmd->errorInfo());
			$rows = $cmd->fetchAll();
			encode_json($rows);
		break;

		case "product_list":
			if($sess_type == "health"){
				$where = "pt.user_id=?";
				$params = $sess_id;
			}elseif($sess_type == "admin"){
				$params = 1;
				$where = "pt.status=?";
			}
			$sql = "SELECT pt.product_id,pt.product_name,pt.product_desc,pt.user_id,pt.product_image,pt.plant_name,pt.status,pt.related_plant,mr.mem_image,mr.mem_fname,mr.mem_lname,mr.mem_type,
				ps.plant_name as name_plant,ps.plant_image,
				CASE WHEN mr.mem_type = 'ordinary' THEN 'profile'
				WHEN mr.mem_type = 'health' THEN 'healthprof' END AS image_location
				FROM product as pt 
				LEFT JOIN member as mr ON mr.mem_id=pt.user_id
				LEFT JOIN plants as ps ON ps.plant_id=pt.related_plant
				WHERE $where ORDER BY product_id DESC";

			$cmd = $db->prepare($sql);
			$cmd->execute(array($params));
			//print_r($cmd->errorInfo());
			$rows = $cmd->fetchAll();
			encode_json($rows);
		break;
		case "clinic_outside":
			$clinic_type = $_POST['clinic_type'];

			$cmd = $db->prepare("SELECT clinic_name,clinic_address,clinic_desc,clinic_image,owner,clinic_type,clinic_id,
				CASE WHEN clinic_type = '0' THEN 'Alternative Medicine'
				WHEN clinic_type = '1' THEN 'Masssage Clinic'
				WHEN clinic_type = '2' THEN 'Reflex Clinic'
				WHEN clinic_type = '3' THEN 'Others' END as type
				FROM clinic WHERE clinic_type=?");
			$cmd->execute(array($clinic_type));
			$rows  = $cmd->fetchAll();
			$arr = array();

			foreach($rows as $r){
				$arr[] = $r;
			}

			echo json_encode($arr);
		break;
		case "update_tutorial":
			$tutorial_id = $_POST['tutorial_id'];
			$video = $_POST['video'];
			$title = $_POST['title'];
			$content = $_POST['content'];

			$cmd = $db->prepare("UPDATE tutorials SET `title`=?,`content`=?,`video`=? WHERE id=?");
			$res = $cmd->execute(array($title,$content,$video,$tutorial_id));
			result_callback($res);
		break;
		case "remove_tutorial":
			$tutorial_id = $_POST['tutorial_id'];
			$cmd = $db->prepare("DELETE FROM tutorials WHERE id=?");
			$res = $cmd->execute(array($tutorial_id));
			result_callback($res);
		break;
		case "tutorial_lists":
			$cmd = $db->prepare("SELECT ts.content,ts.video,ts.title,ts.created_by,ts.id as tutorial_id,ts.created_at,
					mr.mem_id,mr.mem_lname,mr.mem_fname,mr.mem_image,CASE WHEN mr.mem_type = 'ordinary' THEN 'profile'
					WHEN mr.mem_type = 'health' THEN 'healthprof' END AS image_location,
					CASE WHEN ts.created_by = $sess_id THEN 'edit' ELSE 'not_authorized' END ownership
					FROM tutorials as ts
					LEFT JOIN member as mr ON mr.mem_id=ts.created_by ORDER BY ts.id DESC");
			$cmd->execute();
			$rows = $cmd->fetchAll();
			encode_json($rows);
		break;
		case "create_tutorial":
			$video = $_POST['video'];
			$title = $_POST['title'];
			$content = $_POST['content'];

			$cmd = $db->prepare("INSERT INTO tutorials(`content`,`title`,`video`,`created_by`) VALUES(:content,:title,:video,:created_by)");
			$res = $cmd->execute(array(':content'=>$content,':title'=>$title,':video'=>$video,':created_by'=>$sess_id));

			if(!$res){
				$cmd->errorInfo();
			}
			result_callback($res);
		break;
		case "users_message":
			$user_id = $_POST['user_id'];
			$cmd = $db->prepare("SELECT conversation_id FROM messages WHERE receiver=? AND sender=? ORDER BY id LIMIT 1");
			$cmd->execute(array($sess_id,$user_id));
			$row = $cmd->fetch();
			$conversation_id = $row['conversation_id'];

			$find = $db->prepare("SELECT * FROM messages WHERE conversation_id=?");
			$find->execute(array($conversation_id));
			$rows = $find->fetchAll();
			encode_json($rows);
		break;
		case "message_contact_list":
			$cmd = $db->prepare("SELECT ms.id as message_id,ms.sender,ms.receiver,
					mr.mem_image,mem_type,mem_lname,mem_fname,mr.mem_id,
					CASE WHEN mr.mem_type = 'ordinary' THEN 'profile'
					WHEN mr.mem_type = 'health' THEN 'healthprof' END AS image_location 
					FROM messages AS ms
					LEFT JOIN member AS mr ON mr.mem_id=ms.sender 
					WHERE ms.receiver=? 
					GROUP BY ms.sender");

			$cmd->execute(array($sess_id));
			$rows = $cmd->fetchAll();

			encode_json($rows);
		break;
		case "open_message":
			$message_id = $_POST['message_id'];
			$cmd = $db->prepare("SELECT ms.content,ms.id,ms.receiver,ms.sender,ms.created_at,
			mr.mem_id,mr.mem_lname,mr.mem_fname,mr.mem_image,CASE WHEN mr.mem_type = 'ordinary' THEN 'profile'
			WHEN mr.mem_type = 'health' THEN 'healthprof' END as image_location FROM messages as ms LEFT JOIN member as mr ON mr.mem_id=ms.sender WHERE ms.id=?");
			$cmd->execute(array($message_id));
			$rows = $cmd->fetchAll();
			encode_json($rows);
		break;
		case "remove_notification":
			$notification_id = $_POST['notification_id'];
			$cmd = $db->prepare("DELETE FROM notifications WHERE id=?");
			$res = $cmd->execute(array($notification_id));
			result_callback($res);
		break;
		case "notification_list":

			$cmd_n = $db->prepare("UPDATE `notifications` SET `status`=? WHERE receiver=?");
			$cmd_n->execute(array(0,$sess_id));

			$cmd = $db->prepare("SELECT ns.sender,ns.receiver,ns.created_at,ns.status,ns.id as notification_id,ns.content,
			mr.mem_lname,mr.mem_fname,mr.mem_image,mr.mem_type,
			CASE WHEN mr.mem_type = 'ordinary' THEN 'profile'
			WHEN mr.mem_type='health' THEN 'healthprof'
			END as profile_location,
			CASE WHEN ns.type = '0' THEN 'Comment'
			WHEN ns.type = '1' THEN 'Newly Approved Plant'
			WHEN ns.type = '2' THEN 'Review from User'
			WHEN ns.type = '3' THEN 'Request of Promotional Product'
			WHEN ns.type = '4' THEN 'Message from Health Professional'
			WHEN ns.type = '5' THEN 'Approved Suggested Plant'
			WHEN ns.type = '6' THEN 'Message Notification' END as notification_type
			FROM notifications as ns LEFT JOIN member as mr ON mr.mem_id=ns.sender WHERE ns.receiver=? ORDER BY ns.id DESC");
			
			$cmd->execute(array($sess_id));
			$rows = $cmd->fetchAll();
			//print_r($cmd->errorInfo());
			encode_json($rows);
			// 0 - Comment (All) | 
			// 1 - Newly Approved Plants (Admin) | 
			// 2 - Review added from Ordinary (Health)
			// 3 - Request of Promotion product | 
			// 4 - Message from HP (Ordinary) | 
			// 5 - If Suggested plant has been approved (Ordinary) | 
			// 6 - Message from Ordinary (Health) | 
		break;
		case "check_notifications":
				$sql = "SELECT COUNT(id) FROM notifications WHERE receiver=? AND status = ?";
				$cmd = $db->prepare($sql);
				$cmd->execute(array($sess_id,1));
				$num = $cmd->fetchColumn();
				echo $num;
		break;
		case "update_review":
			$review_id = $_POST['review_id'];
			$content = $_POST['content'];

			$cmd = $db->prepare("UPDATE reviews SET `content`=? WHERE id=?");
			$res = $cmd->execute(array($content,$review_id));
			result_callback($res);
		break;
		case "remove_review":
			$id = $_POST['review_id'];
			$cmd = $db->prepare("DELETE FROM reviews WHERE id=?");
			$res = $cmd->execute(array($id));
			result_callback($res);
		break;
		case "health_profile":
			$user_id = $_POST['user_id'];
			$cmd = $db->prepare("SELECT mem_id,mem_image,mem_fname,mem_lname,mem_add,mem_email,mem_contact FROM member WHERE mem_id=?");
			$cmd->execute(array($user_id));
			$rows = $cmd->fetchAll();

			encode_json($rows);

		break;
		case "create_review":
			$prof_id = $_POST['user_id'];
			$content = $_POST['content'];

			$cmd = $db->prepare("INSERT INTO reviews(`content`,`reviewer`,`professional`) VALUES(:content,:reviewer,:professional)");
			$res = $cmd->execute(array(':content'=>$content,':reviewer'=>$sess_id,':professional'=>$prof_id));

			if($res){//create notification
				$cmd_n = $db->prepare("INSERT INTO `notifications`(`receiver`,`sender`,`type`,`content`) VALUES(:receiver,:sender,:type,:content)");
				$cmd_n->execute(array(':receiver'=>$prof_id,':sender'=>$sess_id,':type'=>'2',':content'=>"A user added a review of you. Click <a href='profile.php?pass_param=reviews' style='color:green;font-weight:bold;'>here</a> to see"));
			}

			result_callback($res);

		break;
		case "review_list":
			$user_id = $_POST['user_id'];
			$sql = "SELECT rs.id as review_id,mr.mem_image,mr.mem_fname,mr.mem_lname,mr.mem_id,rs.reviewer,rs.professional,rs.content,rs.created_at FROM reviews AS rs LEFT JOIN member AS mr ON mr.mem_id=rs.reviewer WHERE rs.professional=? ORDER BY rs.id DESC";

			$cmd = $db->prepare($sql);
			$cmd->execute(array($user_id));
			$rows = $cmd->fetchAll();
			$arr = array();
			foreach($rows as $r){
				$arr[] = $r;
			}

			echo json_encode($arr);
		break;

		case "update_account":
			$email = $_POST['email'];
			$username = $_POST['username'];
			$password = md5($_POST['password']);

			$cmd = $db->prepare("UPDATE member SET `mem_username`=?,`mem_email`=?,`mem_pass`=? WHERE mem_id=?");
			$res = $cmd->execute(array($username,$email,$password,$sess_id));
			if($res){
				$_SESSION['sess_username'] = $username;
				$_SESSION['sess_useremail'] = $email;
			}else{
				$cmd->errorInfo();
			}

			result_callback($res);

		break;

		case "check_my_email":
			$email = $_POST['email'];
			if($email == $sess_email){
				echo"available";
			}else{
				$sql = "SELECT COUNT(mem_email) FROM member WHERE mem_email=?";
				$cmd = $db->prepare($sql);
				$cmd->execute(array($email));
				$num = $cmd->fetchColumn();

				if($num > 0){
					echo"exist";
				}else{
					echo"available";
				}
			}

		break; 

		case "check_my_username":
			$username = $_POST['username'];
			if($username == $sess_name){
				echo"available";
			}else{
				$sql = "SELECT COUNT(mem_username) FROM member WHERE mem_username=?";
				$cmd = $db->prepare($sql);
				$cmd->execute(array($username));
				$num = $cmd->fetchColumn();

				if($num > 0){
					echo"exist";
				}else{
					echo"available";
				}
			}


		break;
		case "update_profile":
			$fname = $_POST['fname'];
			$lname = $_POST['lname'];
			$address = $_POST['address'];
			$gender = $_POST['gender'];
			$contact = $_POST['contact'];
			$upload_status = $_POST['upload_status'];

			if($mem_type == "health"){
			$mem_location = "healthprof";
			}else{
				$mem_location = "profile";
			}
			
				$cmdx = $db->prepare("SELECT mem_id,mem_image FROM member WHERE mem_id=?");
				$cmdx->execute(array($sess_id));
				$rowx = $cmdx->fetch();
				$mem_imagex = $rowx['mem_image'];
				

				if($sess_type == "ordinary"){
					$mem_locationx = "profile";
				}elseif($sess_type == "health"){
					$mem_locationx = "healthprof";
				}

				if($upload_status == "upload"){
					if(file_exists('images/'.$mem_locationx.'/'.$mem_imagex)){
						unlink('images/'.$mem_locationx.'/'.$mem_imagex);
					}
				}

			if($upload_status == "upload"){

				$session_id = date('ymdhis');
				$file=$_FILES['file_upload']['tmp_name'];	
				$name=$_FILES['file_upload']['name'];
				$split_point = '.';
				$stringpos = strrpos($name, $split_point, -1);
				$finalstr = substr($name,0,$stringpos);
				$FinalName="".$session_id."_".$finalstr."";

				$image= addslashes(@file_get_contents($_FILES['file_upload']['tmp_name']));
				$image_name= addslashes($_FILES['file_upload']['name']);
				$image_size= @getimagesize($_FILES['file_upload']['tmp_name']);
				$splitName = explode(".", $image_name); //split the file name by the dot
				$fileExt = end($splitName); //get the file extension
				$newFileName  = ucwords($FinalName.'.'.$fileExt); //join file name and ext.
				move_uploaded_file($_FILES["file_upload"]["tmp_name"],"images/".$mem_location."/".$newFileName);
		
				$avatar_location=$newFileName;	
			}else{
				$avatar_location=$mem_imagex;
			}

			$cmd = $db->prepare("UPDATE member SET `mem_fname`=?,`mem_lname`=?,`mem_add`=?,`mem_contact`=?,`mem_gender`=?,`mem_image`=? WHERE mem_id=?");
			$res = $cmd->execute(array($fname,$lname,$address,$contact,$gender,$avatar_location,$sess_id));
			if(!$res){
				print_r($cmd->errorInfo());
			}else{
				result_callback($res);
			}
		break;
		case "approve_pending_prof":
			$user_id = $_POST['user_id'];
			$cmd = $db->prepare("UPDATE member SET `status`=? WHERE mem_id=?");
			$res = $cmd->execute(array(1,$user_id));
			result_callback($res);
		break;
		case "deny_pending_prof":
			$user_id = $_POST['user_id'];
			$cmd = $db->prepare("UPDATE member SET `status`=? WHERE mem_id=?");
			$res = $cmd->execute(array(3,$user_id));
			result_callback($res);
		break;
		case "pending_prof":
			$cmd = $db->prepare('SELECT mem_id,mem_fname,mem_lname,mem_image FROM member WHERE status=? AND mem_type=?');
			$cmd->execute(array('2','health'));
			$rows = $cmd->fetchAll();
			$arr = array();

			foreach($rows as $r){
				$arr[] = $r;
			}

			echo json_encode($arr);
		break;

		case "create_unlike":

			$plant_id = $_POST['plant_id'];
			$cmd = $db->prepare("DELETE FROM likes WHERE plant_id=? AND user_id=?");
			$res = $cmd->execute(array($plant_id,$sess_id));
			if(!$res){
				 print_r($cmd->errorInfo());
			}
			result_callback($res);
		break;
		case "create_like":
			$plant_id = $_POST['plant_id'];
			$cmd = $db->prepare("INSERT INTO `likes`(`plant_id`,`user_id`) VALUES(:plant_id,:user_id)");
			$res = $cmd->execute(array(':plant_id'=>$plant_id,':user_id'=>$sess_id));
			if(!$res){
				 print_r($cmd->errorInfo());
			}
			result_callback($res);

		break;
		case "update_clinic":
			$clinic_id = $_POST['clinic_id'];
			$clinic_name = $_POST['clinic_name'];
			$clinic_address = $_POST['clinic_address'];
			$clinic_desc = $_POST['clinic_desc'];
			$upload_status = $_POST['upload_status'];
			$clinic_type = $_POST['clinic_type'];

			$sqlx = "SELECT clinic_image FROM clinic WHERE clinic_id=?";
			$cmdx = $db->prepare($sqlx);
			$cmdx->execute(array($clinic_id));
			$rowsx = $cmdx->fetch();
			$clinic_image =  $rowsx['clinic_image'];
			if(!empty($clinic_image)){
				if(file_exists('images/clinic/'.$clinic_image)){
					unlink('images/clinic/'.$clinic_image);
				}
			}


			if($upload_status == "upload"){

				$session_id = date('ymdhis');
				$file=$_FILES['file_upload']['tmp_name'];	
				$name=$_FILES['file_upload']['name'];
				$split_point = '.';
				$stringpos = strrpos($name, $split_point, -1);
				$finalstr = substr($name,0,$stringpos);
				$FinalName="".$session_id."_".$finalstr."";

				$image= addslashes(@file_get_contents($_FILES['file_upload']['tmp_name']));
				$image_name= addslashes($_FILES['file_upload']['name']);
				$image_size= @getimagesize($_FILES['file_upload']['tmp_name']);
				$splitName = explode(".", $image_name); //split the file name by the dot
				$fileExt = end($splitName); //get the file extension
				$newFileName  = ucwords($FinalName.'.'.$fileExt); //join file name and ext.
				move_uploaded_file($_FILES["file_upload"]["tmp_name"],"images/clinic/".$newFileName);
		
				$avatar_location=$newFileName;	
			}else{
				$avatar_location=$clinic_image;
			}
			$cmd = $db->prepare("UPDATE clinic SET `clinic_name`=?, `clinic_address`=?, `clinic_desc`=?,`clinic_image`=?,`clinic_type`=? WHERE clinic_id =? ");
			$res = $cmd->execute(array($clinic_name,$clinic_address,$clinic_desc,$avatar_location,$clinic_type,$clinic_id));
			if(!$res){
				 print_r($cmd->errorInfo());
			}
			result_callback($res);

		break;
		
		case "remove_clinic":	

			$clinic_id = $_POST['clinic_id'];

			$sqlx = "SELECT clinic_image FROM clinic WHERE clinic_id=?";
			$cmdx = $db->prepare($sqlx);
			$cmdx->execute(array($clinic_id));
			$rowsx = $cmdx->fetch();
			$clinic_image =  $rowsx['clinic_image'];

			if(!empty($clinic_image)){
				if(file_exists('images/clinic/'.$clinic_image)){
					unlink('images/clinic/'.$clinic_image);
				}

			}

			$cmd = $db->prepare("DELETE FROM clinic WHERE clinic_id =? ");
			$res = $cmd->execute(array($clinic_id));
			result_callback($res);
		break;
		
		case "create_clinic":
			$clinic_name = $_POST['clinic_name'];
			$clinic_address = $_POST['clinic_address'];
			$clinic_desc = $_POST['clinic_desc'];
			$upload_status = $_POST['upload_status'];
			$clinic_type = $_POST['clinic_type'];
			if($upload_status == "upload"){

				$session_id = date('ymdhis');
				$file=$_FILES['file_upload']['tmp_name'];	
				$name=$_FILES['file_upload']['name'];
				$split_point = '.';
				$stringpos = strrpos($name, $split_point, -1);
				$finalstr = substr($name,0,$stringpos);
				$FinalName="".$session_id."_".$finalstr."";

				$image= addslashes(@file_get_contents($_FILES['file_upload']['tmp_name']));
				$image_name= addslashes($_FILES['file_upload']['name']);
				$image_size= @getimagesize($_FILES['file_upload']['tmp_name']);
				$splitName = explode(".", $image_name); //split the file name by the dot
				$fileExt = end($splitName); //get the file extension
				$newFileName  = ucwords($FinalName.'.'.$fileExt); //join file name and ext.
				move_uploaded_file($_FILES["file_upload"]["tmp_name"],"images/clinic/".$newFileName);
		
				$avatar_location=$newFileName;	
			}else{
				$avatar_location="";
			}

			$cmd = $db->prepare("INSERT INTO clinic (`clinic_name`,`clinic_address`,`clinic_desc`,`clinic_image`,`owner`,`clinic_type`) VALUES(:clinic_name,:clinic_address,:clinic_desc,:clinic_image,:owner,:clinic_type)");

			$res = $cmd->execute(array(':clinic_name'=>$clinic_name,':clinic_address'=>$clinic_address,':clinic_desc'=>$clinic_desc,':clinic_image'=>$avatar_location,':owner'=>$sess_id,':clinic_type'=>$clinic_type));
			if(!$res){
				 print_r($cmd->errorInfo());
			}
			result_callback($res);

		break;
		case "clinic_lists":
			$cmd = $db->prepare("SELECT clinic_name,clinic_address,clinic_desc,clinic_image,owner,clinic_type,clinic_id,
				CASE WHEN clinic_type = '0' THEN 'Alternative Medicine'
				WHEN clinic_type = '1' THEN 'Masssage Clinic'
				WHEN clinic_type = '2' THEN 'Reflex Clinic'
				WHEN clinic_type = '3' THEN 'Others' END as type
				FROM clinic WHERE owner=?");
			$cmd->execute(array($sess_id));
			$rows  = $cmd->fetchAll();
			$arr = array();

			foreach($rows as $r){
				$arr[] = $r;
			}

			echo json_encode($arr);
		break;
		case "submitted_request":
			$cmd = $db->prepare("SELECT ps.approver,ps.plant_id,ps.plant_name,ps.description,ps.plant_image,ps.sci_name,ps.common_name,ps.submitted_by,mr.mem_lname,mr.mem_fname,mr.mem_image,mr.mem_id,CASE WHEN ps.status = '1' THEN 'Pending' ELSE 'Approved' end what_status FROM plants as ps LEFT JOIN member as mr ON ps.approver=mr.mem_id WHERE ps.submitted_by=? ORDER BY ps.plant_id DESC");
			$cmd->execute(array($sess_id));
			$rows = $cmd->fetchAll();
			encode_json($rows);
		break;
		case "approve_suggest":
			$plant_id = $_POST['plant_id'];
			$sql = "UPDATE plants SET `status`=? WHERE plant_id =? ";
			$cmd = $db->prepare($sql);
			$res = $cmd->execute(array(0,$plant_id));

			if($res){

				$find_sub = $db->prepare("SELECT submitted_by,plant_id FROM plants WHERE plant_id=?");
				$find_sub->execute(array($plant_id));
				$rowx = $find_sub->fetch();
				$submitted_by = $rowx['submitted_by'];
				$cmd_n = $db->prepare("INSERT INTO `notifications`(`receiver`,`sender`,`type`,`content`) VALUES(:receiver,:sender,:type,:content)");
				$cmd_n->execute(array(':receiver'=>$submitted_by,':sender'=>$sess_id,':type'=>'1',':content'=>"Health Professional approved the plant you submitted. Click <a href='profile.php?pass_param=submitted_request' style='color:green;font-weight:bold;'>here</a> to see"));

			}
			result_callback($res);
		break;
		case "cancel_suggest":
			$plant_id = $_POST['plant_id'];
			// $sql = "UPDATE plants SET `status`=? WHERE plant_id =? ";
			// $cmd = $db->prepare($sql);
			// $res = $cmd->execute(array(2,$plant_id));
			$find_plant = $db->prepare("SELECT plant_image,plant_id FROM plants WHERE plant_id=?");
			$find_plant->execute(array($plant_id));
			$row = $find_plant->fetch();

			$image = "plant/".$row['plant_image'];
			if(file_exists($image)){
				unlink($image);
			}

			$cmd = $db->prepare("DELETE FROM plants WHERE plant_id=?");
			$res = $cmd->execute(array($plant_id));
			result_callback($res);
		break;

		case "pending_suggestions":
			$cmd = $db->prepare("SELECT * FROM plants WHERE status = '1' AND approver='$sess_id'");
			$cmd->execute();
			$rows = $cmd->fetchAll();
			
				$arrayindex=array();
				
				foreach($rows as $r){
				$arrayindex[] = $r;
				}

				echo json_encode($arrayindex);
		break;

		case "my_suggestions":
			$cmd = $db->prepare("SELECT * FROM plants WHERE submitted_by=$sess_id");
			$cmd->execute();
			$rows = $cmd->fetchAll();
			
				$arrayindex=array();
				
				foreach($rows as $r){
				$arrayindex[] = $r;
				}

				echo json_encode($arrayindex);
		break;
		case "submit_suggestion":
			$upload_status = $_POST['upload_status'];

			$sqlx = "SELECT mem_id FROM member WHERE mem_type=? ORDER BY RAND() LIMIT 1";
			$cmdx = $db->prepare($sqlx);
			$cmdx->execute(array('health'));
			$rowsx = $cmdx->fetch();
			$approver =  $rowsx['mem_id'];
			
			if($upload_status == "upload"){

				$session_id = date('ymdhis');
				$file=$_FILES['file_upload']['tmp_name'];	
				$name=$_FILES['file_upload']['name'];
				$split_point = '.';
				$stringpos = strrpos($name, $split_point, -1);
				$finalstr = substr($name,0,$stringpos);
				$FinalName="".$session_id."_".$finalstr."";

				$image= addslashes(@file_get_contents($_FILES['file_upload']['tmp_name']));
				$image_name= addslashes($_FILES['file_upload']['name']);
				$image_size= @getimagesize($_FILES['file_upload']['tmp_name']);
				$splitName = explode(".", $image_name); //split the file name by the dot
				$fileExt = end($splitName); //get the file extension
				$newFileName  = ucwords($FinalName.'.'.$fileExt); //join file name and ext.
				move_uploaded_file($_FILES["file_upload"]["tmp_name"],"plant/".$newFileName);
		
				$avatar_location=$newFileName;	
			}else{
				$avatar_location="";
			}

			$name = $_POST['name'];
			$description = $_POST['description'];
			$image = $_POST['image'];
			$scientifical_name = $_POST['scientifical_name'];
			$common_name = $_POST['common_name'];


			if($sess_type == "health"){
				$p_stat = 0;
				$p_approver = $sess_id;
			}else{
				$p_approver = $approver;
				$p_stat = 1;
			}

			$sql = "INSERT INTO `plants`(`plant_name`,`description`,`plant_image`,`sci_name`,`common_name`,`status`,`approver`,`submitted_by`) VALUES(:plant_name,:description,:plant_image,:sci_name,:common_name,:status,:approver,:submitted_by)";
			$cmd = $db->prepare($sql);
			$res = $cmd->execute(array(':plant_name' => $name,':description' => $description,':plant_image' => $avatar_location,':sci_name'=>$scientifical_name,':common_name' => $common_name,':status' =>$p_stat,':approver' => $p_approver,':submitted_by' => $sess_id));
			if(!$res){
				 print_r($cmd->errorInfo());
			}
			else{//Notify the approver
				if($sess_type != "health"){
					$cmd_n = $db->prepare("INSERT INTO `notifications`(`receiver`,`sender`,`type`,`content`) VALUES(:receiver,:sender,:type,:content)");
					$cmd_n->execute(array(':receiver'=>$approver,':sender'=>$sess_id,':type'=>'1',':content'=>"A user submitted a plant request for you to check. Click <a href='profile.php?pass_param=request' style='color:green;font-weight:bold;'>here</a> to see"));
				}

				$admins = $db->prepare("SELECT mem_type,mem_id FROM member WHERE mem_type=?"); //SEND NOTIFICATION OF ALL ADMIN
				$admins->execute(array('admin'));
				$row_admin = $admins->fetchAll();
				foreach($row_admin as $r_a){
					$cmd_n2 = $db->prepare("INSERT INTO `notifications`(`receiver`,`sender`,`type`,`content`) VALUES(:receiver,:sender,:type,:content)");
					$cmd_n2->execute(array(':receiver'=>$r_a['mem_id'],':sender'=>$sess_id,':type'=>'1',':content'=>"A user submitted a plant request for a health professional to check."));
				}
			}
			result_callback($res);

		break;

		case "check_reg_email":
			$email = $_POST['email'];
			$sql = "SELECT COUNT(mem_email) FROM member WHERE mem_email=?";
			$cmd = $db->prepare($sql);
			$cmd->execute(array($email));
			$num = $cmd->fetchColumn();

			if($num > 0){
				echo"exist";
			}else{
				echo"available";
			}


		break; 

		case "check_reg_username":
			$username = $_POST['username'];
			$sql = "SELECT COUNT(mem_username) FROM member WHERE mem_username=?";
			$cmd = $db->prepare($sql);
			$cmd->execute(array($username));
			$num = $cmd->fetchColumn();

			if($num > 0){
				echo"exist";
			}else{
				echo"available";
			}

		break;

		case "reply_conversation":
			$message = $_POST['message'];
			$receiver = $_POST['receiver'];

			$chex = $db->prepare("SELECT COUNT(conversation_id) FROM messages WHERE receiver=? AND sender=? LIMIT 1");
			$chex->execute(array($sess_id,$receiver));
			$num = $chex->fetchColumn();
			
			if($num > 0){
				$f_plant = $db->prepare("SELECT conversation_id,id FROM messages WHERE receiver=? AND sender=?  ORDER BY id DESC LIMIT 1");
				$f_plant->execute(array($sess_id,$receiver));
				$row_plant = $f_plant->fetch();
				$conv_id = $row_plant['conversation_id'];
			}else{
				$conv_id = 0;
			}

			$sql = "INSERT INTO messages(`content`,`receiver`,`sender`,`conversation_id`) VALUE(:content,:receiver,:sender,:conversation_id)";
			$cmd = $db->prepare($sql);
			$res = $cmd->execute(array(":content"=>$message,':receiver'=>$sess_id,':sender'=>$sess_id,':conversation_id'=>$conv_id));

			$message_id = $db->lastInsertId();

			if($conv_id == 0){
				$cmx = $db->prepare("UPDATE messages SET conversation_id=? WHERE receiver=? AND sender=?");
				$cmx->execute(array($message_id,$sess_id,$receiver));
			}

			if($res){
				// $cmd_n = $db->prepare("INSERT INTO `notifications`(`receiver`,`sender`,`type`,`content`) VALUES(:receiver,:sender,:type,:content)");
				// $cmd_n->execute(array(':receiver'=>$receiver,':sender'=>$sess_id,':type'=>'6',':content'=>"You receive a message from a user. Click <a href='javascript:void(0)' onclick='message_process(\"open_message\",\"".$message_id."\")' style='color:green;font-weight:bold;'>here</a> to see"));
			}else{
				$cmd->errorInfo();
			}
			result_callback($res);
		break;	

		case "reply_message":
			$message = $_POST['message'];
			$receiver = $_POST['receiver'];

			$chex = $db->prepare("SELECT COUNT(conversation_id) FROM messages WHERE receiver=? AND sender=? LIMIT 1");
			$chex->execute(array($sess_id,$receiver));
			$num = $chex->fetchColumn();
			
			if($num > 0){
				$f_plant = $db->prepare("SELECT conversation_id,id FROM messages WHERE receiver=? AND sender=?  ORDER BY id DESC LIMIT 1");
				$f_plant->execute(array($sess_id,$receiver));
				$row_plant = $f_plant->fetch();
				$conv_id = $row_plant['conversation_id'];
			}else{
				$conv_id = 0;
			}

			$sql = "INSERT INTO messages(`content`,`receiver`,`sender`,`conversation_id`) VALUE(:content,:receiver,:sender,:conversation_id)";
			$cmd = $db->prepare($sql);
			$res = $cmd->execute(array(":content"=>$message,':receiver'=>$receiver,':sender'=>$sess_id,':conversation_id'=>$conv_id));

			$message_id = $db->lastInsertId();

			if($conv_id == 0){
				$cmx = $db->prepare("UPDATE messages SET conversation_id=? WHERE receiver=? AND sender=?");
				$cmx->execute(array($message_id,$sess_id,$receiver));
			}

			if($res){
				$cmd_n = $db->prepare("INSERT INTO `notifications`(`receiver`,`sender`,`type`,`content`) VALUES(:receiver,:sender,:type,:content)");
				$cmd_n->execute(array(':receiver'=>$receiver,':sender'=>$sess_id,':type'=>'6',':content'=>"You receive a message from a user. Click <a href='javascript:void(0)' onclick='message_process(\"open_message\",\"".$message_id."\")' style='color:green;font-weight:bold;'>here</a> to see"));
			}else{
				$cmd->errorInfo();
			}
			result_callback($res);
		break;	


		case "submit_message":
			$message = $_POST['message'];
			$receiver = $_POST['receiver'];
			$chex = $db->prepare("SELECT COUNT(conversation_id) FROM messages WHERE receiver=? AND sender=? LIMIT 1");
			$chex->execute(array($receiver,$sess_id));
			$num = $chex->fetchColumn();
			
			if($num > 0){
				$f_plant = $db->prepare("SELECT conversation_id,id FROM messages WHERE receiver=? AND sender=?  ORDER BY id DESC LIMIT 1");
				$f_plant->execute(array($receiver,$sess_id));
				$row_plant = $f_plant->fetch();
				$conv_id = $row_plant['conversation_id'];
			}else{
				$conv_id = 0;
			}

			$sql = "INSERT INTO messages(`content`,`receiver`,`sender`,`conversation_id`) VALUE(:content,:receiver,:sender,:conversation_id)";
			$cmd = $db->prepare($sql);
			$res = $cmd->execute(array(":content"=>$message,':receiver'=>$receiver,':sender'=>$sess_id,':conversation_id'=>$conv_id));

			$message_id = $db->lastInsertId();

			if($conv_id == 0){
				$cmx = $db->prepare("UPDATE messages SET conversation_id=? WHERE receiver=? AND sender=?");
				$cmx->execute(array($message_id,$receiver,$sess_id));
			}

			if($res){
				$cmd_n = $db->prepare("INSERT INTO `notifications`(`receiver`,`sender`,`type`,`content`) VALUES(:receiver,:sender,:type,:content)");
				$cmd_n->execute(array(':receiver'=>$receiver,':sender'=>$sess_id,':type'=>'6',':content'=>"You receive a message from a user. Click <a href='javascript:void(0)' onclick='message_process(\"open_message\",\"".$message_id."\")' style='color:green;font-weight:bold;'>here</a> to see"));
			}else{
				$cmd->errorInfo();
			}
			result_callback($res);
		break;	

		case "prof_list":
			$sql = "SELECT * FROM member WHERE mem_type=? AND status =? ORDER BY mem_id DESC";
			$cmd = $db->prepare($sql);
			$cmd->execute(array('health',1));
			$rows = $cmd->fetchAll();
			
				$arrayindex=array();
				
				foreach($rows as $r){
				$arrayindex[] = $r;
				}

				echo json_encode($arrayindex);
			$db = null;
		break;

		case "update_comment":
			$comment_id = $_POST['comment_id'];
			$comment = $_POST['comment'];

			$sql = "UPDATE `comments` SET `comment_content`=? WHERE comment_id=?";
			$cmd = $db->prepare($sql);
			$res = $cmd->execute(array($comment,$comment_id));
			if($res){
				$f_plant = $db->prepare("SELECT plant_id FROM comments WHERE comment_id=?");
				$f_plant->execute(array($comment_id));
				$row_plant = $f_plant->fetch();
				$plant_id = $row_plant['plant_id'];

				$fetch_comments = $db->prepare("SELECT mem_id,plant_id FROM comments WHERE mem_id!='$sess_id' AND plant_id=?");
				$fetch_comments->execute(array($plant_id));
				$rows_comments = $fetch_comments->fetchAll();
				foreach($rows_comments as $r_c){
					$cmd_n = $db->prepare("INSERT INTO `notifications`(`receiver`,`sender`,`type`,`content`) VALUES(:receiver,:sender,:type,:content)");
					$cmd_n->execute(array(':receiver'=>$r_c['mem_id'],':sender'=>$sess_id,':type'=>'0',':content'=>"A user updated a comment on the plant you recently involved. Click <a href='javascript:void(0)' onclick='comment_process('lists','".$plant_id."')' style='color:green;font-weight:bold;'>here</a> to see"));
				}
			}
			result_callback($res);

		break;
		case "remove_comment":
			$comment_id = $_POST['comment_id'];

			$sql = "DELETE FROM comments WHERE comment_id = ?";
			$cmd = $db->prepare($sql);
			$res = $cmd->execute(array($comment_id));
			result_callback($res);
		break;

		case "plant_comments":
			$plant_id = $_POST['plant_id'];
			$sql = "SELECT * FROM comments WHERE plant_id=? ORDER BY comment_id DESC";
			$cmd = $db->prepare($sql);
			$cmd->execute(array($plant_id));
			$rows = $cmd->fetchAll();
			
				$arrayindex=array();
				
				foreach($rows as $r){
				$arrayindex[] = $r;
				}

				echo json_encode($arrayindex);
			$db = null;

		break;

		case "what_plant":
			$plant_id = $_POST['plant_id'];
			$cmd = $db->prepare("SELECT plant_image,description,plant_name FROM plants WHERE plant_id=?");
			$cmd->execute(array($plant_id));
			$rows = $cmd->fetchAll();
			encode_json($rows);
		break;

		case "insert_plant_comment":
			$comment = $_POST['comment'];
			$plant_name = $_POST['plant_name'];
			$plant_id = $_POST['plant_id'];

			$sql = "INSERT INTO `comments`(`plant_name`,`mem_id`,`comment_content`,`mem_username`,`plant_id`) VALUES(:plant_name,:mem_id,:comment_content,:mem_username,:plant_id);";
			$pre = $db->prepare($sql);
			$res = $pre->execute(array(':plant_name'=>$plant_name,':mem_id'=>$sess_id,':comment_content'=>$comment,':mem_username'=>$sess_name,':plant_id'=>$plant_id));
			if(!$res){
				 print_r($pre->errorInfo());
			}else{
				// $f_plant = $db->prepare("SELECT plant_id FROM comments WHERE comment_id=?");
				// $f_plant->execute(array($comment_id));
				// $row_plant = $f_plant->fetch();
				// $plant_id = $row_plant['plant_id'];

				$fetch_comments = $db->prepare("SELECT mem_id,plant_id FROM comments WHERE mem_id!='$sess_id' AND plant_id=?");
				$fetch_comments->execute(array($plant_id));
				$rows_comments = $fetch_comments->fetchAll();
				foreach($rows_comments as $r_c){
					$cmd_n = $db->prepare("INSERT INTO `notifications`(`receiver`,`sender`,`type`,`content`) VALUES(:receiver,:sender,:type,:content)");
					$cmd_n->execute(array(':receiver'=>$r_c['mem_id'],':sender'=>$sess_id,':type'=>'0',':content'=>"A user commented on the plant you recently involved. Click <a href='javascript:void(0)' onclick='comment_process(\"lists\",\"".$plant_id."\")' style='color:green;font-weight:bold;'>here</a> to see"));
				}
	
			}

			result_callback($res);
		break;

	}//end of switch

	function unlink_image($user_id){

	}

	function result_callback($val){
		if($val){
			$stat = "success";
		}else{

			$stat = "error";
		}

		echo $stat;
	}

	function encode_json($rows){
		$arr = array();
		foreach($rows as $r){
			$arr[] = $r;
		}
		echo json_encode($arr);
	}

?>