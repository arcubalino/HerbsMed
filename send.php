
<html>
<head>
    
    <title>HerbsMed</title>
    <!-- Core CSS - Include with every page -->
    <link href="assets/plugins/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/plugins/pace/pace-theme-big-counter.css" rel="stylesheet" />
    <link href="assets/css/style3.css" rel="stylesheet" />
    <link href="assets/css/main-style.css" rel="stylesheet" />
    <!-- Page-Level CSS -->
    <link href="assets/plugins/morris/morris-0.4.3.min.css" rel="stylesheet" />
   </head>
<body>
        <!--  wrapper -->
        <div id="wrapper">
        <!-- navbar top -->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="navbar">
        <!-- navbar-header -->
        <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.html">
        <img src="assets/img/logo.png" alt="" style="width:150px;height:50px"/>
        </a>
        </div>
            <!-- end navbar-header -->
            <!-- navbar-top-links -->
                <ul class="nav navbar-top-links navbar-right">
                <!-- main dropdown -->
                <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-3x"></i>
                </a>
                <!-- dropdown user-->
                <ul class="dropdown-menu dropdown-user">
                        
                <li class="divider"></li>
                <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i>Logout</a>
                </li>
                </ul>
                    <!-- end dropdown-user -->
                </li>
                <!-- end main dropdown -->
                </ul>
                <!-- end navbar-top-links -->

            </nav>
            
            
            <!-- end navbar top -->

            <!-- navbar side -->
            <nav class="navbar-default navbar-static-side" role="navigation">
            <!-- sidebar-collapse -->
            <div class="sidebar-collapse">
            <!-- side-menu -->
            <ul class="nav" id="side-menu">
            <li>
            <!-- user image section-->
           <div class="user-section">
           <div class="user-section-inner">
           <img src="assets/img/user.jpg" alt="">
            </div>
            <div class="user-info">
            <div>Arjun <strong>Osa</strong></div>
            <div class="user-text-online">
            <span class="user-circle-online btn btn-success btn-circle "></span>&nbsp;Online
            </div>
            </div>
            </div>
            <!--end user image section-->
            </li>
                        <li class="sidebar-search">
                        <!-- search section-->
                        <div class="input-group custom-search-form">
                         <input type="text" class="form-control" placeholder="Search...">
                        <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                        <i class="fa fa-search"></i>
                        </button>
                        </span>
                        </div>
                        <!--end search section-->
                        </li>
                        <li>
                        <a href="about.php"><i class="fa fa-user fa-fw"></i>About Me</a>
                        </li>
                        <li >
                        <a href="searchtreatment.php"><i  class="fa fa-fw fa-search"></i>Search Treatments</a>
                        </li >
                        <li class="selected">
                        <a href="send.php"><i class="fa fa-fw fa-paper-plane"></i>Send Request</a>
                        </li>
                        <li>
                        <a href="tables.html"><i class="fa fa-table fa-fw"></i>List of herbal Plants</a>
                        </li>
                        <li>
                        <a href="forms.html"><i class="fa fa-edit fa-fw"></i>My request</a>
                        </li>
                  
                        </ul>
                        <!-- end side-menu -->
                        </div>
                        <!-- end sidebar-collapse -->
                        </nav>
                        <!-- end navbar side -->
                        <!--  page-wrapper -->
                                                <div id="page-wrapper">

                                                <div class="row">
                                                <!-- Page Header -->
                                                <div class="col-lg-12">
                                                <h1 class="page-header">Send Request</h1>
                                                </div>
                                                <!--End Page Header -->
                                                </div>

                                                <div class="row">
                                                <!-- Welcome -->
                                                <div class="col-lg-12">
                                                <div class="alert alert-info">
                                <i class="fa fa-folder-open"></i><b>&nbsp;Hello ! </b>Welcome Back <b>Arjun Osa </b>
 
                                                </div>
                                                </div>
                                                <!--end  Welcome -->
                                                </div>


         

                                            <!--About Me-->
                                            <div class="row">
                                            <div class="col-lg-4">
                                            <!-- Notifications-->
                                            <div class="panel panel-primary">
                                            <div class="panel-heading">
                                            <i class="fa fa-user fa-fw"></i>Send Request
                                            </div>

                                             <div class="row">
                                            <div class="col-lg-12">
                                            <div class="alert alert-info alert-dismissable">
                                            <i class="fa fa-info-circle"></i>  <strong>Herbal Plant Name:</strong><br>  <input type="text" name="plantname" size="95">
                                            </div>
                                            </div>
                                            </div>
                                                
                                             <div class="row">
                                            <div class="col-lg-12">
                                            <div class="alert alert-info alert-dismissable">
                                            <i class="fa fa-info-circle"></i>  <strong>Description:</strong><br> <textarea class="scrollabletextbox" name="note" style="width:700px;height:200px">
                                            </textarea>
                                            </div>
                                            </div>
                                            </div>
                                                
                                              
                                            <div class="row">
                                            <div class="col-lg-12">
                                            <div class="alert alert-info alert-dismissable">
                                            <i class="fa fa-info-circle"></i>  <strong>Uses:</strong><br> <textarea class="scrollabletextbox" name="note" style="width:700px;height:200px">
                                            </textarea>
                                            </div>
                                            </div>
                                            </div>
                                                
                                        <div class="row">
                                        <div class="col-lg-12">
                                        <div class="alert alert-info alert-dismissable">
                                        <i class="fa fa-info-circle"></i>    <strong>Upload:</strong> <br>  <input type="file" name="filename" accept="image/gif, image/jpeg, image/png">
                                        </div>
                                        </div>
                                        </div>
                
                                        <div class="row">
                                        <div class="col-lg-12">
                                        <div class="alert alert-info alert-dismissable">
                                        <button class="btn-success" type="button">Send Request!</button>
                                        </div>
                                        </div>
                                        </div>
        

                                            </div>
                                            <!--End Notifications-->
                                            </div>
                                            </div>
                                            </div>
                                            <!-- end page-wrapper -->

    </div>
    <!-- end wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="assets/plugins/jquery-1.10.2.js"></script>
    <script src="assets/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="assets/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="assets/plugins/pace/pace.js"></script>
    <script src="assets/scripts/siminta.js"></script>
    <!-- Page-Level Plugin Scripts-->
    <script src="assets/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/plugins/morris/morris.js"></script>
    <script src="assets/scripts/dashboard-demo.js"></script>

</body>

</html>
