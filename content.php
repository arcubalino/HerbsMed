<?php require('bundle.php');?>
<?php $content =  $_POST['content'];?>

<?php 
switch($content){
	case "suggestions":
?>
<script type="text/javascript">
	suggest_process('lists','');
</script>
		<div class="row">
		                <div class="col-lg-12">
		                    <h1 class="page-header">Request</h1>
		                </div>

		    <div id="request_here"></div>            
		</div>
<?php break;
	case "clinics":
?>
<script type="text/javascript">
	clinic_process('lists2','');
</script>
		<div class="row">
		                <div class="col-lg-12">
		                    <h1 class="page-header">List of Clinics</h1>
		                </div>

		     <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>Image</th>
                      <th>Name</th>
                      <th>Description</th>
                      <th>Address</th>
                      <th>Type</th>
                      <th>Map</th>
                      <th>Action</th>
                    </tr> 
                  </thead>
                  <tbody id="clinic_here"></tbody>
                </table> 
                <br><br>
		</div>
<?php break;
case "add_clinic":?>
	<div class="row">
		                <div class="col-lg-12">
		                    <h1 class="page-header">Add new Clinic</h1>
		                </div>

		    <div class="row">
		    	<div class="col-md-9">
		    		<div class="form-group"><input type="hidden" id="clinic_id">
		    			Image:<br>
		    			<input type="file" id="file_upload">
		    		</div>
		    		<div class="form-group">
		    			Name:<br>
		    			<input type="text" id="clinic_name" class='form-control'>
		    		</div>
		    		<div class="form-group">
		    			Type:<br>
		    			<select id="clinic_type" class="form-control">
		    				<option value="0">Alternative Medicine</option>
		    				<option value="1">Massage</option>
		    				<option value="2">Reflex</option>
		    				<option value="3">Others</option>
		    			</select>
		    		</div>
		    		<div class="form-group">
		    			Address:<br>
		    			<input type="text" id="clinic_address" class='form-control'>
		    		</div>
		    		<div class="form-group">
		    			Description:<br>
		    			<textarea id="clinic_desc" class='form-control'></textarea>
		    		</div>
		    		<div class="form-group" align="right">
		    			<a href="javascript:void(0)" onclick="clinic_process('cancel2','')"><i class="fa fa-times"></i> Cancel</a>
		    			&nbsp;&nbsp;
		    			<a href="javascript:void(0)"  onclick="clinic_process('save','inside')"><i class="fa fa-paper-plane"></i> Submit</a>
		    		</div>
		    	</div>
		    </div>            
		</div>
<?php break; 
	  case "notification_admin":
?>
	 <?php require('bundle_js.php'); ?>
          <script type="text/javascript">
            notification_process('lists','');
          </script>
          <h3 class="page-header" style="color:gray;">MY NOTIFICATIONS</h3>
          <div class="row">
            <div class="col-md-12">
              <div  id="notification_ul"></div>
            </div>
          </div>  <br><br>  
<?php break; 
	case "product_approval":
?>	
		 <?php require('bundle_js.php'); ?>
	 	<script type="text/javascript">
            product_process('my_list','');
         </script>
          <h3 class="page-header" style="color:gray;">PRODUCT PROMOTIONS</h3>
          <div class="row">
            <div class="col-md-12">
             <table class="table table-striped">
			    <thead>
			      <tr>
			        <th>&nbsp;</th>
			        <th><center>Name</th>
			        <th><center>Description</center></th>
			        <th><center>Related Plant</center></th>
			        <th><center>Status</center></th>
			        <th><center>Action</center></th>
			      </tr>
			    </thead>
			    <tbody id="products_here"></tbody>
			  </table>
            </div>
          </div>  <br><br>  

<?php break;	
case "admin_ass":
?>
 <?php require('bundle_js.php'); ?>
        <script type="text/javascript">
          assessment_process('lists','');
        </script>
      <div class="row">
        <div class="col-md-9">
          <h3 class="page-header" style="color:gray;">ASSESSMENT LIST</h3>
        </div>
        <div class="col-md-3" align="right" style="padding-top: 20px;">
              <a href="javascript:void(0)" onclick="assessment_process('add_ass','')"><i class="fa fa-plus"></i> Create Assessment</a>
        </div>
      </div>  
      <div class="row" id="assessment_here"></div>
<?php break;
case "add_ass":
?>
<br>
<div class="row">
        <div class="col-md-9">
          <h3 class="page-header" style="color:gray;">CREATE ASSESSMENT</h3>
        </div>
</div>        
<div class="row">
	<div class="col-md-12">
		<input type="hidden" id="ass_id">
				<div class="form-group">
					Title: <br>
					<input type="text" class="form-control" id="ass_title">
				</div><br>
				<div class="form-group">
					Related Plant: <br>
					<select class="form-control" id="related_plant" onchange="assessment_process('select_related','plant')">
						<option value="0">None of the above</option>
					</select>
				</div><br>
				<div class="form-group" id="no_related" style="display:none;">
					Plant Name: <br>
					<input type="text" class="form-control" id="plant_name" style="border-color:red;"><br>
				</div>
				<div class="form-group">
					Related Tutorial: <br>
					<select class="form-control" id="related_tutorial" onchange="assessment_process('select_related','tutorial')">
						<option value="0">None of the above</option>
					</select>
				</div><br>
				<div class="form-group" id="no_tutorial" style="display:none;">
					Video URL: <br>
					<input type="text" class="form-control" id="video_url" style="border-color:red;"><br>
					<small style="color:gray;">Copy <strong>ONLY</strong> the highlighted code of your Youtube Video to the URL textbox:</small>
				<br><img src="images/high.png">
				</div>
				<div class="form-group">
					Content: <br>
					<textarea class="form-control" id="ass_content"></textarea>
				</div><br>
				<div align="right"><a href="javascript:void(0)" onclick="assessment_process('admin_list','')"><i class="fa fa-times"></i> Cancel</a>
				&nbsp;&nbsp;
				<a href="javascript:void(0)" onclick="assessment_process('submit','')"><i class="fa fa-paper-plane"></i> Submit</a></div>
	</div>			
</div>				
<br><br><br><br><br><br>
<?php 
break;
case "approved_list":?>
<br>
	 <?php require('bundle_js.php'); ?>
	 	<script type="text/javascript">
            product_process('approved_list','');
         </script>
          <h3 class="page-header" style="color:gray;">APPROVED PRODUCTS</h3>
          <div class="row">
            <div class="col-md-12">
             <table class="table table-striped">
			    <thead>
			      <tr>
			        <th>&nbsp;</th>
			        <th><center>Name</th>
			        <th><center>Description</center></th>
			        <th><center>Related Plant</center></th>
			        <th><center>Status</center></th>
			      </tr>
			    </thead>
			    <tbody id="products_here"></tbody>
			  </table>
            </div>
          </div>  <br><br>  

<?php break;
} ?>