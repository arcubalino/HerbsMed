Phase 1: 

	- Posting of Comments -- Done
	- Posting of Suggestion/Send -- Done
	- Approve and validation of Plants -- Done
	- Health Professional (Sending and Receiving of Message) - Done
	- Statistics for Alternative Medicines (Based on Likes and Survey)
	- Display of Healthshops with Locations -- Done
	- Validation for Registration, Encryption of Password - Done

Phase 2: 
	- Notifications (Admin, Health Professional and Ordinary)
		- Notify on linked if there is comment on the linked that you comment (All) -- Done
		- Notify if there is a request and approved plants (Admin) -- Done
		- Request of promotion products from HP for approval -- Done
		- Message from HP (Ordinary) -- Done
		- If suggested plant has been approved (Ordinary) -- Done
		- Message from Ordinary (Health) -- Done
		- Review added from ordinary (Heatlh) -- Done
		
			0 - Comment (All) | 
			1 - Newly Approved Plants (Admin) | 
			2 - Review added from Ordinary (Health)
			3 - Request of Promotion product | 
			4 - Message from HP (Ordinary) | 
			5 - If Suggested plant has been approved (Ordinary) | 
			6 - Message from Ordinary (Health) | 

	- Admin (List of Users) -- Done
	
	- Profile Settings for (Ordinary and Health) and Adding of Review in Health Prof
		- Update of Account Details --  Done
		- Update of Profile Details --  Done
		- Check Review (Health Prof only) --  Done
	
	- Tutorials (Video and Textual Representation)
		- Health and Admin only 

	- Admin Dashboard and Approval of Health Prof. --  Done

Phase 3: 
	- Health Professional Dashboard -- Done
	- Product Promotion for Health Prof -- Done
	- Approval of Product Promostion (Admin) -- Done
	- Health Assessment (Listing of Ailments)