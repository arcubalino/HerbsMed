<?php 
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);
// session_start();

?>     
      <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="admin.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-user fa-fw"></i>Users<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="user1.php">Ordinary People</a>
                                </li>
                                <li>
                                    <a href="user2.php">Health Professionals</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="javascript:void(0)" onclick="notification_process('for_admin','')"><i class="fa fa-bell"></i>  Notification <span class="badge" id="notify_num">0</span></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-pagelines fa-fw"></i>Plants<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="view_plant.php">View Plants</a>
                                </li>
                           
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-hospital-o fa-fw"></i>Clinics<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="javascript:void(0)" onclick="clinic_process('view','')">View Clinics</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" onclick="clinic_process('add_now','')">Add New Clinic</a>
                                </li>
                               
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-shopping-cart fa-fw"></i>Herbal Products<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="javascript:void(0)" onclick="product_process('admin_approve','')">View Products</a>
                                </li>
                            </ul>
                        </li>
                        <?php if($_SESSION['sess_userrole'] == "health"){?>
						<li>
                            <a href="javascript:void(0)" onclick="suggest_process('check_suggestions','')"><i class="fa fa-send fa-fw"></i>Requests</a>
                        </li>
                        <?php }?>
                        <li>
                            <a href="javascript:void(0)" onclick="product_process('admin_list','')"><i class="fa fa-dashboard fa-fw"></i> Promotion Approvals</a>
                        </li>

                        <li>
                            <a href="javascript:void(0)" onclick="assessment_process('admin_list','')"><i class="fa fa-list"></i>  Health Assessment</a>
                        </li>
                        <li>
                            <a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->

            <?php 
                require('bundle.php');
            ?>

            <input type="hidden" id="requested_page">