<?php

function upload_image()
{
	if(isset($_FILES["user_image"]))
	{
		$extension = explode('.', $_FILES['user_image']['name']);
		$new_name = rand() . '.' . $extension[1];
		$destination = './plant/' . $new_name;
		move_uploaded_file($_FILES['user_image']['tmp_name'], $destination);
		return $new_name;
	}
}

function get_image_name($plant_id)
{
	include('dbConfig.php');
	$statement = $dbh->prepare("SELECT plant_image FROM plants WHERE plant_id = '$plant_id'");
	$statement->execute();
	$result = $statement->fetchAll();
	foreach($result as $row)
	{
		return $row["plant_image"];
	}
}

function get_total_all_records()
{
	include('dbConfig.php');
	$statement = $dbh->prepare("SELECT * FROM plants");
	$statement->execute();
	$result = $statement->fetchAll();
	return $statement->rowCount();
}

?>