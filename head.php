<head>
	<!-- 1.0 meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<meta name="description" content="Animated 404 Error Page">
	<meta name="author" content="rakesh535">
	<meta name="keywords" content="404, css3, html5, animated, animated bubble, template">
	<!-- 2.0 title -->
	<title>HerbsMed</title>
	<!-- 3.0 CSS IMPORT -->
	<!-- CSS IMPORT FOR PLUGIN -->
	<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/js/wow/css/libs/animate.css" rel="stylesheet">
	<link href="assets/js/wordsrotator/jquery.wordrotator.min.css" rel="stylesheet">
	<!-- CSS IMPORT FOR FONT -->
	<link href='fonts/g1.css' rel='stylesheet' type='text/css'>
	<link href='fonts/g2.css' rel='stylesheet' type='text/css'>
	<link href='assets/fonts/font-awesome/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
	<!-- CSS FOOTER -->	
	    <link  rel="stylesheet" href="fonts/g3.css" type="text/css" />
    	<link rel="stylesheet" href="fonts/w31.css">
    	<link rel="stylesheet" href="fonts/g4.css">
<!-- 		<link rel="https://www.w3schools.com/graphics/google_maps_basic.asp">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 -->	<!-- CSS IMPORT FOR MAIN STYLE -->
 
    <link rel="stylesheet" href="font-awesome/4.5.0/css/font-awesome.min.css" />
	<link href="assets/css/style.css" rel="stylesheet">
	
	<!-- 4.0 FAVICON -->
	<link rel="shortcut icon" href="assets/img/logo2.png">
</head>