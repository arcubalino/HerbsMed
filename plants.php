<!DOCTYPE html>
<html lang="en">
<head>
<?php
include('session.php');
include('head.php');
include('navigation.php');
?>
</head>
  	<script src="js/jquery-2.1.1.min.js"></script>	
	
<script>	
$(document).ready(function(){
	$("#search-box").keyup(function(){
		$.ajax({
		type: "POST",
		url: "readhome.php",
		data:'keyword='+$(this).val(),
		beforeSend: function(){
			$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 410px");
		},
		success: function(data){
			$("#suggesstion-box").show();
			$("#suggesstion-box").html(data);
			$("#search-box").css("background","#FFF");
		}
		});
	});
});

function selectPlant(val) {
$("#search-box").val(val);
$("#suggesstion-box").hide();
}
</script>	

<body>
<br><br><br><br>
<center><img src="images/herbsmed_logo.png" height="150" width="200"></center>
	<div class ="wrapper">
		<div class="container-fluid">	
			<div class="row">
				
				<div class="col-sm-12 ">												
					<h2 style="color:green;"><font="futura">HerbsMed</h2></font>				     
					<form class="form-inline search-form" method="GET" action="testing.php">
						<div class="input-group">
						
							<input type="text" autocomplete="off" class="form-control" id="search-box" placeholder="search plants or ailments"name="query" required />
							
							<span class="input-group-btn">
						  	<button type="submit" id="myBtn" class="btn"><span class="search-icon"></span></button>
							</span>
						</div>
						<div id="suggesstion-box"></div>
						
					</form>
					

				</div>
			</div>
			
		</div>
	</div>
	
 
	<!-- end wrapper -->

	<!-- 7.0 javascript import -->
	<!-- jquery -->
	<script src="js/jquery.min.js"></script>
	<!-- bootstrap -->
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<!-- wow -->
	<script src="assets/js/wow/dist/wow.min.js"></script>
	<!-- wordsrotator -->
	<script src="assets/js/wordsrotator/jquery.wordrotator.min.js"></script>
	<!-- main js -->
	<script src="assets/js/main.js"></script>
	

</body>
</html>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br>
	
<footer class="w3-container w3-black w3-center w3-margin-top">
  <center><p>© 2017 Herbsmed. All rights reserved</p></a></center>
</footer>