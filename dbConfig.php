<?php
//db details
require('db_credentials.php');
$dbHost = $db_host;
$dbUsername = $db_user;
$dbPassword = $db_password;
$dbName = $db_db;

//Connect and select the database
$db = new mysqli($dbHost, $dbUsername, $dbPassword, $dbName);

$dbh = new PDO("mysql:dbname={$dbName};host={$dbHost};port={3306}", $dbUsername, $dbPassword);

if ($db->connect_error) {
    die("Connection failed: " .$db->connect_error);
}

if(!$dbh){

      echo "unable to connect to database";
   }
?>
