<?php 
include('session.php');
require_once("web.config.php"); //global variables for database access
require_once("web.config.php"); //global variables for database access
include('dbConfig.php');
if (isset($_COOKIE['plant_id']))
{
	$query= $_COOKIE['plant_id'];
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>HerbsMed</title>
    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"> 
    <script src="vendor/jquery/jquery.min.js"></script>
    <script>
	
	<!-- function for changing value of modal to show , called at body on load -->
	function cook() 
	{
  $('#userModal1').modal('show');
	}
	
	<!-- function for changing value of modal to show -->
	
	$(document).on('click', '.delete', function(){
		var plant_id = $(this).attr("id");
		if(confirm("Are you sure you want to delete this?"))
		{
			$.ajax({
				url:"delete_plant.php",
				method:"POST",
				data:{plant_id:plant_id},
				success:function(data)
				{
					alert(data);
					dataTable.ajax.reload();
				}
			});
		}
		else
		{
			return false;	
		}
	});
	
	
	$(document).on('click', '.comment', function(){
		var plant_id = $(this).attr("id");
		$.ajax({
			url:"function_commentret.php",
			method:"POST",
			data:{plant_id:plant_id},
			dataType:"json",
			success:function()
			{
				/*$('#userModal1').modal('show');
				
				*/
				
				location.reload();
								
			}
		})
	});
	</script>
<style type="text/css">
.glyphicon{font-size: 20px;}
a.glyphicon{text-decoration: none;}
a.glyphicon-trash{margin-left: 10px;}
.none{display: none;}


p#out
{
	width:100%;
    direction:ltr;
    display:block;
    max-width:100%;
	min-height:100px;
    line-height:1.5;
    padding:15px 15px 30px;
    border-radius:5px;
    border:1px solid #F7E98D;
    font:16px Tahoma, cursive;
    transition:box-shadow 0.5s ease;
    box-shadow:0 4px 6px rgba(0,0,0,0.1);
    font-smoothing:subpixel-antialiased;
    background:linear-gradient(#F9EFAF, #F7E98D);
    background:-o-linear-gradient(#F9EFAF, #F7E98D);
    background:-ms-linear-gradient(#F9EFAF, #F7E98D);
    background:-moz-linear-gradient(#F9EFAF, #F7E98D);
    background:-webkit-linear-gradient(#F9EFAF, #F7E98D);
}
</style>

</head>

<body <?php if (isset($_COOKIE['plant_id'])){  echo "onload='cook()'";  setcookie("plant_id","",time() - 3600);} ?> > 
<!-- //check onload if cookie is set-> run cook() function for changing value of modal1 for view -> delete cookie by setting it to older date//
 -->
    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="admin.php">HerbsMed</a>
            </div>
            <!-- /.navbar-header -->

            <!-- /.navbar-top-links -->

           <?php require('admin_side.php');?>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Herbal Plants</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            List of Herbal Plants
							<div align="left">
									<button type="button" id="add_button" data-toggle="modal" data-target="#userModal" class="btn btn-info">Add</button>
							</div>
					
						<div id="userModal" class="modal fade">
							<div class="modal-dialog">
								<form method="post" id="user_form" enctype="multipart/form-data">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">Add Plant</h4>
										</div>
										<div class="modal-body">
											<label>Enter Plant Name</label>
											<input type="text" name="plant_name" id="plant_name" class="form-control" />
											<br />
											<label>Enter Description</label>
											<textarea type="text" name="desc" id="desc" class="form-control"></textarea>
											<br />
											<label>Enter Scientific Name</label>
											<input type="text" name="sci_name" id="sci_name" class="form-control"/>
											<br />
											<label>Enter Common Name</label>
											<input type="text" name="common_name" id="common_name" class="form-control"/>
											<br />											
											<label>Select Plant Image</label>
											<input type="file" name="user_image" id="user_image" />
											<span id="user_uploaded_image"></span>
										</div>
										<div class="modal-footer">
											<input type="hidden" name="operation" id="operation" />
											<input type="submit" name="action" id="action" class="btn btn-success" value="Add" />
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										</div>
									</div>
								</form>
							</div>
						</div>	
                        <div id="userModal1" class="modal fade" >
                        <div class="modal-dialog">
                        <div class="modal-content">
                        	<div class="modal-header">
                         	<h4 class="modal-title">Comments</h4>
                            </div>
                            
                                            <div class="modal-footer">
                                            <?php echo $query;
                                             include('function_comment_admin.php'); ?>
                                            <button type="button" class="btn btn-default" data-dismiss="modal" onClick="">Close</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal" onClick="">Close</button>
										   </div>  
                            </div>                  
                        </div>
                        </div>					
                        </div>
							
                      <!-- /.panel-heading -->
                        <div class="panel-body">
								<div class="table-responsive">
								
								<table id="user_data" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th width="10%">Image</th>
											<th width="20%">Plant Name</th>
											<th width="35%">Description</th>
											<th width="35%">Common Name</th>
											<th width="35%">Scientific Name</th>
                                            <th width="10%">comments</th>
											<th width="10%">Edit</th>
											<th width="10%">Delete</th>
                                            
										</tr>
									</thead>
								</table>
								
							</div>							
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->             
            </div>
            <!-- /.row -->      
        </div>
        <!-- /#page-wrapper -->

  

  

</body>


</html>
<script type="text/javascript" language="javascript" >
$(document).ready(function(){
	$('#add_button').click(function(){
		$('#user_form')[0].reset();
		$('.modal-title').text("Add Plant");
		$('#action').val("Add");
		$('#operation').val("Add");
		$('#user_uploaded_image').html('');
	});
	
	var dataTable = $('#user_data').DataTable({
		"processing":true,
		"serverSide":true,
		"order":[],
		"ajax":{
			url:"fetch_plant.php",
			type:"POST"
		},
		"columnDefs":[
			{
				"targets":[0, 3, 4],
				"orderable":false,
			},
		],

	});

	$(document).on('submit', '#user_form', function(event){
		event.preventDefault();
		var plantName = $('#plant_name').val();
		var desc = $('#desc').val();
		var sci_name = $('#sci_name').val();
		var common_name = $('#common_name').val();	
		var extension = $('#user_image').val().split('.').pop().toLowerCase();
		if(extension != '')
		{
			if(jQuery.inArray(extension, ['gif','png','jpg','jpeg']) == -1)
			{
				alert("Invalid Image File");
				$('#user_image').val('');
				return false;
			}
		}	
		if(plantName != '' && desc != '' &&sci_name!='' &&common_name!='')
		{
			$.ajax({
				url:"insert_plant.php",
				method:'POST',
				data:new FormData(this),
				contentType:false,
				processData:false,
				success:function(data)
				{
					alert(data);
					$('#user_form')[0].reset();
					$('#userModal').modal('hide');
					dataTable.ajax.reload();
				}
			});
		}
		else
		{
			alert("Both Fields are Required");
		}
	});
	
	$(document).on('click', '.update', function(){
		var plant_id = $(this).attr("id");
		$.ajax({
			url:"fetch_single.php",
			method:"POST",
			data:{plant_id:plant_id},
			dataType:"json",
			success:function(data)
			{
				$('#userModal').modal('show');
				$('#plant_name').val(data.plant_name);
				$('#sci_name').val(data.sci_name);
				$('#common_name').val(data.common_name);				
				$('#desc').val(data.description);			
				$('.modal-title').text("Edit Plant");
				$('#plant_id').val(plant_id);
				$('#user_uploaded_image').html(data.user_image);
				$('#action').val("Edit");
				$('#operation').val("Edit");
			}
		})
	});
	
	
	
});
</script>
 
  <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>
    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->






