<?php
  require("db_credentials.php"); //global variables for database access
  require_once("web.config.php"); //global variables for database access
  include('dbConfig.php');
  include('session.php');
  include('head.php');
  include('navigation.php');
  if (isset($_GET["indicator"]))
  {
  $indicator=$_GET["indicator"];
  }
  else
  {
	  $indicator=0;
  }
?>
 <br><br><br>
 <?php
  
    mysql_connect($db_host, $db_user, $db_password) or die("Error connecting to database: ".mysql_error());
    mysql_select_db($db_db) or die(mysql_error());
    
?>
<html>
<head>
    <title>Search results</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="css/style5.css"/>
	<link href="css/templatemo_style.css" rel="stylesheet" type="text/css" />	 
	<script src="js/jquery-2.1.1.min.js"></script>
  <script src="js/tinymce.min.js"></script>
	
</head>
<style> 
p#out
{
	width:100%;
    direction:ltr;
    display:block;
    max-width:100%;
	min-height:150px;
    line-height:1.5;
    padding:15px 15px 30px;
    border-radius:3px;
    border:1px solid #F7E98D;
    font:16px Tahoma, cursive;
    transition:box-shadow 0.5s ease;
    box-shadow:0 4px 6px rgba(0,0,0,0.1);
    font-smoothing:subpixel-antialiased;
    background:linear-gradient(#F9EFAF, #F7E98D);
    background:-o-linear-gradient(#F9EFAF, #F7E98D);
    background:-ms-linear-gradient(#F9EFAF, #F7E98D);
    background:-moz-linear-gradient(#F9EFAF, #F7E98D);
    background:-webkit-linear-gradient(#F9EFAF, #F7E98D);
}
	
	textarea#note {
    width:100%;
    direction:ltr;
    display:block;
    max-width:100%;
    line-height:1.5;
    padding:15px 15px 30px;
    border-radius:3px;
    border:1px solid #F7E98D;
    font:16px Tahoma, cursive;
    transition:box-shadow 0.5s ease;
    box-shadow:0 4px 6px rgba(0,0,0,0.1);
    font-smoothing:subpixel-antialiased;
    background:linear-gradient(#F9EFAF, #F7E98D);
    background:-o-linear-gradient(#F9EFAF, #F7E98D);
    background:-ms-linear-gradient(#F9EFAF, #F7E98D);
    background:-moz-linear-gradient(#F9EFAF, #F7E98D);
    background:-webkit-linear-gradient(#F9EFAF, #F7E98D);
}
	
	
	input[type=text] {
    width: 800px;
    box-sizing: border-box;
    border: 2px solid #ccc;
    border-radius: 4px;
    font-size: 16px;
	color: black;
    background-color: white;
    padding: 12px 20px 12px 40px;
    

}

#plant-list{  
	position: fixed;
	background-color: #F8F8FF;
	color: black;
	text-align: left;
	left:250px;
    right:299px;
	
	
}

</style>
<body>
<script>

$(document).ready(function(){
	$("#search-box").keyup(function(){
		$.ajax({
		type: "POST",
		url: "readhome.php",
		data:'keyword='+$(this).val(),
		beforeSend: function(){
			$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 820px");
		},
		success: function(data){
			$("#suggesstion-box1").show();
			$("#suggesstion-box1").html(data);
			$("#search-box").css("background","#FFF");
		}
		});
	});
});

function selectPlant(val) {
$("#search-box").val(val);
$("#suggesstion-box1").hide();
}


function myMap() {
var mapProp= {
    center:new google.maps.LatLng(51.508742,-0.120850),
    zoom:5,
};
var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
}

</script>	


<?php
    
	$query = $_GET['query'];

    // gets value sent over search form
     
    $min_length = 1;
    // you can set minimum length of the query if you want
     
    if(strlen($query) >= $min_length){ // if query length is more or equal minimum length then
         
        $query = htmlspecialchars($query); 
        // changes characters used in html to their equivalents, for example: < to &gt;
         
        $query = mysql_real_escape_string($query);
        // makes sure nobody uses SQL injection
         
        $raw_results = mysql_query("SELECT * FROM plants
            WHERE (`plant_name` LIKE '%".$query."%') OR (`plant_name` LIKE '%".$query."%') AND ( `status` = 0) ") or die(mysql_error());   
       ?>
     
<div id="doc">
  <div id="hd"">
    <div id="header">
	<h1>
	<form method="GET" action="testing.php">
			<input type="text"  placeholder="Search.." autocomplete="off" name="query" id="search-box" style="color:black;" required/>	    
			<button type="submit" id="myBtn" class="btn"><span class="search-icon"></span></button>			
			<div style="text-align:left;" id="suggesstion-box1"></div>			
	</form>
	</h1>	
	</div>
  </div>
  <br>
  <div id="bd">
    <div id="yui-main">
      <div class="yui-b">
        <div class="yui-ge">
		<?php
        if(mysql_num_rows($raw_results) > 0){ // if one or more rows are returned do following
             
            while($results = mysql_fetch_array($raw_results)){
            // $results = mysql_fetch_array($raw_results) puts data from database into array, while it's valid it does the loop
           ?>
		   
<div class="yui-u first">
            
  <ul class="nav nav-tabs">
    <input type="hidden" id="what_plant_id" value="<?php echo $results['plant_id']; ?>">
  	<li class="active"><a data-toggle="tab" href="#home">Description</a></li>
    <li><a data-toggle="tab" href="#menu1">Usage</a></li>
    <li><a data-toggle="tab" href="#menu2">Common Name</a></li>
    
    <li><a data-toggle="tab" href="#menu3">Comments</a></li>
    
  </ul>
 	
  <div class="tab-content">
 
    <div id="home" class="tab-pane fade in active">
      <h3><?php echo $results['plant_name']; ?></h3>
	  <h5><i><b><?php echo $results['sci_name']; ?></h5></b></i>
      <p><?php echo $results['description']; ?></p>
    </div>
    <div id="menu1" class="tab-pane fade">
      <h3>Menu 1</h3>
      <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    </div>
    <div id="menu2" class="tab-pane fade">
      <h3>Menu 2</h3>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
    </div>
    <div id="menu3" class="tab-pane fade">
          <i class="fa fa-info-circle"></i> <label>Comments</label>
       
          <?php include('function_comment.php'); ?>
        
<!--modal button for commenting-->
          <br/>
      <?php if(!empty($_SESSION['sess_user_id'])){ ?>
      	       <button type="button" class="btn btn-info btn-lg" onclick="comment_process('lists','<?php echo $results['plant_id']; ?>')">Add Comment</button>
              <!-- Modal -->
           <!--    <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog"> -->
              
                  <!-- Modal content-->
                 <!--  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Write Comment</h4>
                    </div>
                    <div class="modal-body">
                      <form id= "comment_form" name="comment_form" action="comments.php" method="GET">
                          <textarea id="text_comment" name="text_comment" style="width:550px; height:300px;"></textarea>
                          <input type="hidden" id="user_id" name="user_name" value="<?php echo $login_session; ?>">
                          <input type="hidden" id="user_name" name="user_id" value="<?php echo $user_check; ?>">
                          <input type="hidden" id="plant_name" name="plant_name" value="<?php echo $results['plant_name']; ?>">
                          <input type="hidden" id="plant_id" name="plant_id" value="<?php echo $results['plant_id']; ?>">
                          <br/>
                          <input type="submit" value="Save Comment">
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
              
                </div>
              </div> -->
      <?php } ?>
<!--end of modal button for commenting-->
 
    
   </div>
  </div>          
  </div>
       <div class="yui-u">

	<img src="plant/<?php echo $results['plant_image'];?>" style="width:240px;height:295px;"></div>
          </div>
            
		<?php			          
            }
             
			}
			else{ // if there is no matching rows do following
            echo "No results";
			}
         
			}
			else{ // if query length is less than minimum
			echo "Minimum length is ".$min_length;
			}
			?>
         
        </div>
      </div>
    </div>
	<br><br>
    <div class="yui-b">
      <div id="secondary"><h3>Places Usually Found</h3>
	  <?php require_once('map.php') ?>
	 </div>
    </div>
  <br><br><br><br><br><br><br>
  <div class="yui-b">
<h3>Related Products:</h3><hr>

  <script type="text/javascript">
 
ddsmoothmenu.init({
	mainmenuid: "templatemo_menu", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

</script> 
    <div id = "product_list_here" class="row"></div>
    <br><br>
</div>	
<!-- </div> -->
 </div>
  <script type="text/javascript" src="js/transactions.js"></script>
  
  <script type="text/javascript">
    $(function(){
      product_process('related_products','');
    }) 
  </script>
	<!-- bootstrap -->
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<!-- main js -->
	<script src="assets/js/main.js"></script>
</body>
</html>

