$(document).ready(function(){
	$.ajax({
		url: "http://localhost/herbsmedv5/data.php",
		method: "GET",
		success: function(data) {
			console.log(data);
			var ailement = [];
			var user = [];

			for(var i in data) {
				ailement.push("ailement " + data[i].ailement_id);
				user.push(data[i].user);
			}

			var chartdata = {
				labels: ailement,
				datasets : [
					{
						label: 'Ailement User',
						backgroundColor: 'rgba(200, 200, 200, 0.75)',
						borderColor: 'rgba(200, 200, 200, 0.75)',
						hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
						hoverBorderColor: 'rgba(200, 200, 200, 1)',
						data: user
					}
				]
			};

			var ctx = $("#mycanvas");

			var barGraph = new Chart(ctx, {
				type: 'bar',
				data: chartdata
			});
		},
		error: function(data) {
			console.log(data);
		}
	});
});