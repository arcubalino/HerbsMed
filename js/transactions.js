console.log('Transaction JS..')
$(function(){
	$requested_page = $("#requested_page").val();

	if($requested_page!=""){
		if($requested_page!=undefined){
			console.log('Do it here')
			$(document).on("keydown", disableF5);	
		}
	}

	if($("#requested_page").length > 0 || $("#what_sess_id").val() !=""){
		setInterval(function(){
			notification_process('check','');
		},3000);
	}
})

function assessment_process(action,id){
	switch(action){
		case "admin_list":
			request_content('admin_ass');
		break;
		case "related_tutorials":
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					"action":"tutorial_lists"
				},success:function(data){
					data = $.parseJSON(data);
					for(var i=0; i < data.length; i++){
						$("#related_tutorial").prepend('<option selected="selected" value="'+data[i]['tutorial_id']+'">'+data[i]['title']+'</option>')
					}
				}
			});		
		break;

		case "select_related":
			switch(id){
				case "plant":
					$related_plant = $("#related_plant").val();
					if($related_plant == 0){
						$("#no_related").show();
					}else{
						$("#no_related").hide();
						$("#plant_name").val('')
					}
				break;

				case "tutorial":

					$related_tutorial = $("#related_tutorial").val();
					if($related_tutorial == 0){
						$("#no_tutorial").show();
					}else{
						$("#no_tutorial").hide();
						$("#video_url").val('')
					}

				break;
			}
		break;

		case "modal_add":
				modal_call();
				$("#m_content").html('<input type="hidden" id="ass_id">\n\
				<div class="form-group">\n\
					Title: <br>\n\
					<input type="text" class="form-control" id="ass_title">\n\
				</div><br>\n\
				<div class="form-group">\n\
					Related Plant: <br>\n\
					<select class="form-control" id="related_plant" onchange="assessment_process(\'select_related\',\'plant\')">\n\
						<option value="0">None of the above</option>\n\
					</select>\n\
				</div><br>\n\
				<div class="form-group" id="no_related" style="display:none;">\n\
					Plant Name: <br>\n\
					<input type="text" class="form-control" id="plant_name" style="border-color:red;"><br>\n\
				</div>\n\
				<div class="form-group">\n\
					Related Tutorial: <br>\n\
					<select class="form-control" id="related_tutorial" onchange="assessment_process(\'select_related\',\'tutorial\')">\n\
						<option value="0">None of the above</option>\n\
					</select>\n\
				</div><br>\n\
				<div class="form-group" id="no_tutorial" style="display:none;">\n\
					Video URL: <br>\n\
					<input type="text" class="form-control" id="video_url" style="border-color:red;"><br>\n\
					<small style="color:gray;">Copy <strong>ONLY</strong> the highlighted code of your Youtube Video to the URL textbox:</small>\n\
				<br><img src="images/high.png">\n\
				</div>\n\
				<div class="form-group">\n\
					Content: <br>\n\
					<textarea class="form-control" id="ass_content"></textarea>\n\
				</div><br>\n\
				<div align="right"><a href="javascript:void(0)" onclick="assessment_process(\'submit\',\'\')"><i class="fa fa-paper-plane"></i> Submit</div>');
				product_process('related_plant_list','')
				assessment_process('related_tutorials','');
		break;

		case "add_ass":
			request_content('add_ass');
			product_process('related_plant_list','')
				assessment_process('related_tutorials','');
		break;
		case "edit_ass":
			assessment_process('add_ass');
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					"action":"find_ass",
					"ass_id":id
				},success:function(data){
					data = $.parseJSON(data);
					$plant_name = data[0]['plant_name'];
					$related_plant = data[0]['related_plant'];
					$related_tutorial = data[0]['related_tutorial'];
					$tutorial = data[0]['tutorial'];
					if($related_plant == 0){
						$("#no_related").show();
						$("#plant_name").val($plant_name);
						$("#related_plant option[value=0]").attr('selected',true)
					}else{
						$("#related_plant option[value="+$related_plant+"]").attr('selected',true)
						$("#no_related").hide();
						$("#plant_name").val('');
					}

					if($related_tutorial == 0){
						$("#no_tutorial").show();
						$("#video_url").val($tutorial);
						$("#related_tutorial option[value=0]").attr('selected',true)
					}else{
						$("#related_tutorial option[value="+$related_plant+"]").attr('selected',true)
						$("#no_tutorial").hide();
						$("#video_url").val('');
					}
					
					$("#ass_id").val(id);
					$("#ass_title").val(data[0]['title']);
					$("#related_plant").val();
					$("#plant_name").val();
					$("#related_tutorial").val();
					$("#video_url").val();
					$("#ass_content").val(data[0]['content']);
				loader('off');
				}
			})
		break;
		case "remove":
			if(confirm('Are you sure to remove this Assessment?')){
				loader('on');
				$.ajax({
					url: "transactions.php",
					type: "POST",
					data:{
						"action":"remove_ass",
						"ass_id":id
					},success:function(data){
						if(data == "success"){
							$("#assessment_of_"+id).remove();
						}else{
							alert("Error!")
						}
						loader('off');
					}
				})
			}
		break;

		case "modal_edit":
			loader('on')
			assessment_process('modal_add','');
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					"action":"find_ass",
					"ass_id":id
				},success:function(data){
					data = $.parseJSON(data);
					$plant_name = data[0]['plant_name'];
					$related_plant = data[0]['related_plant'];
					$related_tutorial = data[0]['related_tutorial'];
					$tutorial = data[0]['tutorial'];
					if($related_plant == 0){
						$("#no_related").show();
						$("#plant_name").val($plant_name);
						$("#related_plant option[value=0]").attr('selected',true)
					}else{
						$("#related_plant option[value="+$related_plant+"]").attr('selected',true)
						$("#no_related").hide();
						$("#plant_name").val('');
					}

					if($related_tutorial == 0){
						$("#no_tutorial").show();
						$("#video_url").val($tutorial);
						$("#related_tutorial option[value=0]").attr('selected',true)
					}else{
						$("#related_tutorial option[value="+$related_plant+"]").attr('selected',true)
						$("#no_tutorial").hide();
						$("#video_url").val('');
					}
					
					$("#ass_id").val(id);
					$("#ass_title").val(data[0]['title']);
					$("#related_plant").val();
					$("#plant_name").val();
					$("#related_tutorial").val();
					$("#video_url").val();
					$("#ass_content").val(data[0]['content']);
				loader('off');
				}
			})
		break;

		case "submit":
			$sess_role = $("#what_sess_role").val();
			$ass_id = $("#ass_id").val();
			$ass_title = $("#ass_title").val();
			$related_plant = $("#related_plant").val();
			$plant_name = $("#plant_name").val();
			$related_tutorial = $("#related_tutorial").val();
			$video_url = $("#video_url").val();
			$ass_content = $("#ass_content").val();

			loader('on');

			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					'action': "submit_ass",
					'ass_id' : $ass_id,
					'ass_title' : $ass_title,
					'related_plant' : $related_plant,
					'plant_name' : $plant_name,
					'related_tutorial' : $related_tutorial,
					'video_url' : $video_url,
					'ass_content' : $ass_content,
				},success:function(data){
					console.log(data)
					if(data == "success"){
						if($sess_role!="admin"){
							location.reload();
						}else{
							loader('off');
							assessment_process('admin_list','');
						}
					}else{
						loader('off')
						alert("Error!");
					}
				}
			})

		break;

		case "lists":
			$sess_role = $("#what_sess_role").val();
			$(".appended_assessment").remove();
			loader('on');
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					"action":"assessment_list"
				},success:function(data){
					data = $.parseJSON(data)
					if($sess_role == "admin"){
						$what_ass = "edit_ass";
					}else{
						$what_ass = "modal_edit";
					}
					if(data.length > 0){
						for(var i=0; i < data.length; i++){
							$ass_id = data[i]['ass_id'];
							$title = data[i]['title'];
							$related_plant = data[i]['related_plant'];
							$plant_name = data[i]['plant_name'];
							$related_tutorial = data[i]['related_tutorial'];
							$tutorial = data[i]['tutorial'];
							$content = data[i]['content'];
							$created_at = data[i]['created_at'];
							$video = data[i]['video'];
							$name_plant = data[i]['name_plant'];
							$plant_image = data[i]['plant_image'];
							$image_location = data[i]['image_location'];
							$mem_image = data[i]['mem_image'];
							$ownership = data[i]['ownership'];

							if($ownership == "edit"){
								$what_option = '<a href="javascript:void(0)" style="color:green;font-weight:bold;" onclick="assessment_process(\''+$what_ass+'\',\''+$ass_id+'\')"><i class="fa fa-pencil-square-o"></i> Edit</a>&nbsp;&nbsp;<a href="javascript:void(0)" style="color:red;font-weight:bold;" onclick="assessment_process(\'remove\',\''+$ass_id+'\')"><i class="fa fa-times"></i> Remove</a>';
							}else{	
								$what_option = "";
							}

							if($plant_image == ""){
								$plant_image = "images/no_image.png";
							}else{
								$plant_image = "plant/"+$plant_image;
							}

							if($mem_image == ""){
								$what_image = "images/no_image.png"
							}else{
								$what_image = "images/"+$image_location+"/"+$mem_image;
							}

							if($related_plant > 0){
								$what_plant = "<table><tr><td valign='top'><img src='"+$plant_image+"' style='width:100px;height:100px;'></td><td valign='top'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td valign='top' style='padding-top:10px;'><a href='http://localhost/herbsmed/testing.php?query="+$name_plant+"' style='color:green;font-weight:bold;' target='_blank'>"+$name_plant+"</a></td></tr></table>";
							}else{
								$what_plant = data[i]['plant_name'];
							}
							
								if($related_tutorial == 0){
									$what_video = "<center><iframe width='560' height='315' frameborder='0' src='https://www.youtube.com/embed/"+$tutorial+"'></iframe><br>";
								}
								else if($related_tutorial > 0){
									$what_video = "<center><iframe width='560' height='315' frameborder='0' src='https://www.youtube.com/embed/"+$video+"'></iframe><br>";
								}else{
									$what_video = "";
								}

							$("#assessment_here").append('<ul class="list-group appended_assessment" id="assessment_of_'+$ass_id+'">\n\
							<li class="list-group-item">\n\
								<div class="row">\n\
									<div class="col-md-9"><h4 style="color:gray;" id="tutorial_title_of_2">'+$title+'</h4></div>\n\
									<div align="right" class="col-md-3">\n\
										<small><i>'+$created_at+'</i></small>\n\
									</div>\n\
								</div><hr>\n\
								<div class="row">\n\
									<div class="col-md-12" align="center">'+$what_video+'</div>\n\
									<div class="col-md-12"><br>'+$content+'</div>\n\
								</div>\n\
								<br><br><div class="row">\n\
									<div class="col-md-2"><small><i>Related Plant:</i></small></div>\n\
									<div class="col-md-6">'+$what_plant+'</div>\n\
								</div>\n\
								<br><br><div class="row">\n\
									<div class="col-md-6"><small><i>By:<br>\n\
									<br><img src="'+$what_image+'" style="width:50x;height:50px;"><br>\n\
									<br><span><i>'+data[i]['mem_fname']+' '+data[i]['mem_lname']+'</span></i></small></div>\n\
								</div>\n\
								<div align="right">'+$what_option+'</div>\n\
							</li></ul><hr>')
						}
					}else{
							$("#assessment_here").html('<small><i>No record found</i></small>');
					}
					loader('off');
				}
			})
		break;
	}
}

function product_process(action,id){
	switch(action){
		case "specific_product":
			modal_call();
			
		break;
		case "related_products":
			$(".appended_related_products").remove();
			$plant_id = $("#what_plant_id").val();
			loader('on');
			$.ajax({
				url:"transactions.php",
				type: "POST",
				data:{
					"action":"related_products",
					"plant_id":$plant_id
				},success:function(data){
					data = $.parseJSON(data);
					console.log(data);
					if(data.length > 0){
						for(var i=0; i < data.length; i++){
							$("#product_list_here").append('<div class="col-md-3 appended_related_products" align="center"><a rel="lightbox" href="javascript:void(0)" onclick="product_process(\'specific_product\',\''+data[i]['product_id']+'\')"><img class="cloudcarousel" src="images/products/'+data[i]['product_image']+'" alt="" height="180" width="240"/><div style="padding-top:10px;font-weight:bold;">'+data[i]['product_name']+'</div></a></div>')
						}
					}else{	
						$("#product_list_here").hide();
					}
				}
			})
		break;
		case "for_approvals":
			 product_process('lists','')
		break;
		case "admin_list":
			request_content('product_approval');
		break;
		case "select_related":
			$related_plant = $("#related_plant").val();
			if($related_plant == 0){
				$("#no_related").show();
			}else{
				$("#no_related").hide();
				$("#plant_name").val('')
			}
		break;
	
		case "remove":
			if(confirm('Are you sure to remove this product?')){
				loader('on');
				$.ajax({
					url: "transactions.php",
					type: "POST",
					data:{
						"action":"remove_product",
						"product_id":id
					},success:function(data){
						console.log(data);
						if(data == "success"){
							$("#product_of_"+id).remove();
						}else{
							alert("Error!");
						}
						loader('off')
					}
				})
			}
		break;

		case "submit":
			loader('on');

				$file_upload = $("#file_upload").prop('files')[0];
				if(!$file_upload){$upload_stat='dont_upload';}else{$upload_stat='upload';}

				var data_post = new FormData();	
				$product_id = $("#product_id").val();
				$product_name = $("#product_name").val();
				$plant_name = $("#plant_name").val();
				$product_desc = $("#product_desc").val();
				$related_plant = $("#related_plant").val();

				data_post.append('action',"submit_product");
				data_post.append('product_id',$product_id);
				data_post.append('product_name',$product_name);
				data_post.append('plant_name',$plant_name);
				data_post.append('product_desc',$product_desc);
				data_post.append('related_plant',$related_plant);
				data_post.append('upload_status',$upload_stat);
				data_post.append('file_upload',$file_upload);

				$.ajax({
					url: "transactions.php",
					type: "POST",
					processData: false,
					contentType: false,
					data: data_post,
					success:function(data){
						console.log(data);
						if(data == "success"){
							location.reload();
						}else{
							alert("Error!");
							loader('off');
						}
					}
				});
		break;
		case "modal_add":
			loader('on');

				modal_call();
				$("#m_content").html('<input type="hidden" id="product_id"><div class="form-group">\n\
					Image:<br>\n\
					<input type="file" id="file_upload">\n\
				</div><br>\n\
				<div class="form-group">\n\
					Name: <br>\n\
					<input type="text" class="form-control" id="product_name">\n\
				</div><br>\n\
				<div class="form-group">\n\
					Related Plant: <br>\n\
					<select class="form-control" id="related_plant" onchange="product_process(\'select_related\',\'\')">\n\
						<option value="0">None of the above</option>\n\
					</select>\n\
				</div><br>\n\
				<div class="form-group" id="no_related" style="display:none;">\n\
					Plant Name: <br>\n\
					<input type="text" class="form-control" id="plant_name"><br>\n\
				</div>\n\
				<div class="form-group">\n\
					Product Description: <br>\n\
					<textarea class="form-control" id="product_desc"></textarea>\n\
				</div><br>\n\
				<div align="right"><a href="javascript:void(0)" onclick="product_process(\'submit\',\'\')"><i class="fa fa-paper-plane"></i> Submit</div>');
				product_process('related_plant_list','')
			
		break;
		case "related_plant_list":
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					'action':"related_plants"
				},success:function(data){
					data = $.parseJSON(data);
					for(var i=0; i < data.length; i++){
						$("#related_plant").prepend('<option value="'+data[i]['plant_id']+'" selected="selected">'+data[i]['plant_name']+'</option>')
					}
					loader('off');
				}
			})
		break;
		case "modal_edit":
			loader('on')
			product_process('modal_add',id);
			$plant_name = $("#plant_name_product_of_"+id).html();
			$related_plant = $("#related_plant_of_"+id).val();
			$product_name = $("#name_of_product_"+id).html();
			$product_desc = $("#desc_of_product_"+id).html();
			$("#product_name").val($product_name);
			$("#plant_name").val($plant_name);
			$("#product_desc").val($product_desc);
			$("#product_id").val(id);
			$("#related_plant option[value="+$related_plant+"]").attr('selected',true)

			console.log($related_plant)
			if($related_plant == 0){
				$("#no_related").show();
			}
			loader('off');
		break;

		case "approve":
			if(confirm("Are you sure to approve this product?")){
				loader('on');
				$.ajax({
					url: "transactions.php",
					type: "POST",
					data:{
						"action":"admin_approve",
						"product_id":id
					},success:function(data){
						if(data == "success"){
							$("#product_of_"+id).remove();
						}else{
							alert("Error!");
						}
						loader('off')
					}
				})
			}
		break;
		case "admin_approve":
			request_content('approved_list');
		break;
		case "approved_list":
					$(".appended_products").remove();
			loader('on');
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					'action':"approve_products",
				},success:function(data){
					data = $.parseJSON(data);
					if(data.length > 0){
						for(var i=0; i < data.length; i++){
							
							$product_id = data[i]['product_id'];

							$image = 'images/products/'+data[i]['product_image']+'';
							if($image == ""){
								$imgx = "images/no_image.png";
							}else{
								$imgx = $image;
							}

							$status = data[i]['status'];
							if($status == 0){
								$w_stat = "<strong style='color:green;'>Approved</strong>";
							}else{
								$w_stat = "<strong style='color:red;'>Pending</strong>";
							}

							$related = data[i]['related_plant'];
							if($related == 0){
								$plantx = "<span id='plant_name_product_of_"+$product_id+"'>"+data[i]['plant_name']+"</span>";
							}else{
								$plantx = "<center><img src='plant/"+data[i]['plant_image']+"' style='width:50px;height:50px;'><div style='padding-top:10px;'><span id='plant_name_product_of_"+$product_id+"'>"+data[i]['name_plant']+"</span></div></center>";
							}

							$("#products_here").append('<tr class="appended_products" id="product_of_'+data[i]['product_id']+'">\n\
								<td><input type="hidden" id="related_plant_of_'+$product_id+'" value="'+data[i]['related_plant']+'"><center><img src="'+$imgx+'" style="width:100px;height:100px;"></center></td>\n\
					            <td><center id="name_of_product_'+$product_id+'">'+data[i]['product_name']+'</center></td>\n\
					            <td><center id="desc_of_product_'+$product_id+'">'+data[i]['product_desc']+'</center></td>\n\
					            <td><center>'+$plantx+'</center></td>\n\
					            <td><center>'+$w_stat+'</center></td>\n\
							</tr>');
						}
					}
					loader('off');
				}
			})
		break;
		case "my_list":
			$what_role = $("#what_sess_role").val()
			$(".appended_products").remove();
			loader('on');
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					'action':"product_list",
				},success:function(data){
					data = $.parseJSON(data);
					if(data.length > 0){
						for(var i=0; i < data.length; i++){
							
							$product_id = data[i]['product_id'];

							if($what_role == "admin"){
								$what_act = '<a href="javascript:void(0)" style="color:green;font-weight:bold;" onclick="product_process(\'approve\',\''+$product_id+'\')"><i class="fa fa-check"></i> Click to Approve</a>';
							}else if($what_role == "health"){
								$what_act = '<a href="javascript:void(0)" style="color:green;font-weight:bold;" onclick="product_process(\'modal_edit\',\''+$product_id+'\')"><i class="fa fa-pencil-square-o"></i> Edit</a>&nbsp;&nbsp;<a href="javascript:void(0)" style="color:red;font-weight:bold;" onclick="product_process(\'remove\',\''+$product_id+'\')"><i class="fa fa-times"></i> Remove</a>';
							}

							$image = 'images/products/'+data[i]['product_image']+'';
							if($image == ""){
								$imgx = "images/no_image.png";
							}else{
								$imgx = $image;
							}

							$status = data[i]['status'];
							if($status == 0){
								$w_stat = "<strong style='color:green;'>Approved</strong>";
							}else{
								$w_stat = "<strong style='color:red;'>Pending</strong>";
							}

							$related = data[i]['related_plant'];
							if($related == 0){
								$plantx = "<span id='plant_name_product_of_"+$product_id+"'>"+data[i]['plant_name']+"</span>";
							}else{
								$plantx = "<center><img src='plant/"+data[i]['plant_image']+"' style='width:50px;height:50px;'><div style='padding-top:10px;'><span id='plant_name_product_of_"+$product_id+"'>"+data[i]['name_plant']+"</span></div></center>";
							}

							$("#products_here").append('<tr class="appended_products" id="product_of_'+data[i]['product_id']+'">\n\
								<td><input type="hidden" id="related_plant_of_'+$product_id+'" value="'+data[i]['related_plant']+'"><center><img src="'+$imgx+'" style="width:100px;height:100px;"></center></td>\n\
					            <td><center id="name_of_product_'+$product_id+'">'+data[i]['product_name']+'</center></td>\n\
					            <td><center id="desc_of_product_'+$product_id+'">'+data[i]['product_desc']+'</center></td>\n\
					            <td><center>'+$plantx+'</center></td>\n\
					            <td><center>'+$w_stat+'</center></td>\n\
					            <td>'+$what_act+'</td>\n\
							</tr>');
						}
					}
					loader('off');
				}
			})
		break;
	}
}

function tutorial_process(action,id){
	switch(action){
		case "create":
			modal_call();
			$("#m_content").html('<input type="hidden" id="tutorial_id"><div class="form-group">\n\
				<label>Title:</label>\n\
				<input type="text" id="title" class="form-control">\n\
			</div><br>\n\
			<div class="form-group">\n\
				<label>Youtube Video Link:</label>\n\
				<input type="text" id="video" class="form-control"><br>\n\
				<small style="color:gray;">Copy <strong>ONLY</strong> the highlighted code of your Youtube Video to the URL textbox:</small>\n\
				<br><img src="images/high.png">\n\
			</div><br>\n\
			<div class="form-group">\n\
				<label>Content:</label>\n\
				<textarea id="content" class="form-control"></textarea>\n\
			</div><br>\n\
			<div align="right">\n\
				<a href="javascript:void(0)" onclick="tutorial_process(\'submit\',\'\')"><i class="fa fa-paper-plane"></i>\n\ SAVE</a>\n\
			</div>');
		break;
		case "edit":
			loader('on')
			tutorial_process('create','')
			$video = $("#tutorial_video_of_"+id).html();
			$title = $("#tutorial_title_of_"+id).html();
			$content = $("#tutorial_content_of_"+id).html();

			$("#tutorial_id").val(id);
			$("#title").val($video);
			$("#video").val($title);
			$("#content").val($content);
			loader('off');
		break;
		case "submit":
			$title = $("#title").val();
			$video = $("#video").val();
			$content = $("#content").val();
			$tutorial_id = $("#tutorial_id").val();

			if($tutorial_id == ""){
				$action = "create_tutorial";
			}else{
				$action = "update_tutorial";
			}
			loader('on');
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					"action":$action,
					"video":$video,
					"title":$title,
					"tutorial_id":$tutorial_id,
					"content":$content
				},success:function(data){
					console.log(data);
					if(data == "success"){
						tutorial_process('lists','');
						alert('Successfully saved tutorial!');
						$("#title").val('');
						$("#video").val('');
						$("#content").val('');
						location.reload();
					}else{
						loader('off');
						alert("Error!");
					}

				}	
			});
		break;

		case "remove":
			if(confirm('Are you sure to remove this tutorial?')){
				loader('on');
				$.ajax({
					url: "transactions.php",
					type: "POST",
					data:{
						"action":"remove_tutorial",
						"tutorial_id":id
					},success:function(data){
						if(data == "success"){
							$("#tutorial_of_"+id).remove();
						}else{
							alert("Error!");
						}
						loader('off');
					}
				})
			}
		break;

		case "lists":
			loader('on');
			$(".appended_tutorial").remove();
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					"action":"tutorial_lists"
				},success:function(data){
					data = $.parseJSON(data);
					for(var i=0; i < data.length; i++){
						$image = data[i]['mem_image'];
						$video = data[i]['video'];
						$content = data[i]['content'];

						if($image == ""){
							$what_image = "images/no_image.png";
						}else{
							$what_image = "images/"+data[i]['image_location']+"/"+data[i]['mem_image'];
						}

						if($video == ""){
							$what_video = "<small style='color:gray;'><i>No video found</i></small>";
						}else{
							$what_video = "<center><iframe width='560' height='315' frameborder='0' src='https://www.youtube.com/embed/"+$video+"'></iframe><br>\n\
							<small style='color:gray;'><i>https://www.youtube.com/embed/<strong id='tutorial_video_of_"+data[i]['tutorial_id']+"'>"+$video+"</strong></i></small></center>";
						}

						if($content == ""){
							$what_content = "";
						}else{
							$what_content = "<p id='tutorial_content_of_"+data[i]['tutorial_id']+"'>"+$content+"</p>";
						}

						if(data[i]['ownership'] == "edit"){
							$what_option = "<a href='javascript:void(0)' style='color:green;' onclick='tutorial_process(\"edit\",\""+data[i]['tutorial_id']+"\")'><i class='fa fa-pencil-square-o'></i> Edit</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(0)' style='color:red;' onclick='tutorial_process(\"remove\",\""+data[i]['tutorial_id']+"\")'><i class='fa fa-pencil-square-o'></i> Remove</a>";
						}else{
							$what_option = "";
						}
						$("#tutorial_list").append('<ul class="list-group appended_tutorial" id="tutorial_of_'+data[i]['tutorial_id']+'">\n\
							<li class="list-group-item">\n\
								<div class="row">\n\
									<div class="col-md-9"><h4 style="color:gray;" id="tutorial_title_of_'+data[i]['tutorial_id']+'">'+data[i]['title']+'</h4></div>\n\
									<div align="right" class="col-md-3">\n\
										<small><i>'+data[i]['created_at']+'</i></small>\n\
									</div>\n\
								</div><br>\n\
								<div class="row"><div class="col-md-12">'+$what_video+'<br>'+$what_content+'</div></div><hr>\n\
								<div class="row">\n\
									<div class="col-md-9">\n\
											<img src="'+$what_image+'" style="width:50px;height:50px;">\n\
											<br><br>\n\
											<span><small><i>By: '+data[i]['mem_fname']+' '+data[i]['mem_lname']+'</i></small></span>\n\
									</div>\n\
									<div class="col-md-3" align="right">'+$what_option+'</div>\n\
								</div>\n\
							</li>\n\
						</ul><hr>')
					}
				}
			})
		break;
	}
}

function message_process(action,id){
	switch(action){
		case "users_message":
			$("html, body").animate({ scrollTop: $(document).height() }, 500);
			$sess_id = $("#what_sess_id").val();
			$("#msg_reply").html('');
			$(".appended_message").remove();
			$owner = $("#message_of_"+id).html();
			$("#whos_message").html('Your conversation with '+$owner+":");
			loader('on');
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					"action":"users_message",
					"user_id":id
				},success:function(data){
					data = $.parseJSON(data);
					for(var i=0; i < data.length; i++){
						$receiver = data[i]['receiver'];

						if($receiver==$sess_id){
							$align = "left";
							$what_name = "You";
						}else{
							$align="right"
							$what_name = $("#message_of_"+id).html();
						}
						$("#message_list").append('<div align="'+$align+'" style="padding-right:10px;"><li class="list-group-item">\n\
							<div class="row">\n\
								<div class="col-md-12">\n\
									<small style="color:gray;"><i>'+data[i]['created_at']+'</i></small>\n\
								</div>\n\
							</div>\n\
							<div class="row">\n\
								<div class="col-md-12">\n\
									'+data[i]['content']+'\n\
								</div>\n\
							</div>\n\
							<div class="row">\n\
								<div class="col-md-12">\n\
									<small style="color:gray;"><em>- '+$what_name+'</em></small>\n\
								</div>\n\
							</div>\n\
							</li></div><hr>');
					}
					// <ul class="list-group appended_message">\n\
					// 		<li class="list-group-item">\n\
					// 			<div align="right"><small><i>'+data[i]['created_at']+'</i></small></div>\n\
					// 			<div class="row"><div class="col-md-12">'+data[i]['content']+'</div></div>\n\
					// 		</li>\n\
					// 	</ul><hr>
					$("#msg_reply").html('<div class="row">\n\
					<div class="col-md-9">\n\
							<input type="hidden" id="prof_id" value="'+id+'"><textarea class="form-control" placeholder="Enter message.." id="prof_message"></textarea>\n\
						</div>\n\
						<div class="col-md-3" style="padding-top:10px;">\n\
								<a onclick="health_prof_process(\'submit_message\',\'conversation\')" href="javascript:void(0)"><i class="fa fa-paper-plane"></i> SEND MESSAGE</a>\n\
						</div>\n\
					</div><div class="row" style="padding-top:10px;"><div class="col-md-12" id="msg_result"><div>')
					loader('off');
				}
			})
		break;

		case "contact_list":
			$sess_id = $("#what_sess_id").val();
			$(".appended_contact_list").remove();
			loader('on');
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					"action":"message_contact_list"
				},success:function(data){
					data = $.parseJSON(data);
					console.log(data);
					for(var i=0; i < data.length; i++){	
						$mem_id = data[i]['mem_id'];

						if($mem_id !=$sess_id){
							if(data[i]['mem_image'] == ""){
							$what_image = "images/no_image.png";
							}else{
								$what_image = 'images/'+data[i]['image_location']+'/'+data[i]['mem_image']+'';
							}

								$("#contact_list").append('<a href="javascript:void(0)" style="text-decoration:none;" onclick="message_process(\'users_message\',\''+data[i]['mem_id']+'\')"><ul class="list-group appended_contact_list">\n\
					                <li class="list-group-item">\n\
					                	<div align="center"><img src="'+$what_image+'" id="image_of_'+data[i]['mem_id']+'" style="width:50px;50px;">\n\
					                	<br><br><span id="message_of_'+$mem_id+'">'+data[i]['mem_fname']+' '+data[i]['mem_lname']+'</span>\n\
					                </div></li>\n\
					              </ul></a>')
						}
						
					}
					loader('off');
				}
			})
		break;
		case "open_message":
			loader('on');
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					'action':'open_message',
					'message_id': id
				},success:function(data){
					data = $.parseJSON(data);
					console.log(data);
					modal_call();
					$("#m_content").html('<div class="row">\n\
						<div class="col-md-3" align="center">\n\
							<img src="images/'+data[0]['image_location']+'/'+data[0]['mem_image']+'" style="width:70px;height:70px;"><br><br>\n\
							<span><small>'+data[0]['mem_fname']+' '+data[0]['mem_lname']+'</small></span>\n\
						</div>\n\
						<div class="col-md-9">\n\
							<div  align="right"><small><i>'+data[0]['created_at']+'</i></small></div>\n\
							<br>\n\
							'+data[0]['content']+'\n\
						</div>\n\
					</div>\n\
					<hr>\n\
					<div class="row">\n\
					<div class="col-md-9">\n\
							<input type="hidden" id="prof_id" value="'+data[0]['sender']+'"><textarea class="form-control" placeholder="Enter message.." id="prof_message"></textarea>\n\
						</div>\n\
						<div class="col-md-3" style="padding-top:10px;">\n\
								<a onclick="health_prof_process(\'submit_message\',\'reply\')" href="javascript:void(0)"><i class="fa fa-paper-plane"></i> REPLY</a>\n\
						</div>\n\
					</div><div class="row" style="padding-top:10px;"><div class="col-md-12" id="msg_result"><div>');
					loader('off');
				}
			})	
		break;
	}
}

function notification_process(action,id){
	switch(action){
		case "for_admin":
			request_content('notification_admin','');
		break;
		case "remove":
			if(confirm("Are you sure to remove this notification?")){
				loader('on');
				$.ajax({
					url: "transactions.php",
					type: "POST",
					data:{
						'action':"remove_notification",
						'notification_id':id
					},success:function(data){
						if(data == "success"){
							$("#notification_of_"+id).remove();
						}else{
							alert("Error!");
						}
						loader('off');
					}
				})
			}
		break;
		case "lists":
			loader('on');
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					'action':"notification_list"
				},success:function(data){
					$("#notify_num").html('0');
					data = $.parseJSON(data);
					if(data.length > 0){
						for(var i=0; i < data.length; i++){
							$image = data[i]['mem_image'];

							if($image == ""){
								$what_image = "images/no_image.png";
							}else{
								$what_image = 'images/'+data[i]['profile_location']+'/'+data[i]['mem_image']+'';
							}
							$("#notification_ul").append('<ul class="list-group well" id="notification_of_'+data[i]['notification_id']+'"><li class="list-group-item">\n\
								<div class="row">\n\
								<div class="col-md-6" style="padding-top:10px;">'+data[i]['created_at']+'</div>\n\
								<div class="col-md-6" align="right">\n\
									<a href="javascript:void(0)" style="color:red;" onclick="notification_process(\'remove\',\''+data[i]['notification_id']+'\')"><i class="fa fa-times"></i></a>\n\
								</div>\n\
							</div><hr>\n\
							<div class="row" id="review_content_of_'+data[i]['notification_id']+'">\n\
								<div class="col-md-2" align="left"><small><i>From:</i></small> <br><hr>\n\
									<img src="'+$what_image+'" style="width:70px;heigth:70px;">\n\
									<br>\n\
										<div style="padding-top:10px;"><small>'+data[i]['mem_fname']+' '+data[i]['mem_lname']+'</small></div>\n\
								</div>\n\
								<div class="col-md-10" style="padding-left:20px;">\n\
									<span id="review_content_val_of_'+data[i]['notification_id']+'">'+data[i]['content']+'</div>\n\
								</div>\n\
							</div><div align="right"><small><i>'+data[i]['notification_type']+'</i></small></div>\n\
							</li></ul>')
						}	
                			
					}
					loader('off');
				}
			})
		break;
		case "check":
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					'action':"check_notifications"
				},success:function(data){
					console.log(data);
					$("#notify_num").html(data);
				}				
			})
		break;
	}
}	

function review_process(action,id){
	switch(action){
		case "profile":
			loader('on');
			review_process('lists',id);
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					'action':'health_profile',
					'user_id': id
				},success:function(data){
					data = $.parseJSON(data);
					$prof_image = data[0]['mem_image'];
					if($prof_image == ""){
						$what_image = "images/no_image.png";
					}else{
						$what_image = "images/healthprof/"+$prof_image;
					}

					$("#prof_image").attr('src',$what_image);
					$("#prof_name").html(data[0]['mem_fname']+" "+data[0]['mem_lname'])
					$("#prof_add").html(data[0]['mem_add'])
					$("#prof_contact").html(data[0]['mem_contact'])
					$("#prof_email").html(data[0]['mem_email'])
				}
			});
		break;

		case "create":
			loader('on');
			$content = $("#review_content").val();
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					'action':'create_review',
					'content': $content,
					'user_id':id
				},success:function(data){
					if(data == "success"){
						$("#review_content").val('');
						review_process('lists',id);
					}else{
						alert('error')
					}
				}
			});

		break;

		case "remove":
			if(confirm('Are you sure to remove this review?')){
				loader('on');
				$.ajax({
					url: "transactions.php",
					type: "POST",
					data:{
						'action':"remove_review",
						'review_id':id
					},success:function(data){
						if(data == "success"){
							$("#appended_review_ul_"+id).remove();
						}else{
							alert('Error')
						}
						loader('off');
					}
				})
			}
		break;

		case "update":
			$new_content = $("#update_review_content_of_"+id).val();
			loader('on');
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					'action':'update_review',
					'review_id':id,
					'content':$new_content
				},success:function(data){
					console.log(data);
					if(data == "success"){
						review_process('cancel',id);
						$("#review_content_val_of_"+id).html($new_content)
					}else{
						alert('Error!');
					}
					loader('off')
				}
			})
		break;
		case "edit":
			$content = $("#review_content_val_of_"+id).html();
			$("#review_option_of_"+id+",#review_content_of_"+id).hide();
			$("#update_content_of_"+id+",#update_review_option_of_"+id).show();
			$("#update_review_content_of_"+id).val($content);
		break;

		case "cancel":
			$("#review_option_of_"+id+",#review_content_of_"+id).show();
			$("#update_content_of_"+id+",#update_review_option_of_"+id).hide();
		break;

		case "lists":
			$(".appended_reviews").remove();
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					'action':'review_list',
					'user_id':id
				},success:function(data){
					data = $.parseJSON(data);
					for(var i=0; i < data.length; i++){
						if(data[i]['reviewer'] == $("#what_sess_id").val()){
							$what_option = '<div class="row" id="update_content_of_'+data[i]['review_id']+'" style="display:none;">\n\
				                    <div class="col-md-12"> <textarea class="form-control" id="update_review_content_of_'+data[i]['review_id']+'" placeholder="Update your review here.."></textarea></div>\n\
							</div><br>\n\
							<div align="right" id="review_option_of_'+data[i]['review_id']+'">\n\
								<a href="javascript:void(0)" style="color:green;" onclick="review_process(\'edit\',\''+data[i]['review_id']+'\')"><i class="fa fa-pencil-square-o"></i> Edit</a>\n\
									&nbsp;&nbsp;\n\
								<a href="javascript:void(0)" style="color:red;" onclick="review_process(\'remove\',\''+data[i]['review_id']+'\')"><i class="fa fa-times"></i> Delete</a>\n\
							</div>\n\
							<div align="right" id="update_review_option_of_'+data[i]['review_id']+'" style="display:none;">\n\
								<a href="javascript:void(0)" style="color:green;" onclick="review_process(\'update\',\''+data[i]['review_id']+'\')"><i class="fa fa-pencil-square-o"></i> Update</a>\n\
									&nbsp;&nbsp;\n\
								<a href="javascript:void(0)" style="color:red;" onclick="review_process(\'cancel\',\''+data[i]['review_id']+'\')"><i class="fa fa-times"></i> Cancel</a>\n\
							</div>';
						}else{
							$what_option = "";
						}
						$("#review_ul").append('<ul class="list-group appended_reviews" id="appended_review_ul_'+data[i]['review_id']+'"><li class="list-group-item">\n\
							<div class="row">\n\
								<div class="col-md-6">&nbsp;</div>\n\
								<div class="col-md-6" align="right" style="padding-top:10px;">'+data[i]['created_at']+'</div>\n\
							</div><hr>\n\
							<div class="row" id="review_content_of_'+data[i]['review_id']+'">\n\
								<div class="col-md-4" align="left">\n\
									<img src="images/profile/'+data[i]['mem_image']+'" style="width:70px;heigth:70px;">\n\
									<br>\n\
										<div style="padding-top:10px;"><small>'+data[i]['mem_fname']+' '+data[i]['mem_lname']+'</small></div>\n\
								</div>\n\
								<div class="col-md-8" style="padding-left:20px;">\n\
									<span id="review_content_val_of_'+data[i]['review_id']+'">'+data[i]['content']+'</div>\n\
								</div>\n\
							</div>'+$what_option+'\n\
						</li></ul>');
					}
				}
			})
			loader('off');
		break;
	}
}

		// <div class="row">\n\
		// 						<div class="col-md-4">\n\
		// 							<img src="images/profile/'+data[i]['mem_image']+'">\n\
		// 						</div>\n\
		// 						<div class="col-md-8">\n\
		// 							'+data[i]['content']+'\n\
		// 						</div>\n\
		// 					</div>\n\	
function update_process(action,id){
	switch(action){	
		case "account":
			$email = $("#my_email").val();
			$username = $("#my_username").val();
			$password = $("#password").val();

			loader('on');

			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					'action':'update_account',
					'email':$email,
					'username':$username,
					'password':$password
				},success:function(data){
					console.log(data);
					if(data == "success"){
						location.reload();
					}else{
						alert('Error!');
						loader('off');
					}
				}
			})

		break;
		case "check_my_email":
			$email = id.value;
			$("#email_error").hide();
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					'action': 'check_reg_email',
					'email': $email
				},success:function(data){

					if(data == "exist"){
						$("#submit_reg").attr('disabled',true);
						$("#email_error").show();
					}else{
						$("#submit_reg").attr('disabled',false);
						$("#email_error").hide();
					}
				}
			})

		break;

		case "check_my_pass":
			$password = $("#password").val();
			$confirm_password = id.value;

			if($password!=$confirm_password){
				$("#submit_reg").attr('disabled',true);
				$("#pass_error").show();
			}else{
				$("#submit_reg").attr('disabled',false);
				$("#pass_error").hide();
			}

		break;
		case "check_my_username":
			$username = id.value;
			$("#username_error").hide();
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					'action': 'check_my_username',
					'username': $username
				},success:function(data){

					if(data == "exist"){
						$("#submit_reg").attr('disabled',true);
						$("#username_error").show();
					}else{
						$("#submit_reg").attr('disabled',false);
						$("#username_error").hide();
					}
				}
			})

		break;
		case "profile":
			loader('on');
			$file_upload = $("#userFile").prop('files')[0];
			if(!$file_upload){$upload_stat='dont_upload';}else{$upload_stat='upload';}

				var data_post = new FormData();	

				$fname = $("input[name=fname]").val();
				$lname = $("input[name=lname]").val();
				$address = $("input[name=address]").val();
				$gender = $("input[name=gender]:checked").val();
				$contact = $("input[name=contact]").val();

				data_post.append('action',"update_profile");
				data_post.append('fname',$fname);
				data_post.append('lname',$lname);
				data_post.append('address',$address);
				data_post.append('gender',$gender);
				data_post.append('contact',$contact);
				data_post.append('upload_status',$upload_stat);
				data_post.append('file_upload',$file_upload);

				$.ajax({
					url: "transactions.php",
					type: "POST",
					processData: false,
					contentType: false,
					data: data_post,
					success:function(data){
						console.log(data);
						if(data == "success"){
							alert('Successfully updated profile!');
							location.reload();
						}else{
							alert('Error encountered while trying to update clinic');
						}
						loader('off');
					}
				})
		break;
	}
}

function process_pending(action,id){
	switch(action){
		case "deny":
			if(confirm('Are you sure to deny this health professional?')){
				loader('on');
				$.ajax({
					url: "transactions.php",
					type: "POST",
					data:{
						'action':'deny_pending_prof',
						'user_id':id
					},success:function(data){
						if(data == "success"){
							alert('Successfully deny health professional!');
							$("#appended_pending_prof_"+id).remove();
						}else{
							alert('Error!');
						}
						loader('off')
					}
				});
			}
		break;
		case "approve":
			if(confirm('Are you sure to approve this health professional?')){
				loader('on');
				$.ajax({
					url: "transactions.php",
					type: "POST",
					data:{
						'action':'approve_pending_prof',
						'user_id':id
					},success:function(data){
						if(data == "success"){
							alert('Successfully approve health professional!');
							$("#appended_pending_prof_"+id).remove();
						}else{
							alert('Error!');
						}
						loader('off')
					}
				});
			}
		break;
		case "lists":
			$(".appended_pending_prof").remove();
			loader('on');
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					'action':'pending_prof'
				},success:function(data){
					data = $.parseJSON(data);

					if(data.length <= 0){
						$("#pending_here").html('<div class="col-md-12"><i style="color:gray;">No pending application!</i></div>');
					}else{

							for(var i=0; i < data.length; i++){	
								$("#pending_here").append('<div class="col-md-3 appended_pending_prof" align="center" id="appended_pending_prof_'+data[i]['mem_id']+'">\n\
									<img src="images/healthprof/'+data[i]['mem_image']+'" style="width:150px;height:150px;">\n\
									<br>\n\
									<strong>'+data[i]['mem_fname']+' '+data[i]['mem_lname']+'</strong><br>\n\
									<div class="row" style="padding-top:15px;">\n\
										<div class="col-md-6">\n\
											<a href="javascript:void(0)" style="color:red;" onclick="process_pending(\'deny\',\''+data[i]['mem_id']+'\')"><i class="fa fa-times"></i> Deny</a>\n\
										</div>\n\
										<div class="col-md-6">\n\
											<a href="javascript:void(0)" style="color:green;" onclick="process_pending(\'approve\',\''+data[i]['mem_id']+'\')"><i class="fa fa-check"></i> Approve</a>\n\
										</div>\n\
									</div>\n\
								</div>');
							}
					}
					loader('off');
				}
			})
		break;
	}
}

function process_like(action,id){
	switch(action){
		case "like":
			loader('on');
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					'action':"create_like",
					'plant_id': id
				},success:function(data){
					loader('off')
					console.log(data);
					if(data == "success"){
						$("#what_like_"+id).html('<a href="javascript:void(0)" onclick="process_like(\'unlike\',\''+id+'\')">Unlike</a>');
					}else{
						alert("Error encountered while processing like")
					}
				}
			})
		break;
		case "unlike":
			loader('on');
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					'action':"create_unlike",
					'plant_id': id
				},success:function(data){
					loader('off')
					console.log(data);
					if(data == "success"){
						$("#what_like_"+id).html('<a href="javascript:void(0)" onclick="process_like(\'like\',\''+id+'\')">Like</a>');
					}else{
						alert("Error encountered while processing unlike")
					}
				}
			})
		break;
	}
}
function clinic_process(action,id){
	switch(action){
		case "outside_lists":
			$(".appended_clinic").remove();
			loader('on');
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					"action":"clinic_outside",
					"clinic_type": id
				},success:function(data){
					data = $.parseJSON(data);
					console.log(data);
					if(data.length > 0){
						for(var i=0; i < data.length; i++){
							$image = data[i]['clinic_image'];
						if($image == ""){
							$what_image = "images/no_image.png"
						}else{
							$what_image = 'images/clinic/'+data[i]['clinic_image']+'';
						}
						
						// $("#clinic_list").append('<div id="clinic_of_'+data[i]['clinic_id']+'" class="col-md-6 appended_clinic" align="center" style="margin-bottom:20px;">\n\
						// 	<img src="'+$what_image+'" style="width:200px;height:200px;"><br>\n\
						// 	<br><input type="file" id="edited_file_upload_'+data[i]['clinic_id']+'" class="clinic_edit_only_'+data[i]['clinic_id']+'" style="display:none;">\n\
						// 	\n\
						// 		<span class="clinic_view_only_'+data[i]['clinic_id']+'">Name: <strong id="clinic_what_name_'+data[i]['clinic_id']+'">'+data[i]['clinic_name']+'</strong></span>\n\
						// 	<input type="text" id="edited_clinic_name_'+data[i]['clinic_id']+'" class="form-control clinic_edit_only_'+data[i]['clinic_id']+'" style="display:none;">\n\
						// 	<br>\n\
						// 	<span class="clinic_view_only_'+data[i]['clinic_id']+'">Description: <strong id="clinic_what_desc_'+data[i]['clinic_id']+'">'+data[i]['clinic_desc']+'</strong></span>\n\
						// 	<input type="text" id="edited_clinic_desc_'+data[i]['clinic_id']+'" class="form-control clinic_edit_only_'+data[i]['clinic_id']+'" style="display:none;">\n\
						// 	<br>\n\
						// 	<span class="clinic_view_only_'+data[i]['clinic_id']+'">Address: <strong id="clinic_what_address_'+data[i]['clinic_id']+'">'+data[i]['clinic_address']+'</strong></span>\n\
						// 	<br><span class="clinic_view_only_'+data[i]['clinic_id']+'"><input type="hidden" id="clinic_what_type_'+data[i]['clinic_id']+'" value="'+data[i]['clinic_type']+'">Type: <strong>'+data[i]['type']+'</strong></span>\n\
						// 	<input type="text" id="edited_clinic_address_'+data[i]['clinic_id']+'" class="form-control clinic_edit_only_'+data[i]['clinic_id']+'" style="display:none;">\n\
						// 	<br><iframe style="height:50%;width:100%;border:0;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q='+data[i]['clinic_address']+'y&key=AIzaSyAHB3QX7B2wXu4M_b3zMA-xbs4n9tkfTAM"></iframe><br>\n\
						// </div>')

						$("#clinic_here").append('<tr class="appended_clinic" id="clinic_of_'+data[i]['clinic_id']+'">\n\
							<td><img src="'+$what_image+'" style="width:50px;height:50px;"><br>\n\
							\n\
							<br><input type="file" id="edited_file_upload_'+data[i]['clinic_id']+'" class="clinic_edit_only_'+data[i]['clinic_id']+'" style="display:none;"></td>\n\
							\n\
							<td><span class="clinic_view_only_'+data[i]['clinic_id']+'"><strong id="clinic_what_name_'+data[i]['clinic_id']+'">'+data[i]['clinic_name']+'</strong></span><input type="text" id="edited_clinic_name_'+data[i]['clinic_id']+'" class="form-control clinic_edit_only_'+data[i]['clinic_id']+'" style="display:none;"></td>\n\
							\n\
							<td><span class="clinic_view_only_'+data[i]['clinic_id']+'"><strong id="clinic_what_desc_'+data[i]['clinic_id']+'">'+data[i]['clinic_desc']+'</strong></span><input type="text" id="edited_clinic_desc_'+data[i]['clinic_id']+'" class="form-control clinic_edit_only_'+data[i]['clinic_id']+'" style="display:none;"></td>\n\
							\n\
							<td><span class="clinic_view_only_'+data[i]['clinic_id']+'"><strong id="clinic_what_address_'+data[i]['clinic_id']+'">'+data[i]['clinic_address']+'</strong></span><br><span class="clinic_view_only_'+data[i]['clinic_id']+'"><input type="hidden" id="clinic_what_type_'+data[i]['clinic_id']+'" value="'+data[i]['clinic_type']+'"></td>\n\
							\n\
							<td><strong>'+data[i]['type']+'</strong></span>\n\
							<input type="text" id="edited_clinic_address_'+data[i]['clinic_id']+'" class="form-control clinic_edit_only_'+data[i]['clinic_id']+'" style="display:none;"></td>\n\
							<td><a href="javascript:void(0)" onclick="clinic_process(\'see_map2\',\''+data[i]['clinic_address']+'\')">See Map</a></td>\n\
						</tr>')

						}
					}else{
						$("#clinic_list").html('<div class="col-md-12"><i>No record found!</i></div>')	
					}
					
				}
			})
		break;
		case "remove":
			
			if(confirm('Are you sure to remove this clinic?')){
				$.ajax({
					url: "transactions.php",
					type: "POST",
					data:{
						'action':'remove_clinic',
						'clinic_id':id
					},success:function(data){
						if(data == "success"){
							$("#clinic_of_"+id).remove();
							alert('Successfully removed clinic!');
						}else{
							alert("Error has been encountered while tyring remove clinic!");
						}
					}
				})	
			}
		break;

		case "update":
			$clinic_name = $("#edited_clinic_name_"+id).val();
			$clinic_address = $("#edited_clinic_address_"+id).val();
			$clinic_desc = $("#edited_clinic_desc_"+id).val();

			$file_upload = $("#edited_file_upload_"+id).prop('files')[0];
			if(!$file_upload){$upload_stat='dont_upload';}else{$upload_stat='upload';}

				var data_post = new FormData();	

				data_post.append('action',"update_clinic");
				data_post.append('clinic_id',id);
				data_post.append('clinic_name',$clinic_name);
				data_post.append('clinic_address',$clinic_address);
				data_post.append('clinic_desc',$clinic_desc);
				data_post.append('upload_status',$upload_stat);
				data_post.append('file_upload',$file_upload);

				$.ajax({
					url: "transactions.php",
					type: "POST",
					processData: false,
					contentType: false,
					data: data_post,
					success:function(data){
						console.log(data);
						if(data == "success"){
							alert('Successfully updated clinic!');
							request_content('clinics');
						}else{
							alert('Error encountered while trying to update clinic');
						}
					}
				})
		break;
		case "save":
			loader('on');
			$clinic_name = $("#clinic_name").val();
			$clinic_address = $("#clinic_address").val();
			$clinic_desc = $("#clinic_desc").val();
			$clinic_type = $("#clinic_type").val();
			$clinic_id = $("#clinic_id").val();
			$file_upload = $("#file_upload").prop('files')[0];

			if($clinic_id == ""){
				$action = "create_clinic"
			}else{
				$action = "update_clinic"
			}

			if(!$file_upload){$upload_stat='dont_upload';}else{$upload_stat='upload';}

				var data_post = new FormData();	

				data_post.append('action',$action);
				data_post.append('clinic_name',$clinic_name);
				data_post.append('clinic_id',$clinic_id);
				data_post.append('clinic_type',$clinic_type);
				data_post.append('clinic_address',$clinic_address);
				data_post.append('clinic_desc',$clinic_desc);
				data_post.append('upload_status',$upload_stat);
				data_post.append('file_upload',$file_upload);

				$.ajax({
					url: "transactions.php",
					type: "POST",
					processData: false,
					contentType: false,
					data: data_post,
					success:function(data){
						console.log(data);
						if(data == "success"){
							alert('Successfully saved clinic!');
							if(id == "inside"){
								request_content('clinics')
							}else{
								location.reload();
							}
						}else{
							loader('off');
							alert('Error encountered while trying to create clinic');
						}
					}
				})
		break;
		case "save2":
			loader('on');

			$clinic_id = $("#clinic_id").val();
			$clinic_name = $("#edited_clinic_name_"+$clinic_id).val();
			$clinic_address = $("#edited_clinic_address_"+$clinic_id).val();
			$clinic_desc = $("#edited_clinic_desc_"+$clinic_id).val();
			$clinic_type = $("#edited_clinic_type_"+$clinic_id).val();
			$file_upload = $("#edited_file_upload_"+$clinic_id).prop('files')[0];

			if($clinic_id == ""){
				$action = "create_clinic"
			}else{
				$action = "update_clinic"
			}

			if(!$file_upload){$upload_stat='dont_upload';}else{$upload_stat='upload';}

				var data_post = new FormData();	

				data_post.append('action',$action);
				data_post.append('clinic_name',$clinic_name);
				data_post.append('clinic_id',$clinic_id);
				data_post.append('clinic_type',$clinic_type);
				data_post.append('clinic_address',$clinic_address);
				data_post.append('clinic_desc',$clinic_desc);
				data_post.append('upload_status',$upload_stat);
				data_post.append('file_upload',$file_upload);

				$.ajax({
					url: "transactions.php",
					type: "POST",
					processData: false,
					contentType: false,
					data: data_post,
					success:function(data){
						console.log(data);
						if(data == "success"){
							alert('Successfully saved clinic!');
							request_content('clinics','');
				
						}else{
							loader('off');
							alert('Error encountered while trying to create clinic');
						}
					}
				})
		break;

		case "cancel":
			clinic_process('lists',id)
		break;
		case "cancel2":
			request_content('clinics','');
		break;
		case "view":
			request_content('clinics');
		break;

		case "add":
			modal_call();	
			$("#m_content").html('<div class="row">\n\
		    	<div class="col-md-12"><input type="hidden" id="clinic_id" value="'+id+'">\n\
		    		<div class="form-group">\n\
		    			Image:<br>\n\
		    			<input type="file" id="file_upload">\n\
		    		</div>\n\
		    		<div class="form-group">\n\
		    			Name:<br>\n\
		    			<input type="text" id="clinic_name" class="form-control">\n\
		    		</div>\n\
		    		<div class="form-group">\n\
		    			Type:<br>\n\
		    			<select id="clinic_type" class="form-control">\n\
		    				<option value="0">Alternative Medicine</option>\n\
		    				<option value="1">Massage</option>\n\
		    				<option value="2">Reflex</option>\n\
		    				<option value="3">Others</option>\n\
		    			</select>\n\
		    		</div>\n\
		    		<div class="form-group">\n\
		    			Address:<br>\n\
		    			<input type="text" id="clinic_address" class="form-control">\n\
		    		</div>\n\
		    		<div class="form-group">\n\
		    			Description:<br>\n\
		    			<textarea id="clinic_desc" class="form-control"></textarea>\n\
		    		</div>\n\
		    		<div class="form-group" align="right">\n\
		    			<a href="javascript:void(0)"  onclick="clinic_process(\'save\',\'\')"><i class="fa fa-paper-plane"></i> Submit</a>\n\
		    		</div>\n\
		    	</div>\n\
		    </div>')

		break;

		case "add_now":
			request_content('add_clinic');
		break;

		case "edit2":
			$(".clinic_view_only_"+id).hide();
			$(".clinic_edit_only_"+id).show();
			$x1 = $("#clinic_what_name_"+id).html();
			$x2 = $("#clinic_what_desc_"+id).html();
			$x3 = $("#clinic_what_address_"+id).html();
			$x4 = $("#clinic_what_type_"+id).val();

			console.log($x1)
			$("#edited_clinic_name_"+id).val($x1);
			$("#edited_clinic_desc_"+id).val($x2);
			$("#edited_clinic_address_"+id).val($x3);	
			$("#clinic_id").val(id);
			$("#edited_clinic_type_"+id+" option[value="+$x4+"]").attr('selected',true);

		break;
		case "edit":
			console.log(id);
			clinic_process('add',id)
			$x1 = $("#clinic_what_name_"+id).html();
			$x2 = $("#clinic_what_desc_"+id).html();
			$x3 = $("#clinic_what_address_"+id).html();
			$x4 = $("#clinic_what_type_"+id).val();

			$("#clinic_name").val($x1);
			$("#clinic_desc").val($x2);
			$("#clinic_address").val($x3);
			$("#clinic_type option[value="+$x4+"]").attr('selected',true);

		break;
		case "lists2":
			$(".appended_clinic").remove();
			loader('on');
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					'action':'clinic_lists'
				},success:function(data){
					data = $.parseJSON(data);
					for(var i=0; i < data.length; i++){
						$image = data[i]['clinic_image'];
						if($image == ""){
							$what_image = "images/no_image.png"
						}else{
							$what_image = 'images/clinic/'+data[i]['clinic_image']+'';
						}
						$("#clinic_here").append('<tr class="appended_clinic" id="clinic_of_'+data[i]['clinic_id']+'">\n\
							<td><img src="'+$what_image+'" style="width:50px;height:50px;"><br>\n\
							\n\
							<input type="hidden" value="'+id+'" id="clinic_id"><input type="file" id="edited_file_upload_'+data[i]['clinic_id']+'" class="clinic_edit_only_'+data[i]['clinic_id']+'" style="display:none;"></td>\n\
							\n\
							<td><span class="clinic_view_only_'+data[i]['clinic_id']+'"><strong id="clinic_what_name_'+data[i]['clinic_id']+'">'+data[i]['clinic_name']+'</strong></span><input type="text" id="edited_clinic_name_'+data[i]['clinic_id']+'" class="form-control clinic_edit_only_'+data[i]['clinic_id']+'" style="display:none;"></td>\n\
							\n\
							<td><span class="clinic_view_only_'+data[i]['clinic_id']+'"><strong id="clinic_what_desc_'+data[i]['clinic_id']+'">'+data[i]['clinic_desc']+'</strong></span><input type="text" id="edited_clinic_desc_'+data[i]['clinic_id']+'" class="form-control clinic_edit_only_'+data[i]['clinic_id']+'" style="display:none;"></td>\n\
							\n\
							<td><span class="clinic_view_only_'+data[i]['clinic_id']+'"><strong id="clinic_what_address_'+data[i]['clinic_id']+'">'+data[i]['clinic_address']+'</strong></span><input type="text" id="edited_clinic_address_'+data[i]['clinic_id']+'" class="form-control clinic_edit_only_'+data[i]['clinic_id']+'" style="display:none;"></td>\n\
							\n\
							<td><span class="clinic_view_only_'+data[i]['clinic_id']+'"><input type="hidden" id="clinic_what_type_'+data[i]['clinic_id']+'" value="'+data[i]['clinic_type']+'">Type: <strong>'+data[i]['type']+'</strong></span>\n\
								<select style="display:none;" id="edited_clinic_type_'+data[i]['clinic_id']+'" class="form-control clinic_edit_only_'+data[i]['clinic_id']+'">\n\
								  <option value="">Select Type: </option>\n\
								  <option value="0">Alternative Medicine</option>\n\
								  <option value="1">Massage</option>\n\
								  <option value="2">Reflex</option>\n\
								  <option value="3">Others</option>\n\
								  </select></td>\n\
							\n\
							<td><a href="javascript:void(0)" onclick="clinic_process(\'see_map2\',\''+data[i]['clinic_address']+'\')">See Map</a></td>\n\
							\n\
							<td><span class="clinic_view_only_'+data[i]['clinic_id']+'"><a href="javascript:void(0)" style="color:red;" onclick="clinic_process(\'remove\',\''+data[i]['clinic_id']+'\')"><i class="fa fa-times"></i> Remove</a>\n\
						 		&nbsp;&nbsp;<a href="javascript:void(0)" style="color:green;" onclick="clinic_process(\'edit2\',\''+data[i]['clinic_id']+'\')"><i class="fa fa-check"></i> Edit</a></span>\n\
							<span class="clinic_edit_only_'+data[i]['clinic_id']+'" style="display:none;"><a href="javascript:void(0)" style="color:red;" onclick="clinic_process(\'cancel\',\'\')"><i class="fa fa-times"></i> Cancel</a>\n\
							&nbsp;&nbsp;<a href="javascript:void(0)" style="color:green;" onclick="clinic_process(\'save2\',\''+data[i]['clinic_id']+'\')"><i class="fa fa-check"></i> Update</a></span></td>\n\
						</tr>')
					}
				}
			});
			loader('off');
		
		break;
		case "see_map":
			modal_call();	
			$("#m_content").html('<small><i>Please wait for the map to load.. </i></small><br><iframe style="height:50%;width:100%;border:0;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q='+id+'y&key=AIzaSyAHB3QX7B2wXu4M_b3zMA-xbs4n9tkfTAM"></iframe>')

		break;
		case "see_map2":
		window.open ("mapz.php?location="+id,"mywindow","menubar=1,resizable=1,width=550,height=550");
		break;
		case "lists":
			$(".appended_clinic").remove();
			loader('on');
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					'action':'clinic_lists'
				},success:function(data){
					data = $.parseJSON(data);
					for(var i=0; i < data.length; i++){
						$image = data[i]['clinic_image'];
						if($image == ""){
							$what_image = "images/no_image.png"
						}else{
							$what_image = 'images/clinic/'+data[i]['clinic_image']+'';
						}
						$("#clinic_here").append('<tr class="appended_clinic" id="clinic_of_'+data[i]['clinic_id']+'">\n\
							<td><img src="'+$what_image+'" style="width:50px;height:50px;"><br>\n\
							\n\
							<br><input type="file" id="edited_file_upload_'+data[i]['clinic_id']+'" class="clinic_edit_only_'+data[i]['clinic_id']+'" style="display:none;"></td>\n\
							\n\
							<td><span class="clinic_view_only_'+data[i]['clinic_id']+'"><strong id="clinic_what_name_'+data[i]['clinic_id']+'">'+data[i]['clinic_name']+'</strong></span><input type="text" id="edited_clinic_name_'+data[i]['clinic_id']+'" class="form-control clinic_edit_only_'+data[i]['clinic_id']+'" style="display:none;"></td>\n\
							\n\
							<td><span class="clinic_view_only_'+data[i]['clinic_id']+'"><strong id="clinic_what_desc_'+data[i]['clinic_id']+'">'+data[i]['clinic_desc']+'</strong></span><input type="text" id="edited_clinic_desc_'+data[i]['clinic_id']+'" class="form-control clinic_edit_only_'+data[i]['clinic_id']+'" style="display:none;"></td>\n\
							\n\
							<td><span class="clinic_view_only_'+data[i]['clinic_id']+'"><strong id="clinic_what_address_'+data[i]['clinic_id']+'">'+data[i]['clinic_address']+'</strong></span><br><span class="clinic_view_only_'+data[i]['clinic_id']+'"><input type="hidden" id="clinic_what_type_'+data[i]['clinic_id']+'" value="'+data[i]['clinic_type']+'"></td>\n\
							\n\
							<td><strong>'+data[i]['type']+'</strong></span>\n\
							<input type="text" id="edited_clinic_address_'+data[i]['clinic_id']+'" class="form-control clinic_edit_only_'+data[i]['clinic_id']+'" style="display:none;"></td>\n\
							<td><a href="javascript:void(0)" onclick="clinic_process(\'see_map\',\''+data[i]['clinic_address']+'\')">See Map</a></td>\n\
							<td><span class="clinic_view_only_'+data[i]['clinic_id']+'"><a href="javascript:void(0)" style="color:red;" onclick="clinic_process(\'remove\',\''+data[i]['clinic_id']+'\')"><i class="fa fa-times"></i> Remove</a>\n\
						 		&nbsp;&nbsp;<a href="javascript:void(0)" style="color:green;" onclick="clinic_process(\'edit\',\''+data[i]['clinic_id']+'\')"><i class="fa fa-check"></i> Edit</a></span>\n\
							<span class="clinic_edit_only_'+data[i]['clinic_id']+'" style="display:none;"><a href="javascript:void(0)" style="color:red;" onclick="clinic_process(\'cancel\',\'\')"><i class="fa fa-times"></i> Cancel</a>\n\
							&nbsp;&nbsp;<a href="javascript:void(0)" style="color:green;" onclick="clinic_process(\'update\',\''+data[i]['clinic_id']+'\')"><i class="fa fa-check"></i> Update</a></span></td>\n\
						</tr>')
					}
				}
			});
			loader('off');
		break;
	}

}


function suggest_process(action,id){
	switch(action){

		case "cancel_my_suggest":
			if(confirm('Are you sure to cancel your suggestion?')){
				loader('on');
				$.ajax({
					url: "transactions.php",
					type: "POST",
					data:{
						"action":"cancel_my_suggest",
						"plant_id":id
					},success:function(data){
						console.log(data)
						if(data == "success"){
							$("#suggestion_of_"+id).remove();
						}else{
							alert("Error encountered!")
						}
						loader('off')
					},error:function(data){
						console.log(data)
						alert("Error encountered!");
						loader('off')
					}
				})
			}
		break;	
		case "my_list":
			$(".appended_request").remove();
			loader('on');
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					'action':'submitted_request'
				},success:function(data){
					data = $.parseJSON(data);
					for(var i=0; i < data.length; i++){

						if(data[i]['what_status'] == "Pending"){
							$action = '<a href="javascript:void(0)" style="color:red;font-weight:bold;" onclick="suggest_process(\'cancel_my_suggest\',\''+data[i]['plant_id']+'\')"><i class="fa fa-times"></i> Cancel Suggestion</a>';
						}else{
							$action = "<strong style='color:green;'>"+data[i]['what_status']+"</strong>";
						}

						$("#request_list").append('<tr class="appended_request" id="suggestion_of_'+data[i]['plant_id']+'">\n\
							<td><img src="plant/'+data[i]['plant_image']+'" style="width:100px;height:100px;"></td>\n\
			                  <td>'+data[i]['plant_name']+'</td>\n\
			                  <td>'+data[i]['common_name']+'</td>\n\
			                  <td>'+data[i]['sci_name']+'</td>\n\
			                  <td>'+data[i]['description']+'</td>\n\
			                  <td>'+$action+'</td>\n\
			              </tr>')
					}
					
			                  // <td><a id="prof_link" style="color:green;font-weight:bold;" href="profile.php?pass_param=healthprof&amp;id='+data[i]['mem_id']+'">'+data[i]['mem_fname']+' '+data[i]['mem_lname']+'</a></td>\n\
				}
			})
		break;
		case "cancel":
			if(confirm('Are you sure to deny this suggestion?')){
				$.ajax({
					url: "transactions.php",
					type: "POST",
					data:{
						'action':"cancel_suggest",
						'plant_id': id
					},success:function(data){
						if(data =="success"){
							alert('Successfully cancelled suggestion!');
							$("#plant_of_"+id).remove();
						}else{
							alert('Error has been encountered while trying to cancel suggestion');
						}
					}
				});
			}
		break;
		case "approve":
			if(confirm('Are you sure to approve this suggestion?')){
				$.ajax({
					url: "transactions.php",
					type: "POST",
					data:{
						'action':"approve_suggest",
						'plant_id': id
					},success:function(data){
						if(data =="success"){
							alert('Successfully approved suggestion!');
							$("#plant_of_"+id).remove();
						}else{
							alert('Error has been encountered while trying to approve suggestion');
						}
					}
				});
			}
		break;
		case "lists":
			$(".appended_sug").remove();
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{'action':'pending_suggestions'},
				success:function(data){
					data = $.parseJSON(data);
					for(var i=0; i < data.length; i++){
						$image = data[i]['plant_image'];
						if($image == ""){
							$imagex = "images/no_image.png";
						}else{
							$imagex = "plant/"+$image;
						}
						$("#request_here").append('<div style="margin-bottom:25px;" id="plant_of_'+data[i]['plant_id']+'" class="col-md-4 appended_sug" align="center">\n\
							<img src="'+$imagex+'" style="width:100px;height:100px;">\n\
							<br><br>\n\
							<div class="row">\n\
								<div class="col-md-12">Name: '+data[i]['plant_name']+'</div>\n\
							</div>\n\
							<br>\n\
							<div class="row">\n\
								<div class="col-md-12">Scient. Name: '+data[i]['sci_name']+'</div>\n\
							</div>\n\
							<br>\n\
							<div class="row">\n\
								<div class="col-md-12">Common Name: '+data[i]['common_name']+'</div>\n\
							</div>\n\
							<br>\n\
							<div class="row">\n\
								<div class="col-md-12">Desc: '+data[i]['description']+'</div>\n\
							</div>\n\
							<hr>\n\
							<div class="row" align="center">\n\
								<div class="col-md-5"><a href="javascript:void(0)" style="color:red;" onclick="suggest_process(\'cancel\',\''+data[i]['plant_id']+'\')"><i class="fa fa-times"></i> Deny</a></div>\n\
								<div class="col-md-5"><a href="javascript:void(0)" style="color:green;" onclick="suggest_process(\'approve\',\''+data[i]['plant_id']+'\')"><i class="fa fa-check"></i> Approve</a></div>\n\
							</div>\n\
						</div>');
					}
				}
			})
		break;
		case "check_suggestions":
			request_content('suggestions');
		break;
		case "submit":
			$s_image = $("#s_image").val();
			$s_name = $("#s_name").val();
			$s_common_name = $("#s_common_name").val();
			$s_s_name = $("#s_s_name").val();
			$s_desc = $("#s_desc").val();
			$file_upload = $("#s_image").prop('files')[0];
			if(!$file_upload){$upload_stat='dont_upload';}else{$upload_stat='upload';}

				var data_post = new FormData();	

				data_post.append('action',"submit_suggestion");
				data_post.append('name',$s_name);
				data_post.append('description',$s_common_name);
				data_post.append('image',$s_s_name);
				data_post.append('scientifical_name',$s_s_name);
				data_post.append('common_name',$s_desc);
				data_post.append('upload_status',$upload_stat);
				data_post.append('file_upload',$file_upload);

			$.ajax({
				url: "transactions.php",
				type: "POST",
				processData: false,
				contentType: false,
				data: data_post,
				success:function(data){
					if(data == "success"){
						$("#reset_here").click();
						alert('Successfully submitted suggestion!');
						location.reload();
					}else{
						alert('Error has been encountered while trying to suggest new plant!');
					}
				}
			})
		break;

		case "create":
			modal_call();
			$("#m_content").html('<form>\n\
				<div class="form-group">\n\
					Image:\n\
					<input type="file" id="s_image">\n\
				</div>\n\
				<div class="form-group">\n\
					Name:\n\
					<input type="text" class="form-control" id="s_name">\n\
				</div>\n\
				<div class="form-group">\n\
					Common Name:\n\
					<input type="text" class="form-control" id="s_common_name">\n\
				</div>\n\
				<div class="form-group">\n\
					Scientifical Name:\n\
					<input type="text" class="form-control" id="s_s_name">\n\
				</div>\n\
				<div class="form-group">\n\
					Description:\n\
					<textarea class="form-control" id="s_desc" placeholder="Enter Description.."></textarea>\n\
				</div>\n\
				<div align="right">\n\
					<a href="javascript:void(0)" onclick="suggest_process(\'submit\',\'\')"><i class="fa fa-paper-plane"></i> Submit</a>\n\
					<input type="reset" id="reset_here" style="display:none;" value="Reset">\n\
				</div>\n\
			</form>');

		break;
	}
}

function register_process(action,id){
	switch(action){

		case "check_email":
			$email = id.value;
			$("#email_error").hide();
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					'action': 'check_reg_email',
					'email': $email
				},success:function(data){

					if(data == "exist"){
						$("#submit_reg").attr('disabled',true);
						$("#email_error").show();
					}else{
						$("#submit_reg").attr('disabled',false);
						$("#email_error").hide();
					}
				}
			})

		break;

		case "check_confirm_pass":
			$password = $("#password").val();
			$confirm_password = $("#confirm_password").val();

			if($password!=$confirm_password){
				$("#submit_reg").attr('disabled',true);
				$("#pass_error").show();
			}else{
				$("#submit_reg").attr('disabled',false);
				$("#pass_error").hide();
			}

		break;
		case "check_username":
			$username = id.value;
			$("#username_error").hide();
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					'action': 'check_reg_username',
					'username': $username
				},success:function(data){

					if(data == "exist"){
						$("#submit_reg").attr('disabled',true);
						$("#username_error").show();
					}else{
						$("#submit_reg").attr('disabled',false);
						$("#username_error").hide();
					}
				}
			})

		break;
	}
}

function health_prof_process(action,id){
	switch(action){
		
		case "message":
			$("#msg_result").html('');
			$name = $("#prof_name_"+id).html();
			$src = $("#image_of_"+id).attr('src');
			$("#prof_link").attr('href','profile.php?pass_param=healthprof&id='+id+'');
			$("#prof_id").val(id);
			$("#prof_message").val('')
			$("#prof_image").attr('src',$src);
			$("#prof_name").html($name);
			$("#prof_list").hide();
			$("#prof_here").show();
		break;

		case "cancel_message":

			$("#prof_list").show();
			$("#prof_here").hide();
		break;

		case "submit_message":
			if(id == "reply"){
				$action = "reply_message"
			}else if(id == "conversation"){
				$action = "reply_conversation";
			}else{
				$action = "submit_message";
			}
			console.log($action);
			$("#msg_result").html('')
			$receiver = $("#prof_id").val();
			$message = $("#prof_message").val();
			if($message == ""){
				alert('Please enter message!');
			}else{
				loader('on')
				$.ajax({
					url: "transactions.php",
					type: "POST",
					data:{
						'action':$action,
						'message': $message,
						'receiver':$receiver
					},success:function(data){
						console.log(data);
						if(data == "success"){

							if(id == "conversation"){
								message_process('users_message',$receiver);
							}else{

								$("#msg_result").html('<div class="alert alert-success">\n\
									  <strong>Successfully submit message!</strong>\n\
									</div>')
								$("#prof_message").val('');

							}
						}else{
							alert("Error encountered while tyring to submit message!")
						}
						loader('off');
					}
				})
			}
		break;

		case "lists":
			loader('on');
			modal_call();
			$("#m_content").html('<div id="prof_list" class="row"></div>\n\
				<div class="row" id="prof_here" style="display:none;">\n\
					<div class="col-md-4">\n\
						<input type="hidden" id="prof_id">\n\
						<img id="prof_image" style="width:150px;height:150px;">\n\
						<br><br>\n\
						<span id="prof_name" style="font-weight:bold;"></span>\n\
						<a id="prof_link" style="color:green;font-weight:bold;">View Profile</a>\n\
					</div>\n\
					<div class="col-md-8" style="padding-top:10px;">\n\
						<textarea class="form-control" style="width:100%;" placeholder="Enter Message.." id="prof_message"></textarea>\n\
						<br>\n\
						<div class="row">\n\
							<div class="col-md-6">&nbsp;</div>\n\
							<div class="col-md-3"><a href="javascript:void(0)" onclick="health_prof_process(\'cancel_message\',\'\')"><i class="fa fa-times"></i> Cancel</a></div>\n\
							<div class="col-md-3"><a href="javascript:void(0)" onclick="health_prof_process(\'submit_message\',\'\')"><i class="fa fa-paper-plane"></i> Send</a></div>\n\
						</div>\n\
						<br><br>\n\
								<div id="msg_result"></div>\n\
					</div>\n\
				</div>');
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					'action':"prof_list"
				},success:function(data){
					data = $.parseJSON(data);
					console.log(data);
					for(var i=0; i < data.length; i++){
						$("#prof_list").append('<div class="col-md-3" align="center">\n\
								<a href="javascript:void(0)" onclick="health_prof_process(\'message\',\''+data[i]['mem_id']+'\')"><img src="images/healthprof/'+data[i]['mem_image']+'" id="image_of_'+data[i]['mem_id']+'" style="width:100px;height:100px;">\n\
								<br><br>\n\
								<span style="padding-top:5%;" id="prof_name_'+data[i]['mem_id']+'">'+data[i]['mem_fname']+' '+data[i]['mem_lname']+'</span></a>\n\
							</div>')
					}

					loader('off');
				}
			})
		break;
	}
}

function comment_process(action,id){
	switch(action){
		case "what_plant":
			$.ajax({
				url: "transactions.php",
				type: "POST",
				data:{
					'action':'what_plant',
					'plant_id':id
				},success:function(data){
					data = $.parseJSON(data);
					$("#plant_name_x").html(data[0]['plant_name']);
					$("#plant_src").attr('src',"plant/"+data[0]['plant_image']);
					$("#plant_desc_x").html(data[0]['description']);
				}
			})
		break;
		case "lists":
			loader('on');
				modal_call();
				$img = $("#comment_img_of_"+id).attr('src');
				$plant_name = $("#plant_name_"+id).html();
				$content = $("#comment_content_of_"+id).html();
				$("#m_content").html('<div class="row">\n\
						<div class="col-md-2"><img id="plant_src" src="'+$img+'" style="width:50px;height:50px;"></div>\n\
						<div class="col-md-10"><strong id="plant_name_x">'+$plant_name+'</strong></div>\n\
					</div><br><div class="row"><div class="col-md-12" style="padding-left:40px;" id="plant_desc_x">'+$content+'</div></div><br>\n\
					<table width="100%">\n\
					<tr>\n\
						<td valign="top" width="90%">\n\
							<textarea class="form-control" id="comment" placeholder="Enter comment.."></textarea>\n\
						</td>\n\
						<td valign="top">&nbsp;&nbsp;</td>\n\
						<td valign="top" width="10%" style="padding-top:2%;">\n\
							<button style="width:97%;height:10%;" class="btn btn-success" onclick="comment_process(\'create_plant_comment\',\''+id+'\')"><i class="fa fa-paper-plane"></i> Comment</button>\n\
						</td>\n\
					</tr>\n\
				</table><br><div id="comment_list"></div>');
				$.ajax({
					url: "transactions.php",
					type: "POST",
					data:{
						'plant_id':id,
						'action':"plant_comments"
					},success:function(data){
						if($("#plant_name_"+id).val() == undefined){
							comment_process('what_plant',id);
						}
						data = $.parseJSON(data);

						for(var i=0; i < data.length; i++){

							if(data[i]['mem_id'] == $("#sess_id").val()){

								$what ='<div class="row" align="right" id="comment_option">\n\
									<a href="javascript:void(0);"  onclick="comment_process(\'edit\',\''+data[i]['comment_id']+'\')" style="color:green;"><i class="fa fa-pencil-square-o"></i> Edit</a>\n\
									&nbsp;&nbsp;\n\
									<a href="javascript:void(0);" onclick="comment_process(\'remove\',\''+data[i]['comment_id']+'_'+id+'\')" style="color:red;"><i class="fa fa-times"></i> Remove</a>\n\
								</div>\n\
								<div class="row" align="right" id="comment_update" style="display:none;">\n\
									<a href="javascript:void(0);"  onclick="comment_process(\'update\',\''+data[i]['comment_id']+'\')"  style="color:green;"><i class="fa fa-pencil-square-o"></i> Update</a>\n\
									&nbsp;&nbsp;\n\
									<a href="javascript:void(0);" onclick="comment_process(\'cancel\',\''+data[i]['comment_id']+'\')"  style="color:red;"><i class="fa fa-times"></i> Cancel</a>\n\
								</div>'
							}else{
								$what = "";
							}

							$("#comment_list").append('<div class="well" id="comment_line_'+data[i]['comment_id']+'">\n\
								<div class="row">\n\
									<div class="col-md-6" style="font-weight:bold;">\n\
										'+data[i]['mem_username']+'\n\
									</div>\n\
									<div class="col-md-6" align="right">\n\
										'+data[i]['comment_date']+'\n\
									</div>\n\
								</div>\n\
								<div class="row" id="what_comment_'+data[i]['comment_id']+'"><div class="col-md-12" id="comment_of_'+data[i]['comment_id']+'">'+data[i]['comment_content']+'</div></div>\n\
								<div class="row" id="comment_updated_'+data[i]['comment_id']+'" style="display:none;padding-top:3%;">\n\
									<div class="col-md-12">\n\
										<textarea class="form-control" id="updated_comment" placeholder="Enter comment.."></textarea>\n\
									</div>\n\
								</div>\n\
							<div style="padding-top:3%;">'+$what+'</div>\n\
							</div>');	
						}

						loader('off');
					}

				});
		break;

		case "edit":
			$("#what_comment_"+id+",#comment_option").hide();
			$("#comment_update,#comment_updated_"+id).show();

			$comment = $("#comment_of_"+id).html();
			$("#updated_comment").val($comment);
		break;

		case "cancel":

			$("#what_comment_"+id+",#comment_option").show();
			$("#comment_update,#comment_updated_"+id).hide();

		break;

		case "update":
			$comment = $("#updated_comment").val();
			if($comment == ""){
				alert("Comment can't be empty!");
			}else{
				loader('on');
				$.ajax({
					url: "transactions.php",
					type: "POST",
					data:{
						'action':"update_comment",
						'comment_id':id,
						'comment': $comment
					},success:function(data){
						loader('off');
						if(data == "success"){
							comment_process('cancel',id);
							$("#comment_of_"+id).html($comment);
						}else{
							alert('Error has been encountered while tyring to update comment!');
						}
					}
				})
			}
		break;

		case "remove":
			$comment_id = id.split('_')[0];
			$plant_id = id.split('_')[1];
			if(confirm('Are you sure to remove this comment?')){
				loader('on')
				$.ajax({
					url: "transactions.php",
					type: "POST",
					data:{
						'action':'remove_comment',
						'comment_id': $comment_id
					},success:function(data){
						loader('off');
						$num_comment = $("#comment_of_"+$plant_id).html();
						$("#comment_of_"+$plant_id).html(parseInt($num_comment) - 1);
						if(data == 'success'){
							$("#comment_line_"+$comment_id).remove();
						}else{
							alert('Error has been encountered while trying to remove comment!');
						}
					}

				})
			}
		break;

		case "create_plant_comment":
			$comment = $("#comment").val();
			if($comment == ""){
				alert("Comment can't be empty!");
			}else{
				$plant_name = $("#plant_name_"+id).html();
				if($plant_name == undefined){
					$plant_name = $("#plant_name_x").html();
				}

				loader('on');
				$.ajax({
					url: "transactions.php",
					type: "POST",
					data:{
						'comment': $comment,
						'plant_id': id,
						'plant_name': $plant_name,
						'action': "insert_plant_comment",
					},success:function(data){
						$num_comment = $("#comment_of_"+id).html();
						$("#comment_of_"+id).html(parseInt($num_comment) + 1);
						if(data == "success"){
							$("#comment").val('');
							comment_process('lists',id);
						}else{
							alert('Error encountered while trying to insert data!');
						}
						loader('off');
					}
				});
			}	

		break;
	}
}

function modal_call(){
	$("#m_content").html('');
	$("#myModalx").modal();

}

function request_content(content){
	$.ajax({
		url: "content.php",
		type: "POST",
		data:{
			'content': content
		},
		success:function(data){
			console.log(data)
			$("#requested_page").val(content);
			$("#page-wrapper").html(data);
		}
	})

}
	
function loader(action){
	switch(action){
		case "on":
			$(".pre_blocker").show();
		break;

		case "off":
			$(".pre_blocker").hide();
		break;
	}
}

function disableF5(e) {
 // || (e.which || e.keyCode) == 82

if ((e.which || e.keyCode) == 116){
e.preventDefault();
    $req = $("#requested_page").val();
    // refresh_page();
    if($req !=""){
    	request_content($req);
    }
    console.log('refresh here')
  }
}