<?php
 require_once("web.config.php"); //global variables for database access
  include('dbConfig.php');
 include('session.php');
 include('navigation.php');
 require_once('head.php');
 if (isset($_GET["pass_param"]))
 {
	 $passed= $_GET["pass_param"];
 }
 else
 {
 	 $passed="feeds";
 }
 

 ?>
 
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>HerbsMed</title>

  <link rel="stylesheet" href="css/w3.css">
  <link href="css/templatemo_style.css" rel="stylesheet" type="text/css" />
 </head>
<style>

button {
  background-image: url(images/comments.png);
  background-repeat: no-repeat;
  background-position: 50% 50%;
  /* put the height and width of your image here */
  height: 50px;
  width: 200px;
  border: none;
}

button span {
  display: none;
}

p#out
{
	width:100%;
    direction:ltr;
    display:block;
    max-width:100%;
	/*min-height:250px;*/
    height:auto;
    line-height:1.5;
    padding:15px 15px 30px;
    border-radius:3px;
    border:1px solid #F7E98D;
    font:16px Tahoma, cursive;
    transition:box-shadow 0.5s ease;
    box-shadow:0 4px 6px rgba(0,0,0,0.1);
    font-smoothing:subpixel-antialiased;
    background:linear-gradient(#F9EFAF, #F7E98D);
    background:-o-linear-gradient(#F9EFAF, #F7E98D);
    background:-ms-linear-gradient(#F9EFAF, #F7E98D);
    background:-moz-linaear-gradient(#F9EFAF, #F7E98D);
    background:-webkit-linear-gradient(#F9EFAF, #F7E98D);
}

textarea#note {
    width:100%;
    direction:ltr;
    display:block;
    max-width:100%;
	min-height:250px;
    line-height:1.5;
    padding:15px 15px 30px;
    border-radius:3px;
    border:1px solid #F7E98D;
    font:16px Tahoma, cursive;
    transition:box-shadow 0.5s ease;
    box-shadow:0 4px 6px rgba(0,0,0,0.1);
    font-smoothing:subpixel-antialiased;
    background:linear-gradient(#F9EFAF, #F7E98D);
    background:-o-linear-gradient(#F9EFAF, #F7E98D);
    background:-ms-linear-gradient(#F9EFAF, #F7E98D);
    background:-moz-linear-gradient(#F9EFAF, #F7E98D);
    background:-webkit-linear-gradient(#F9EFAF, #F7E98D);
}

html,body,h1,h2,h3,h4,h5,h6 {font-family: "Roboto", sans-serif}
</style>
<body>
	<!-- Page Container -->
<div class="w3-content w3-margin-top" style="max-width:1400px;">

  <!-- The Grid -->
  <div class="w3-row-padding">
  
    <!-- Left Column -->
    <div class="w3-third">
      
      <?php 
        if($row['mem_type'] =="ordinary"){
          $what_location = "profile";
        }elseif($row['mem_type'] == "health"){
          $what_location = "healthprof";
        }

        if(!empty($row['mem_image'])){
          $profile_location  = "images/".$what_location."/".$row['mem_image'];
        }else{
          $profile_location = "images/no_image.png";
        }
      ?>
      <div class="w3-white w3-text-grey w3-card-4">
        <div class="w3-display-container">
          <a href="profile.php?pass_param=profile"><img src="<?php echo $profile_location;?>" style="width:100%" alt="Avatar" id="my_avatar"></a>
          <div class="w3-display-bottomleft w3-container w3-text-black">
            <h2 style="color:white;"><?php echo $row['mem_fname']; ?> <?php echo $row['mem_lname']; ?></h2>
          </div>
        </div><br>
        <div class="w3-container">
          <p><i class="fa fa-user fa-fw w3-margin-right w3-large w3-text-teal"></i><?php echo $row['mem_type']; ?></p>
          <p><i class="fa fa-paper-plane fa-fw w3-margin-right w3-large w3-text-teal"></i><?php echo $row['mem_email']; ?></p>
          <p><i class="fa fa-phone fa-fw w3-margin-right w3-large w3-text-teal"></i><?php echo $row['mem_contact']; ?></p>
          <p><a  href="profile.php?pass_param=messages"><i class="fa fa-envelope fa-fw w3-margin-right w3-large w3-text-teal"></i> Messages </a></p>
          <p><a href="profile.php?pass_param=account"><i class="fa fa-gear fa-fw w3-margin-right w3-large w3-text-teal"></i>Account Setting</a></p>
          <hr>
          <p class="w3-large"><b><i class="fa fa-asterisk fa-fw w3-margin-right w3-text-teal"></i><a href="profile.php?pass_param=feeds">Health Feeds</a></b></p>
 <!--      <p class="w3-large"><b><i class="fa fa-asterisk fa-fw w3-margin-right w3-text-teal"></i><a href="profile.php?pass_param=professionals">Health Professionals</a></b></p> -->
       <?php if($_SESSION['sess_userrole'] == "ordinary"){?>
      <p class="w3-large"><b><i class="fa fa-asterisk fa-fw w3-margin-right w3-text-teal"></i><a href="javascript:void(0)" onclick="health_prof_process('lists','')">Health Professionals</a></b></p>
        <p class="w3-large"><b><i class="fa fa-asterisk fa-fw w3-margin-right w3-text-teal"></i><a href="profile.php?pass_param=submitted_request">Submitted Request</a></b></p>
      <?php }?>
      <p class="w3-large"><b><i class="fa fa-asterisk fa-fw w3-margin-right w3-text-teal"></i><a href="profile.php?pass_param=profile">Health Profile</a></b></p>
      
      <?php if($_SESSION['sess_userrole'] == "health"){?>
        <p class="w3-large"><b><i class="fa fa-asterisk fa-fw w3-margin-right w3-text-teal"></i><a href=" profile.php?pass_param=submitted_request">Add plant</a></b></p>
        <p class="w3-large"><b><i class="fa fa-asterisk fa-fw w3-margin-right w3-text-teal"></i><a href=" profile.php?pass_param=reviews">My Review</a></b></p>
        <p class="w3-large"><b><i class="fa fa-asterisk fa-fw w3-margin-right w3-text-teal"></i><a href="profile.php?pass_param=clinics">Clinics</a></b></p>
        <p class="w3-large"><b><i class="fa fa-asterisk fa-fw w3-margin-right w3-text-teal"></i><a href="profile.php?pass_param=request">Submitted Request</a></b></p>
        <p class="w3-large"><b><i class="fa fa-asterisk fa-fw w3-margin-right w3-text-teal"></i><a href="profile.php?pass_param=products">Products</a></b></p>
      <?php } ?>


      <p class="w3-large"><b><i class="fa fa-asterisk fa-fw w3-margin-right w3-text-teal"><a href="profile.php?pass_param=assessment"></i>Health Assessment</a></b></p>
    
           <p>Sporty</p>
          <div class="w3-light-grey w3-round-xlarge w3-small">
            <div class="w3-container w3-center w3-round-xlarge w3-teal" style="width:90%">90%</div>
          </div>
          <p>Energetic</p>
          <div class="w3-light-grey w3-round-xlarge w3-small">
            <div class="w3-container w3-center w3-round-xlarge w3-teal" style="width:80%">
              <div class="w3-center w3-text-white">80%</div>
            </div>
          </div>
          <p>Health Condition</p>
          <div class="w3-light-grey w3-round-xlarge w3-small">
            <div class="w3-container w3-center w3-round-xlarge w3-teal" style="width:75%">75%</div>
          </div>
          <p>Vegetarian</p>
          <div class="w3-light-grey w3-round-xlarge w3-small">
            <div class="w3-container w3-center w3-round-xlarge w3-teal" style="width:50%">50%</div>
          </div>
          <br>
        </div>
      </div><br>

    <!-- End Left Column -->
    </div>

    <!-- Right Column -->
    <div class="w3-twothird">
	 
    
      <div class="w3-container w3-card-2 w3-white w3-margin-bottom"  style="height: auto">
        <h2 class="w3-text-grey w3-padding-16"></i></h2>
        <div class="w3-container">
            <!-- <div align="right">
              <a href="javascript:void(0)" onclick="suggest_process('create','')"><h4><i class="fa fa-paper-plane"> </i> Suggest New</h4></a>
            </div> -->
            <hr>
         <?php
		 	if ($passed=="feeds")
			{  
        echo'
          <h3 class="page-header" style="color:gray;">HEALTH FEEDS</h3>';
				 include('function_comment.php');
			}
			else if ($passed=="professionals")
			{echo $passed;}
      else if ($passed=="profile")
      { include('my_profile.php');}
      elseif($passed=="clinics"){ ?>
        <?php require('bundle_js.php');?>
        <script type="text/javascript">
          clinic_process('lists','');
        </script>
            <div class="row">
                    <div class="col-md-9">
                        <h1 class="page-header" style="color:gray;">List of Clinics</h1>
                    </div>
                    <div class="col-md-3" align="right">
                      <?php if( $_SESSION['sess_userrole'] == "admin" ||  $_SESSION['sess_userrole'] == "health"){?>
                        <a href="javascript:void(0)" onclick="clinic_process('add','')">
                          <i class="fa fa-plus"></i> Create Clinic
                        </a>
                      <?php }?>
                    </div>
                <!-- <div id="clinic_here" class="row"></div>-->
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>Image</th>
                      <th>Name</th>
                      <th>Description</th>
                      <th>Address</th>
                      <th>Type</th>
                      <th>Map</th>
                      <th>Action</th>
                    </tr> 
                  </thead>
                  <tbody id="clinic_here"></tbody>
                </table> 
                <br><br>
            </div>
      <?php }
      elseif($passed == "request"){ ?>
        <?php require('bundle_js.php');?>
          <script type="text/javascript">
            suggest_process('lists','');
          </script>
              <div class="row">
                              <div class="col-lg-12">
                                  <h1 class="page-header" style="color:gray;">Request</h1>
                              </div>

                  <div id="request_here"></div>            
              </div>
      <?php } elseif($passed=="healthprof"){?>

        <?php require('bundle_js.php');?>
          <script type="text/javascript">
            review_process('profile','<?php echo $_GET["id"];?>');
          </script>
          <h3 class="page-header" style="color:gray;">HEALTH PROFESSIONAL PROFILE</h3>
          <div class="row">
            <div class="col-md-4">
              <div align="center">
                <img id="prof_image" style="width:150px;height:150px;">
              </div>
              <hr>
              <table width="100%">
                <tr>
                  <td>Name: </td>
                  <td><span id="prof_name"></span></td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                  <td>Address: </td>
                  <td><span id="prof_add"></span></td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                  <td>Contact: </td>
                  <td><span id="prof_contact"></span></td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                  <td>Email: </td>
                  <td><span id="prof_email"></span></td>
                </tr>
              </table>
              <br>
            </div>
            <div class="col-md-8">
              <ul class="list-group">
                
                <li class="list-group-item">
                  <table width="100%">
                    <tr>
                      <td valign="top" width="70%">
                        <textarea class='form-control' id="review_content" placeholder="Add your review here.."></textarea>
                      </td>
                      <td valign="top" width="10%">&nbsp;</td>
                      <td valign="top">
                        <button class="btn btn-success" id="review_btn_sub" onclick="review_process('create','<?php echo $_GET["id"];?>')">Submit Review</button>
                      </td>
                    </tr>
                  </table>
                </li>
              </ul>
              <div  id="review_ul"></div>
            </div>

          </div>


      <?php } 
      else if ($passed=="account")
      { include('my_account.php');}
      elseif($passed == "reviews"){
      ?>   
      <?php require('bundle_js.php'); ?>
      <script type="text/javascript">
            review_process('profile','<?php echo $_SESSION["sess_user_id"];?>');
          </script>
          <h3 class="page-header" style="color:gray;">MY REVIEWS</h3>
          <div class="row">
            <div class="col-md-12">
              <div  id="review_ul"></div>
            </div>
          </div><br><br>  
      <?php } elseif($passed == "notifications"){
      ?>   
          <?php require('bundle_js.php'); ?>
          <script type="text/javascript">
            notification_process('lists','');
          </script>
          <h3 class="page-header" style="color:gray;">MY NOTIFICATIONS</h3>
          <div class="row">
            <div class="col-md-12">
              <div  id="notification_ul"></div>
            </div>
          </div>  <br><br>  
       <?php require('bundle.php'); ?>
      <?php } elseif($passed == "submitted_request"){?>
          <?php require('bundle_js.php'); ?>
          <script type="text/javascript">
            suggest_process('my_list','');
          </script>
          <div class="row">
            <div class="col-md-9">
              <h3 class="page-header" style="color:gray;">MY REQUESTS</h3>
            </div>
            <div class="col-md-3" align="right">
                <a href="#" onclick="suggest_process('create','')"></i>Suggest New Plant</a>
            </div>
          </div>  
          <div class="row">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Image</th>
                  <th>Name</th>
                  <th>Common Name</th>
                  <th>Scientifical Name</th>
                  <th>Description</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody id="request_list"></tbody>
            </table>

          </div> 
          <br><br>  
     <?php } elseif($passed == "messages") {?>
		  <?php require('bundle_js.php'); ?>
          <script type="text/javascript">
            message_process('contact_list','');
          </script>

          <div class="row">
            <div class="col-md-3" id="contact_list">
              <ul class="list-group">
                <li class="list-group-item"><center><strong>CONTACT LIST</strong></center></li>
              </ul>
            </div>
            <div class="col-md-9">
                <h3 class="page-header" style="color:gray;" id="whos_message"></h3>
                <div  id="message_list"></div>
                <div id="msg_reply"></div>
            </div>
          </div>
<!--  style="height:900px;overflow: auto;" -->
    <?php } elseif($passed == "tutorials"){?>
        <?php require('bundle_js.php'); ?>
          <script type="text/javascript">
            tutorial_process('lists','');
          </script>
      <div class="row">
        <div class="col-md-9">
          <h3 class="page-header" style="color:gray;">TUTORIAL LISTS</h3>
        </div>
        <div class="col-md-3">
          <?php if($_SESSION['sess_userrole'] == "health" || $_SESSION['sess_userrole'] == "admin"){ ?>
              <a href="javascript:void(0)" onclick="tutorial_process('create','')"><i class="fa fa-paper-plane"></i> Create Tutorial</a>
            <?php }?>
        </div>
      </div>  
      <hr>
      <div id="tutorial_list"></div>
    <?php } elseif($passed == "products" && $_SESSION['sess_userrole'] == "health"){ ?>
         <?php require('bundle_js.php'); ?>
          <script type="text/javascript">
            product_process('my_list','');
          </script>
      <div class="row">
        <div class="col-md-9">
          <h3 class="page-header" style="color:gray;">PRODUCT LIST</h3>
        </div>
        <div class="col-md-3">
              <a href="javascript:void(0)" onclick="product_process('modal_add','')"><i class="fa fa-plus"></i> Promote Product</a>
        </div>
      </div>  
      <hr>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>&nbsp;</th>
            <th>Name</th>
            <th>Description</th>
            <th>Related Plant</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody id="products_here"></tbody>
      </table>

     <?php } elseif($passed = "assessment"){ ?>
        <?php require('bundle_js.php'); ?>
        <script type="text/javascript">
          assessment_process('lists','');
        </script>
      <div class="row">
        <div class="col-md-9">
          <h3 class="page-header" style="color:gray;">ASSESSMENT LIST</h3>
        </div>
        <div class="col-md-3">
              <a href="javascript:void(0)" onclick="assessment_process('modal_add','')"><i class="fa fa-plus"></i> Create Assessment</a>
        </div>
      </div>  
      <div class="row" id="assessment_here"></div>
     <?php }else
			{echo "Page not found";}
		 ?> 
        </div>
        </div>
        <div class="w3-container">
          <h5 class="w3-opacity"><b></b></h5>
          <h6 class="w3-text-teal"></h6>
          <p></p><br>
        </div>



    <!-- End Right Column -->
    </div>
    
  <!-- End Grid -->
  </div>
  
  <!-- End Page Container -->
</div>

<footer class="w3-container w3-black w3-center w3-margin-top">
  <center><p>© 2017 Herbsmed. All rights reserved</p></a></center>
</footer>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<div id="myModalx" class="modal fade" role="dialog">
  <div class="modal-dialog" style="z-index:1000">

    <!-- Modal content-->
    <div class="modal-content">
   <!--    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" style="width:20px;">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div> -->
      <div class="modal-body">
        <div id="m_content"></div>
      </div>
<!--       <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" style="width:15%;height:15%;">Close</button>
      </div> -->
    </div>

  </div>
</div>

<style>
  .pre_blocker{
   position:fixed;
   left:0;
   top:0;
   z-index:99999999999;
   width:100%;
   height:100%;
   overflow:visible;
   background: rgba(0,0,0,0.8);
   color: #fff;
   display: table;
}
  .pre_blocker2{
   position:fixed;
   left:0;
   top:0;
   z-index:1000;
   width:100%;
   height:100%;
   overflow:visible;
   background: rgba(0,0,0,0.8);
   color: #fff;
   display: table;
}
</style>
<div class="pre_blocker" style="display:none;">
    <center>
       <p style="padding-top:20%;font-size: 15px;"> <i class="fa fa-circle-o-notch fa-spin fa-1x fa-fw fa-spin-2x"></i> Processing...</p>
    </center>
</div>
</body>
</html>

</body>
</html>