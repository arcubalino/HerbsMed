<?php

if(!isset($login_session)){

?>

<html>
<style>
body {

	background:  url(images/bg.jpg) no-repeat;
	background-attachment: fixed;	
}
</style>


<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" ></a><img src="images/herbsmed_logo.png" height="50" width="80">
            </div>
            <!-- Collect the nav links for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="index.php"><span class="fa fa-home fa-lg"></span>&nbsp; Home</a>
                    </li>
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-leaf"></span>&nbsp; Plants
						<span class="caret"></span></a>
						<ul class="dropdown-menu">
						<li><a href="plants.php">Search Plants</a></li>
						<li><a href="#"> Validated Plants</a></li>					
						</ul>
					</li>                   
                    <li><a href="clinic.php"><span class="glyphicon glyphicon-plus"></span>&nbsp; Clinics</a>
                    </li>
					<li><a href="chart.php"><span class="glyphicon glyphicon-stats"></span>&nbsp; Statistics</a>
                    </li>
                    <li><a href="aboutus.php"><span class="glyphicon glyphicon-info-sign"></span>&nbsp; About us</a>					
                    </li>
					</li>
					<li><a href="login_form2.php"><span class="glyphicon glyphicon-log-in"></span>&nbsp; Login</a>
                    </li>
                   
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	
</html>

<?php }
else{
?>

<html>
<style>
body {

	background:  url(images/bg.jpg) no-repeat;
	background-attachment: fixed;	
}
</style>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" ></a><img src="images/herbsmed_logo.png" height="50" width="80">
            </div>
            <!-- Collect the nav links for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                     <?php if(empty($_SESSION['sess_user_id'])){?>
                    <li><a href="index.php"><span class="fa fa-home fa-lg"></span>&nbsp; Home</a>
                    </li>
                    <?php }?>
                    <?php if(!empty($_SESSION['sess_user_id'])){?>
                    <li><a href="profile.php"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;Profile</a>  
                    <?php }?>
                    <li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-leaf"></span>&nbsp; Plants
						<span class="caret"></span></a>
						<ul class="dropdown-menu">
						<li><a href="plants.php">Search Plants</a></li>
						<li><a href="#">Validated Plants</a></li>					
						</ul>
					</li>  
                    <li><a href="profile.php?pass_param=tutorials"><span class="glyphicon glyphicon-info-sign"></span>&nbsp; Tutorials</a>  
                    <li><a href="clinic.php"><span class="glyphicon glyphicon-plus"></span>&nbsp; Clinics</a>
                    </li>
                    <li><a href="chart.php"><span class="glyphicon glyphicon-stats"></span>&nbsp; Statistics</a>
                    </li>
                    <?php if(!empty($_SESSION['sess_user_id'])){?>
                    <li><a href="profile.php?pass_param=notifications"><span class="badge" id="notify_num">0</span>&nbsp; Notifications</a>
                    </li>
                    <?php }?>
                    <li><a href="aboutus.php"><span class="glyphicon glyphicon-info-sign"></span>&nbsp; About us</a>					
                    </li>
					</li>
					<li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span>&nbsp; Logout</a>
                    </li>
                   
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	
</html>
	
<?php	
mysql_close($connection); // Closing Connection
}?>