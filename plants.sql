-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 09, 2009 at 07:14 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `herbsmed`
--

-- --------------------------------------------------------

--
-- Table structure for table `plants`
--

CREATE TABLE `plants` (
  `plant_id` int(11) NOT NULL,
  `plant_name` varchar(30) NOT NULL,
  `description` varchar(500) NOT NULL,
  `plant_image` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `plants`
--

INSERT INTO `plants` (`plant_id`, `plant_name`, `description`, `plant_image`) VALUES
(1, 'Abaniko', 'An abaniko (from the Spanish word abanico, meaning fan) is a type of hand-held fan that originated from the Philippines. The abaniko, together with the baro''t saya, is a part of a lady''s attire. Various ways of using and holding the abaniko may convey different meanings. For example, an open abaniko that covers the chest area is a sign of modesty while rapid fan movements express the lady''s displeasure.[1]', 'abaniko.jpg'),
(2, 'Acacia', 'Acacia, commonly known as the wattles or acacias, is a large genus of shrubs, lianas and trees in the subfamily Mimosoideae of the pea family Fabaceae', 'acacia.jpg'),
(3, 'Ampalaya', '', ''),
(4, 'Azucena', '', ''),
(5, 'Banaba', '', ''),
(6, 'Bawang', '', ''),
(7, 'Bayabas', '', ''),
(8, 'Caimito', '', ''),
(9, 'Chico', '', ''),
(10, 'Dalandan', '', ''),
(11, 'Dama de noche', '', ''),
(12, 'Eucalyptus', '', ''),
(13, 'Everlasting', '', ''),
(14, 'Garlic vine', '', ''),
(15, 'Gumamela', '', ''),
(16, 'Guyabano', '', ''),
(17, 'Hagonoi', '', ''),
(18, 'Hydrangea', '', ''),
(19, 'Iba', '', ''),
(20, 'Ipil-ipil', '', ''),
(21, 'Kalachuchi', '', ''),
(22, 'Labanos', '', ''),
(23, 'Lagundi', '', ''),
(24, 'Laurel', '', ''),
(25, 'Mais', '', ''),
(26, 'Mahogany', '', ''),
(27, 'Tsaang gubat', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `plants`
--
ALTER TABLE `plants`
  ADD PRIMARY KEY (`plant_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `plants`
--
ALTER TABLE `plants`
  MODIFY `plant_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
