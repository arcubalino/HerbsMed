<?php include('session.php');?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HerbsMed</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"> 

<style type="text/css">

.glyphicon{font-size: 20px;}
a.glyphicon{text-decoration: none;}
a.glyphicon-trash{margin-left: 10px;}
.none{display: none;}
</style>




<script>
function getUsers(){
    $.ajax({
        type: 'POST',
        url: 'userAction.php',
        data: 'action_type=view&'+$("#userForm").serialize(),
        success:function(html){
            $('#userData').html(html);
        }
    });
}
function userAction(type,id){
    id = (typeof id == "undefined")?'':id;
    var statusArr = {add:"added",edit:"updated",delete:"deleted"};
    var userData = '';
    if (type == 'add') {
        userData = $("#addForm").find('.form').serialize()+'&action_type='+type+'&id='+id;
    }else if (type == 'edit'){
        userData = $("#editForm").find('.form').serialize()+'&action_type='+type;
    }else{
        userData = 'action_type='+type+'&id='+id;
    }
    $.ajax({
        type: 'POST',
        url: 'userAction.php',
        data: userData,
        success:function(msg){
            if(msg == 'ok'){
                alert('User data has been '+statusArr[type]+' successfully.');
                getUsers();
                $('.form')[0].reset();
                $('.formData').slideUp();
            }else{
                alert('Some problem occurred, please try again error.');
            }
        }
    });
}

function editUser(id){
    $.ajax({
        type: 'POST',
        dataType:'JSON',
        url: 'userAction.php',
        data: 'action_type=data&id='+id,
        success:function(data){
			$('#idEdit').val(data.id);
            $('#mem_usernameEdit').val(data.mem_username);
            $('#mem_fnameEdit').val(data.mem_fname);
            $('#mem_lnameEdit').val(data.mem_lname);
            $('#mem_addEdit').val(data.mem_add);
			$('#mem_ageEdit').val(data.mem_age);
			$('#mem_contactEdit').val(data.mem_contact);
			$('#mem_emailEdit').val(data.mem_email);
            $('#editForm').slideDown();
        }
    });
}

</script>
</head>



<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="admin.php">HerbsMed</a>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->

            <?php require('admin_side.php');?>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Health Professional</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            List of Health Professional Users
                        </div>
				
				
				<div class="panel-body none formData" id="editForm">
                <h2 id="actionLabel">Edit Content</h2>
                <form class="form" id="userForm">
                    <div class="form-group">
                        <label>Date</label>
                        <input type="date" class="form-control" name="date" id="date1Edit"/>
                    </div>
                    <div class="form-group">
                        <label>Time</label>
                        <input type="text" class="form-control" name="time" id="timeEdit"/>
                    </div>
                    <div class="form-group">
                        <label>Day of the Week</label>
                        <input type="text" class="form-control" name="day" id="dayEdit"/>
                    </div>
					 <div class="form-group">
                        <label>Theme</label>
                        <input type="text" class="form-control" name="theme" id="themeEdit"/>
                    </div>
					 <div class="form-group">
                        <label>Platform</label>
                        <input type="text" class="form-control" name="platform" id="platformEdit"/>
                    </div>
					 <div class="form-group">
                        <label>Content</label>
                        <input type="text" class="form-control" name="contents" id="contentEdit"/>
                    </div>
					 <div class="form-group">
                        <label>Media URL</label>
                        <input type="text" class="form-control" name="media_url" id="media_urlEdit"/>
                    </div>
                    <input type="hidden" class="form-control" name="id" id="idEdit"/>
                    <a href="javascript:void(0);" class="btn btn-warning" onclick="$('#editForm').slideUp();">Cancel</a>
                    <a href="javascript:void(0);" class="btn btn-success" onclick="userAction('edit')">Update Content</a>
                </form>				
             </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                           <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Username</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
										<th>Address</th>
										<th>Age</th>
										<th>Contact</th>
										<th>Email</th>
										<th>Action</th>
                                    </tr>
                                </thead>
					    
						
						<tbody id="userData">
						<?php
                       	include 'dbConfig.php';
						$sql = "SELECT * FROM member where mem_type='Health'";					
						$member = $db->query($sql);
						
						if(!empty($member)): $count = 0; foreach($member as $member): $count++;
                       ?>
                      <tr>
                       
                        <td><?php echo $member['mem_username']; ?></td>
                        <td><?php echo $member['mem_fname']; ?></td>
						<td><?php echo $member['mem_lname']; ?></td>						
                        <td><?php echo $member['mem_add']; ?></td>
						<td><?php echo $member['mem_age']; ?></td>
						<td><?php echo $member['mem_contact']; ?></td>
						<td><?php echo $member['mem_email']; ?></td>
						
                        <td>
                            <a href="javascript:void(0);" class="fa fa-check" onclick="editUser('<?php echo $member['mem_id']; ?>')"></a>
                            &nbsp;
                            <a href="javascript:void(0);" class="fa fa-times" onclick="return confirm('Are you sure to reject this user?')?userAction('delete','<?php echo $member['mem_id']; ?>'):false;"> </a>
                        </td>
                    </tr>
                    <?php endforeach; else: ?>
                    <tr><td colspan="5">No content(s) found......</td></tr>
                    <?php endif; ?>
								
                                   
                                </tbody>
                            </table>
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            
               
            </div>
            <!-- /.row -->      
        </div>
        <!-- /#page-wrapper -->

  


    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>

</body>

</html>