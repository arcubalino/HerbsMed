<?php 
	require('bundle.php');
	require('db_credentials.php');	 
	$db = NEW PDO('mysql:host='.$db_host.';dbname='.$db_db.'', "".$db_user."", "".$db_password."");
	$sess_id = $_SESSION['sess_user_id'];

	if(empty($sess_id)){
		header('Location: profile.php');
	}

	$cmd = $db->prepare("SELECT mem_lname,mem_fname,mem_gender,mem_add,mem_contact FROM member WHERE mem_id=?");
	$cmd->execute(array($sess_id));
	$row = $cmd->fetch();	
	$mem_gender = $row['mem_gender'];

	if($mem_gender == "Male"){
		$gen_m = "checked='checked'";
	}else{
		$gen_m = '';
	}

	if($mem_gender == "Female"){
		$gen_f = "checked='checked'";
	}else{
		$gen_f = '';
	}

?>
	<h1 class="page-header" style="color:gray;">UPDATE PROFILE</h1><hr>
  <div class="row">
              <div class="col-sm-6">
				<div class="form-group">
					<label>Change Profile Picture: </label>
				   <input type="file" name="userFile" id="userFile"/>
				</div> 
				<br>
                <div class="form-group">	
                	<label>First Name: </label>
                  <input type="text" class="form-control" name="fname" value="<?php echo $row['mem_fname'];?>" placeholder="Firstname" >
                </div>
                <br>
                <div class="form-group">
                	<label>Last Name: </label>
                  <input type="text" class="form-control" name="lname" value="<?php echo $row['mem_lname'];?>"  placeholder="Lastname"  >
                </div>
                <br>
                <div class="form-group">
                	<label>Gender: </label>
               		<input type="radio" name="gender" value="Male"  <?php echo $gen_m;?>>Male
			   		<input type="radio" name="gender" value="Female" <?php echo $gen_f;?>>Female
				</div>
              </div>
               <div class="col-sm-6" style="padding-top:4px;">
               	 <br><br> <br><br>
               	 <label>Address: </label>
                 <div class="form-group">
                  <input type="text" class="form-control" name="address" value="<?php echo $row['mem_add'];?>"  placeholder="Address" >
                </div>
                <br>
                <div class="form-group">
                	<label>Contact Number: </label>
                  <input type="text" class="form-control" name="contact" placeholder="Contact Number" value="<?php echo $row['mem_contact'];?>" >
                </div>
              </div>

              </div>
				
                  <div class="form-group">
                    </br><button type="submit" class="btn btn-success center-block" onclick="update_process('profile','')" name="submit-register"><i class="fa fa-user"></i> UPDATE PROFILE</button><br>
                  </div>
<br><br><br>