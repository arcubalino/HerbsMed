<?php
include('dbConfig.php');
include('function_plant.php');
if(isset($_POST["plant_id"]))
{
	$output = array();
	$statement = $dbh->prepare(
		"SELECT * FROM plants 
		WHERE plant_id = '".$_POST["plant_id"]."' 
		LIMIT 1"
	);
	$statement->execute();
	$result = $statement->fetchAll();
	foreach($result as $row)
	{
		$output["plant_name"] = $row["plant_name"];
		$output["description"] = $row["description"];
		$output["sci_name"] = $row["sci_name"];
		$output["common_name"] = $row["common_name"];
		
		if($row["plant_image"] != '')
		{
			$output['user_image'] = '<img src="plant/'.$row["plant_image"].'" class="img-thumbnail" width="50" height="35" /><input type="hidden" name="hidden_user_image" value="'.$row["plant_image"].'" />';
		}
		else
		{
			$output['user_image'] = '<input type="hidden" name="hidden_user_image" value="" />';
		}
	}
	echo json_encode($output);
}
?>