<?php
include('dbConfig.php');
include('function_plant.php');
if(isset($_POST["operation"]))
{
	if($_POST["operation"] == "Add")
	{
		$image = '';
		if($_FILES["user_image"]["name"] != '')
		{
			$image = upload_image();
		}
		$statement = $dbh->prepare("
			INSERT INTO plants (plant_name, description,common_name,sci_name, plant_image) 
			VALUES (:plant_name, :desc, :sci_name, :common_name, :image)
		");
		$result = $statement->execute(
			array(
				':plant_name'	=>	$_POST["plant_name"],
				':desc'   	    =>	$_POST["desc"],
				':sci_name'   	=>	$_POST["sci_name"],	
				':common_name'	=>	$_POST["common_name"],
				':image'		=>	$image
			)
		);
		
		
		if(!empty($result))
		{
			echo 'Data Inserted';
		}
	}
	if($_POST["operation"] == "Edit")
	{
		$image = '';
		if($_FILES["user_image"]["name"] != '')
		{
			$image = upload_image();
		}
		else
		{
			$image = $_POST["hidden_user_image"];
		}
		$statement = $dbh->prepare(
			"UPDATE plants 
			SET plant_name = :plant_name, description = :desc, sci_name= :sci_name, common_name= :common_name, plant_image = :image  
			WHERE plant_id = :id
			"
		);
		$result = $statement->execute(
			array(
				':plant_name'	=>	$_POST["plant_name"],
				':desc'			=>	$_POST["desc"],
				':sci_name'		=>	$_POST["sci_name"],
				':common_name'	=>	$_POST["common_name"],
				':image'		=>	$image,
				':id'			=>	$_POST["plant_id"]
			)
		);
		if(!empty($result))
		{
			echo 'Data Updated';
		}
	}
}

?>