<?php
require("db_credentials.php");
include('navigation.php');
include('head.php');
?> 
<!DOCTYPE html>
<html>
 <head>
  <title>Statistics </title>
 <script src="js/jquery-2.1.1.min.js"></script>
 <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  
 </head>
 <body>
  <br /><br />
  <div class="container" style="width:900px;">
    <br>
   <h2 align="center" style="color:black;">Statistical Data</h2>

          <center><h3 id="title">Graph Per Like</h3></center>
          <canvas id="canvas_bar"></canvas>
          <hr>
          <center><h3 id="title">Survey Per Ailment</h3></center>
          <canvas id="canvas_bar2"></canvas>
          <hr>
          <center><h3 id="title">Survey Per Gender</h3></center>
          <canvas id="canvas_bar3"></canvas>
        
          <hr>
          <center><h3 id="title">Survey Per Satisfaction</h3></center>
          <canvas id="canvas_bar4"></canvas>
        


    <script src="js/chartjs/chart.min.js"></script>
    <script src="js/transactions.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            charts('per_like');
            charts('chart_per_ailment');
            charts('chart_per_gender');
            charts('chart_per_satisfaction');
        });


          function charts(action){
            switch(action){
              case "chart_per_gender":
              $.ajax({
                url: "transactions.php",
                type: "POST",
                data:{
                  "action":"chart_per_gender"
                },success:function(data){
                  $gender = [];
                  $count = [];
                  data = $.parseJSON(data);
                  console.log(data);
                  for(var i=0; i < data.length; i++){

                    $gender.push(""+data[i]["gender"]+"");
                    $count.push(""+data[i]["count"]+"");
                  }
                  console.log($gender)
                   var barChartData2 = {
                      // labels: [$plant_name],
                      labels:$gender,

                      datasets: [
                          {
                              fillColor: "#26B99A", //rgba(220,220,220,0.5)
                              strokeColor: "#26B99A", //rgba(220,220,220,0.8)
                              highlightFill: "#36CAAB", //rgba(220,220,220,0.75)
                              highlightStroke: "#36CAAB", //rgba(220,220,220,1)
                              data: $count
                      }
                  ],
                  }

                new Chart($("#canvas_bar3").get(0).getContext("2d")).Bar(barChartData2, {
                    tooltipFillColor: "rgba(51, 51, 51, 0.55)",
                    responsive: true,
                    barDatasetSpacing: 6,
                    barValueSpacing: 5
                });

                },error:function(data){
                  console.log(data);
                }
              })

               
              break;
              case "chart_per_satisfaction":
              $.ajax({
                url: "transactions.php",
                type: "POST",
                data:{
                  "action":"chart_per_satisfaction"
                },success:function(data){
                  $gender = [];
                  $count = [];
                  data = $.parseJSON(data);
                  console.log(data);
                  for(var i=0; i < data.length; i++){
                    $sat = data[i]["satisfaction_rate"];
                    if($sat == "vg"){
                      $what_sat = "Very Good";
                    }else if($sat == "e"){
                      $what_sat = "Excellent";
                    }else if($sat == "g"){
                      $what_sat = "Good";
                    }

                    $gender.push(""+$what_sat+"");
                    $count.push(""+data[i]["count"]+"");
                  }
                   var barChartData2 = {
                      // labels: [$plant_name],
                      labels:$gender,

                      datasets: [
                          {
                              fillColor: "#26B99A", //rgba(220,220,220,0.5)
                              strokeColor: "#26B99A", //rgba(220,220,220,0.8)
                              highlightFill: "#36CAAB", //rgba(220,220,220,0.75)
                              highlightStroke: "#36CAAB", //rgba(220,220,220,1)
                              data: $count
                      }
                  ],
                  }

                new Chart($("#canvas_bar4").get(0).getContext("2d")).Bar(barChartData2, {
                    tooltipFillColor: "rgba(51, 51, 51, 0.55)",
                    responsive: true,
                    barDatasetSpacing: 6,
                    barValueSpacing: 5
                });

                },error:function(data){
                  console.log(data);
                }
              })

               
              break;

              case "chart_per_ailment":
              $.ajax({
                url: "transactions.php",
                type: "POST",
                data:{
                  "action":"chart_per_ailment"
                },success:function(data){
                  $ailments = [];
                  $count = [];
                  data = $.parseJSON(data);
                  console.log(data);
                  for(var i=0; i < data.length; i++){

                    $ailments.push(""+data[i]["ailments"]+"");
                    $count.push(""+data[i]["count"]+"");
                  }
                  console.log($ailments)
                   var barChartData2 = {
                      // labels: [$plant_name],
                      labels:$ailments,

                      datasets: [
                          {
                              fillColor: "#26B99A", //rgba(220,220,220,0.5)
                              strokeColor: "#26B99A", //rgba(220,220,220,0.8)
                              highlightFill: "#36CAAB", //rgba(220,220,220,0.75)
                              highlightStroke: "#36CAAB", //rgba(220,220,220,1)
                              data: $count
                      }
                  ],
                  }

                new Chart($("#canvas_bar2").get(0).getContext("2d")).Bar(barChartData2, {
                    tooltipFillColor: "rgba(51, 51, 51, 0.55)",
                    responsive: true,
                    barDatasetSpacing: 6,
                    barValueSpacing: 5
                });


                },error:function(data){
                  console.log(data);
                }
              })

               
              break;
              case "per_like":

              $.ajax({
                url: "transactions.php",
                type: "POST",
                data:{
                  "action":"chart_per_like"
                },success:function(data){
                  
                  $plant_name = [];
                  $count_like = [];
                  data = $.parseJSON(data);
                  for(var i=0; i < data.length; i++){

                    $plant_name.push(""+data[i]["plant_name"]+"");
                    $count_like.push(""+data[i]["count"]+"");
                  }
                  console.log($count_like)
                   var barChartData = {
                      // labels: [$plant_name],
                               labels:$plant_name,

                      datasets: [
                          {
                              fillColor: "#26B99A", //rgba(220,220,220,0.5)
                              strokeColor: "#26B99A", //rgba(220,220,220,0.8)
                              highlightFill: "#36CAAB", //rgba(220,220,220,0.75)
                              highlightStroke: "#36CAAB", //rgba(220,220,220,1)
                              data: $count_like
                      }
                  ],
                  }

                new Chart($("#canvas_bar").get(0).getContext("2d")).Bar(barChartData, {
                    tooltipFillColor: "rgba(51, 51, 51, 0.55)",
                    responsive: true,
                    barDatasetSpacing: 6,
                    barValueSpacing: 5
                });

                },error:function(data){
                  console.log(data);
                }
              })

               
              break;
            }//end of switch
          }

    </script>
   <br/><br />
  </div>
 </body>
</html>