<?php 
	require 'dbConfig.php';
	session_start();
	$username = "";
	$password = "";
	if(isset($_POST['username'])){
		$username = $_POST['username'];
	}
	if (isset($_POST['password'])) {
		$password = md5($_POST['password']);

	}

	$q = 'SELECT * FROM member WHERE mem_username=:username AND mem_pass=:password AND status=1';
	
    // $credentials = $result->num_rows;
	
	$query = $dbh->prepare($q);
	$query->execute(array(':username' => $username, ':password' => $password));
	
	if($query->rowCount() == 0){
		header('Location: login_form2.php?err=1');
	}else{

		$row = $query->fetch(PDO::FETCH_ASSOC);

		session_regenerate_id();
		$_SESSION['sess_user_id'] = $row['mem_id'];
		$_SESSION['sess_username'] = $row['mem_username'];
        $_SESSION['sess_userrole'] = $row['mem_type'];
        $_SESSION['sess_useremail'] = $row['mem_email'];

        echo $_SESSION['sess_userrole'];
		session_write_close();

		if( $_SESSION['sess_userrole'] == "ordinary"){
			header('Location: profile.php');
			
		}
		else if ($_SESSION['sess_userrole']=="health"){
			header('Location: profile.php');		
		}
		else{
			header('Location: admin.php');
		}
		
		
	}


?>