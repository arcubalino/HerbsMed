<?php
require('db_credentials.php');
// ==================================================================
// Database connection settings -- CHANGE HERE
define("EZSQL_DB_USER", $db_user);			// <-- mysql db user
define("EZSQL_DB_PASSWORD", $db_password);		// <-- mysql db password
define("EZSQL_DB_NAME", $db_db);		// <-- mysql db pname
define("EZSQL_DB_HOST", $db_host);	// <-- mysql server host
?>