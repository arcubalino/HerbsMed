<?php 
	require('bundle.php');
  require('bundle_credentials.php');   
	$sess_id = $_SESSION['sess_user_id'];

	if(empty($sess_id)){
		header('Location: profile.php');
	}

	$cmd = $db->prepare("SELECT mem_username,mem_email FROM member WHERE mem_id=?");
	$cmd->execute(array($sess_id));
	$row = $cmd->fetch();	
	$mem_gender = $row['mem_gender'];

	if($mem_gender == "Male"){
		$gen_m = "checked='checked'";
	}else{
		$gen_m = '';
	}

	if($mem_gender == "Female"){
		$gen_f = "checked='checked'";
	}else{
		$gen_f = '';
	}

?>
	<h1 class="page-header" style="color:gray;">UPDATE ACCOUNT </h1><hr>
  <div class="row">
              <div class="col-sm-6">
				<br>
                <div class="form-group">	
                	<label>Email: </label>
                  <input type="text" class="form-control" id="my_email" name="email" onkeyup="update_process('check_my_email',this)" value="<?php echo $row['mem_email'];?>" placeholder="Enter email">
                    <div class="alert alert-danger" id="email_error" style="display:none;">
                      <strong>Error!</strong> email already exist.
                    </div>
                </div>
                <br>
                <div class="form-group">
                	<label>Username: </label>
                  <input type="text" class="form-control" id="my_username" name="username" onkeyup="update_process('check_my_username',this)"  value="<?php echo $row['mem_username'];?>"  placeholder="Enter username.."  >
                  <div class="alert alert-danger" id="username_error" style="display:none;">
                    <strong>Error!</strong> username exist.
                  </div>
                </div>
                <br>
              </div>
               <div class="col-sm-6">
               	 <br>
               	 <label>Password: </label>
                 <div class="form-group">
                  <input type="password" class="form-control" id="password" name="password" placeholder="Enter password..">
                </div>
                <br>
                <div class="form-group">
                	<label>Confirm password: </label>
                  <input type="password" class="form-control" id="confirm_pass" onkeyup="update_process('check_my_pass',this)"  name="confirm_password" placeholder="Confirm password">
                    <div class="alert alert-danger" id="pass_error" style="display:none;">
                      <strong>Error!</strong> password doesn't match.
                    </div>
                </div>
              </div>

              </div>
				
                  <div class="form-group">
                    </br><button type="submit" class="btn btn-success center-block" onclick="update_process('account','')" name="submit-register" id="submit_reg"><i class="fa fa-user"></i> UPDATE ACCOUNT</button><br>
                  </div>
<br><br><br>