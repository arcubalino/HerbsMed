/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.5-10.1.21-MariaDB : Database - herbsmed
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`herbsmed` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `herbsmed`;

/*Table structure for table `account` */

DROP TABLE IF EXISTS `account`;

CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `purchase` int(11) NOT NULL,
  `sale` int(11) NOT NULL,
  `profit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `account` */

insert  into `account`(`id`,`year`,`purchase`,`sale`,`profit`) values (0,2000,100,23,1213),(0,2000,234,234,123),(0,2017,23,23,23);

/*Table structure for table `assessments` */

DROP TABLE IF EXISTS `assessments`;

CREATE TABLE `assessments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(1000) DEFAULT NULL,
  `related_plant` int(255) DEFAULT NULL,
  `plant_name` varchar(1000) DEFAULT NULL,
  `related_tutorial` int(255) DEFAULT NULL,
  `tutorial` varchar(1000) DEFAULT NULL,
  `content` varchar(1000) DEFAULT NULL,
  `user_id` int(255) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `assessments` */

insert  into `assessments`(`id`,`title`,`related_plant`,`plant_name`,`related_tutorial`,`tutorial`,`content`,`user_id`,`created_at`) values (1,'Lagundi Herbal Treatment for Cough',23,NULL,2,NULL,'If you are not lazy to prepare the tea or lagundi extact, make it as follows: Gather one-half cup of chopped fresh leaves and boil it in two cups of water for 10 to 15 minutes. Administer one-half cup three times a day. I hope a 15 minutes preparation would not hurt your busy schedule. This herb has other medicinal uses by BPI:',32,'2017-08-06 14:24:01'),(2,'Preparation of Herbal Medication (Akapulko)',0,'Akapulko',0,'SuaDCkfsUD0','Lagundi, a herbal medicine is becoming popular as treatment for cough. In fact, the Philippine Department of Health is actively promoting it as an alternative medicine. The herb is included in 10 herbal medicine approved by DOH.',32,'2017-08-06 14:28:27'),(5,'Break',0,'Ahoi',0,'02QLjESHw68','asdfa',28,'2017-08-09 18:27:53');

/*Table structure for table `clinic` */

DROP TABLE IF EXISTS `clinic`;

CREATE TABLE `clinic` (
  `clinic_id` int(11) NOT NULL AUTO_INCREMENT,
  `clinic_name` varchar(30) NOT NULL,
  `clinic_address` varchar(60) NOT NULL,
  `clinic_desc` varchar(150) NOT NULL,
  `clinic_image` varchar(150) NOT NULL,
  `owner` int(255) DEFAULT NULL,
  `clinic_type` int(255) DEFAULT '0' COMMENT '0 -  Alternative | 1 - Massage | 2 - Reflex | 3 - Others',
  PRIMARY KEY (`clinic_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `clinic` */

insert  into `clinic`(`clinic_id`,`clinic_name`,`clinic_address`,`clinic_desc`,`clinic_image`,`owner`,`clinic_type`) values (2,'Doc Samaritan','Guadalupe, Cebu City','Nice Place','doc_samaritan.jpg',32,0),(3,'Arc','Guizo, Mandaue City','Arc','170728104843_arc.PNG',32,1),(5,'aa','Tabok, Mandaue City','aa','170728112331_hero-203547c7.png',32,2),(6,'Quag Clinic','New York City','For Ladies only!','170804032806_Facespace_portrait_quagmire_default@4x.png',32,3),(7,'Laravel2','Saitama, Japan','Laravel','170826082024_asd.PNG',28,2);

/*Table structure for table `comments` */

DROP TABLE IF EXISTS `comments`;

CREATE TABLE `comments` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `plant_name` text CHARACTER SET utf8 COLLATE utf8_bin,
  `mem_id` int(255) DEFAULT NULL,
  `comment_content` text CHARACTER SET utf8 COLLATE utf8_bin,
  `comment_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `mem_username` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `plant_id` int(255) NOT NULL,
  PRIMARY KEY (`comment_id`),
  FULLTEXT KEY `mem_username` (`mem_username`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=latin1;

/*Data for the table `comments` */

insert  into `comments`(`comment_id`,`plant_name`,`mem_id`,`comment_content`,`comment_date`,`mem_username`,`plant_id`) values (84,'Avocado',47,'this is the second test','2017-07-26 02:35:27','test',82),(85,'Avocado',28,'this should show','2017-07-26 02:39:18','admin',82),(86,'Abaniko',0,'hello','2017-07-26 12:08:14','',1),(87,'Abaniko',0,'hellow2','2017-07-26 12:10:41','',1),(88,'Abaniko',0,'helogfffddfgf','2017-07-26 14:31:34','',1),(89,'Abaniko',42,'geklkls','2017-07-26 18:43:32','renz54',1),(94,'Avocado',53,'yummy!','2017-07-27 15:03:46','ranil',82),(98,'Avocado',32,'sure btaw?','2017-08-01 22:16:53','health',82),(99,'Avocado',53,'oo bai sure ko','2017-08-01 22:30:50','ordinary',82),(100,NULL,32,'hey','2017-08-01 22:38:14','health',82),(101,NULL,32,'adfa','2017-08-12 15:21:10','health',1),(102,'Abaniko',32,'aaa','2017-08-23 20:41:40','health',1),(103,'Abaniko',32,'fffff','2017-08-23 20:49:44','health',1),(104,'Abaniko',32,'gggg','2017-08-23 21:18:49','health',1),(105,'Caimito',32,'walai upai','2017-08-23 21:27:26','health',8),(106,'arc',32,'stupid plant','2017-08-23 21:52:34','health',89);

/*Table structure for table `likes` */

DROP TABLE IF EXISTS `likes`;

CREATE TABLE `likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) DEFAULT NULL,
  `plant_id` int(255) DEFAULT NULL,
  `date_liked` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `likes` */

insert  into `likes`(`id`,`user_id`,`plant_id`,`date_liked`) values (3,53,82,NULL),(4,32,82,NULL),(5,32,1,NULL),(6,60,1,NULL),(9,59,82,NULL),(10,59,1,NULL),(11,53,1,NULL);

/*Table structure for table `markers` */

DROP TABLE IF EXISTS `markers`;

CREATE TABLE `markers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `address` varchar(80) NOT NULL,
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL,
  `type` varchar(30) NOT NULL,
  `link` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `markers` */

insert  into `markers`(`id`,`name`,`address`,`lat`,`lng`,`type`,`link`) values (1,'Abaniko','Talamaban Cebu City',10.367068,123.917389,'restaurant',''),(2,'Abaniko','Tisa Cebu City',10.301553,123.870529,'bar','');

/*Table structure for table `member` */

DROP TABLE IF EXISTS `member`;

CREATE TABLE `member` (
  `mem_id` int(10) NOT NULL AUTO_INCREMENT,
  `mem_username` varchar(10) NOT NULL,
  `mem_pass` varchar(1000) NOT NULL,
  `mem_fname` char(15) NOT NULL,
  `mem_lname` char(15) NOT NULL,
  `mem_add` varchar(50) NOT NULL,
  `mem_age` int(10) NOT NULL,
  `mem_contact` text NOT NULL,
  `mem_email` varchar(20) NOT NULL,
  `mem_image` varchar(500) NOT NULL,
  `mem_gender` varchar(6) NOT NULL,
  `mem_type` char(10) NOT NULL,
  `modified` datetime NOT NULL,
  `created` datetime NOT NULL,
  `status` int(255) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=Inactive, 2 - Pending, 3 - Deny',
  `not_counter` int(255) unsigned DEFAULT NULL,
  PRIMARY KEY (`mem_id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

/*Data for the table `member` */

insert  into `member`(`mem_id`,`mem_username`,`mem_pass`,`mem_fname`,`mem_lname`,`mem_add`,`mem_age`,`mem_contact`,`mem_email`,`mem_image`,`mem_gender`,`mem_type`,`modified`,`created`,`status`,`not_counter`) values (28,'admin','21232f297a57a5a743894a0e4a801fc3','Mr. Admin','Istrator','',0,'0','','','','admin','2017-06-12 15:07:13','2017-06-12 15:07:13',1,NULL),(32,'health','555bf8344ca0caf09b42f55e185526d8','Renato','Fernandez','fddf',0,'2147483647','renatofer@yahoo.com','2.jpg','Male','health','0000-00-00 00:00:00','0000-00-00 00:00:00',1,NULL),(53,'ordinary','178ac33dfc50ee56dea6aca5cdb9f470','Ranil','Jaramillo','Mandaue City',24,'09202684921','ran.revolution@gmail','170801023733_images.png','Male','ordinary','2017-07-27 11:52:02','2017-07-27 11:52:06',1,NULL),(59,'tambalan','896ffcb82fbacb0e383b351910b76fdd','tambalan','tambalan','tambalan',0,'231123','tambalan@tambalan.co','170731110251_Koala.jpg','Male','health','0000-00-00 00:00:00','0000-00-00 00:00:00',1,NULL),(60,'ahoi','5688b56e6af21cf6c3a8995a8829f869','ahoi','ahoi','ahoi',0,'1231231','ahoi@ahoi.com','170812091441_images.png','Male','ordinary','0000-00-00 00:00:00','0000-00-00 00:00:00',1,NULL);

/*Table structure for table `messages` */

DROP TABLE IF EXISTS `messages`;

CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(1000) DEFAULT NULL,
  `receiver` int(255) DEFAULT NULL,
  `sender` int(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `conversation_id` int(255) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

/*Data for the table `messages` */

insert  into `messages`(`id`,`content`,`receiver`,`sender`,`created_at`,`conversation_id`) values (24,'Hi',32,53,'2017-08-16 20:41:59',24),(27,'hey',53,32,'2017-08-16 20:46:58',24),(28,'ue musta na?',32,53,'2017-08-16 20:47:19',24),(29,'okay ra ko kaw?',53,32,'2017-08-16 20:47:37',24),(30,'nag unsa ka?',32,53,'2017-08-16 21:22:19',24),(31,'hoy!',53,32,'2017-08-16 21:22:40',24),(32,'Paminawa tawn ko !',53,32,'2017-08-16 21:23:26',24),(33,'corrent',53,32,'2017-08-16 21:23:54',24),(34,'ahoi',32,53,'2017-08-16 21:25:37',24),(35,'aaa',53,53,'2017-08-16 21:30:18',24),(36,'haha na unsa ka?',32,32,'2017-08-16 21:31:27',24),(37,'hoy bai',53,53,'2017-08-16 21:34:02',24),(38,'oh saman?',32,32,'2017-08-16 21:34:22',24);

/*Table structure for table `notifications` */

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender` int(255) DEFAULT NULL,
  `receiver` int(255) DEFAULT NULL,
  `type` int(255) DEFAULT NULL COMMENT '0 - Comment (All) | 1 - Newly Approved Plants (Admin) | 3 - Request of Promotion product | 4 - Message from HP (Ordinary) | 5 - If Suggested plant has been approved (Ordinary) | 6 - Message from Ordinary (Health) | 2 - Review added from Ordinary (Health)',
  `content` varchar(1000) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` int(255) DEFAULT '1' COMMENT '0 - Read | 1 - Unread',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;

/*Data for the table `notifications` */

insert  into `notifications`(`id`,`sender`,`receiver`,`type`,`content`,`created_at`,`status`) values (23,53,32,6,'You receive a message from a user. Click <a href=\'javascript:void(0)\' onclick=\'message_process(\"open_message\",\"13\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-02 13:47:28',0),(24,32,53,6,'You receive a message from a user. Click <a href=\'javascript:void(0)\' onclick=\'message_process(\"open_message\",\"14\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-02 14:07:12',0),(25,53,32,6,'You receive a message from a user. Click <a href=\'javascript:void(0)\' onclick=\'message_process(\"open_message\",\"15\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-02 14:08:54',0),(26,53,32,6,'You receive a message from a user. Click <a href=\'javascript:void(0)\' onclick=\'message_process(\"open_message\",\"16\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-02 14:09:08',0),(27,32,28,6,'You receive a message from a user. Click <a href=\'javascript:void(0)\' onclick=\'message_process(\"open_message\",\"17\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-02 14:48:58',0),(28,32,28,3,'A user request for product promotion approval for you to review. Click <a href=\'javascript:void(0)\' onclick=\'product_process(\"admin_list\",\"\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-05 19:34:06',0),(29,32,28,3,'A user request for product promotion approval for you to review. Click <a href=\'javascript:void(0)\' onclick=\'product_process(\"admin_list\",\"\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-05 19:40:20',0),(30,32,28,3,'A user request for product promotion approval for you to review. Click <a href=\'javascript:void(0)\' onclick=\'product_process(\"admin_list\",\"\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-05 21:05:57',0),(31,32,28,3,'A user request for product promotion approval for you to review. Click <a href=\'javascript:void(0)\' onclick=\'product_process(\"admin_list\",\"\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-05 21:06:42',0),(32,32,28,3,'A user request for product promotion approval for you to review. Click <a href=\'javascript:void(0)\' onclick=\'product_process(\"admin_list\",\"\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-05 21:46:58',0),(33,32,28,3,'A user request for product promotion approval for you to review. Click <a href=\'javascript:void(0)\' onclick=\'product_process(\"admin_list\",\"\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-05 21:47:35',0),(34,28,32,3,'Your request for product promotion has been approved. Click <a href=\'profile.php?pass_param=products\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-06 11:10:29',0),(35,32,0,0,'A user commented on the plant you recently involved. Click <a href=\'javascript:void(0)\' onclick=\'comment_process(\"lists\",\"1\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-12 15:21:10',1),(36,32,0,0,'A user commented on the plant you recently involved. Click <a href=\'javascript:void(0)\' onclick=\'comment_process(\"lists\",\"1\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-12 15:21:10',1),(37,32,0,0,'A user commented on the plant you recently involved. Click <a href=\'javascript:void(0)\' onclick=\'comment_process(\"lists\",\"1\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-12 15:21:10',1),(38,32,42,0,'A user commented on the plant you recently involved. Click <a href=\'javascript:void(0)\' onclick=\'comment_process(\"lists\",\"1\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-12 15:21:10',1),(39,32,28,3,'A user request for product promotion approval for you to review. Click <a href=\'javascript:void(0)\' onclick=\'product_process(\"admin_list\",\"\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-12 15:24:55',0),(40,53,32,6,'You receive a message from a user. Click <a href=\'javascript:void(0)\' onclick=\'message_process(\"open_message\",\"18\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-16 20:36:54',0),(41,53,32,6,'You receive a message from a user. Click <a href=\'javascript:void(0)\' onclick=\'message_process(\"open_message\",\"19\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-16 20:41:02',0),(42,53,32,6,'You receive a message from a user. Click <a href=\'javascript:void(0)\' onclick=\'message_process(\"open_message\",\"20\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-16 20:41:16',0),(43,53,32,6,'You receive a message from a user. Click <a href=\'javascript:void(0)\' onclick=\'message_process(\"open_message\",\"21\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-16 20:41:35',0),(44,53,32,6,'You receive a message from a user. Click <a href=\'javascript:void(0)\' onclick=\'message_process(\"open_message\",\"22\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-16 20:41:40',0),(45,53,32,6,'You receive a message from a user. Click <a href=\'javascript:void(0)\' onclick=\'message_process(\"open_message\",\"23\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-16 20:41:44',0),(46,53,32,6,'You receive a message from a user. Click <a href=\'javascript:void(0)\' onclick=\'message_process(\"open_message\",\"24\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-16 20:41:59',0),(47,32,53,6,'You receive a message from a user. Click <a href=\'javascript:void(0)\' onclick=\'message_process(\"open_message\",\"25\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-16 20:42:36',0),(48,32,53,6,'You receive a message from a user. Click <a href=\'javascript:void(0)\' onclick=\'message_process(\"open_message\",\"26\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-16 20:46:15',0),(49,32,53,6,'You receive a message from a user. Click <a href=\'javascript:void(0)\' onclick=\'message_process(\"open_message\",\"27\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-16 20:46:58',0),(50,53,32,6,'You receive a message from a user. Click <a href=\'javascript:void(0)\' onclick=\'message_process(\"open_message\",\"28\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-16 20:47:19',0),(51,32,53,6,'You receive a message from a user. Click <a href=\'javascript:void(0)\' onclick=\'message_process(\"open_message\",\"29\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-16 20:47:37',1),(52,53,32,6,'You receive a message from a user. Click <a href=\'javascript:void(0)\' onclick=\'message_process(\"open_message\",\"30\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-16 21:22:19',0),(53,32,53,6,'You receive a message from a user. Click <a href=\'javascript:void(0)\' onclick=\'message_process(\"open_message\",\"31\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-16 21:22:40',1),(54,32,53,6,'You receive a message from a user. Click <a href=\'javascript:void(0)\' onclick=\'message_process(\"open_message\",\"32\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-16 21:23:26',1),(55,32,53,6,'You receive a message from a user. Click <a href=\'javascript:void(0)\' onclick=\'message_process(\"open_message\",\"33\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-16 21:23:54',1),(56,53,32,6,'You receive a message from a user. Click <a href=\'javascript:void(0)\' onclick=\'message_process(\"open_message\",\"34\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-16 21:25:37',0),(57,53,32,6,'You receive a message from a user. Click <a href=\'javascript:void(0)\' onclick=\'message_process(\"open_message\",\"35\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-16 21:30:19',0),(58,32,0,0,'A user commented on the plant you recently involved. Click <a href=\'javascript:void(0)\' onclick=\'comment_process(\"lists\",\"1\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-23 20:40:47',1),(59,32,0,0,'A user commented on the plant you recently involved. Click <a href=\'javascript:void(0)\' onclick=\'comment_process(\"lists\",\"1\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-23 20:40:47',1),(60,32,0,0,'A user commented on the plant you recently involved. Click <a href=\'javascript:void(0)\' onclick=\'comment_process(\"lists\",\"1\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-23 20:40:47',1),(61,32,42,0,'A user commented on the plant you recently involved. Click <a href=\'javascript:void(0)\' onclick=\'comment_process(\"lists\",\"1\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-23 20:40:47',1),(62,32,0,0,'A user commented on the plant you recently involved. Click <a href=\'javascript:void(0)\' onclick=\'comment_process(\"lists\",\"1\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-23 20:49:44',1),(63,32,0,0,'A user commented on the plant you recently involved. Click <a href=\'javascript:void(0)\' onclick=\'comment_process(\"lists\",\"1\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-23 20:49:44',1),(64,32,0,0,'A user commented on the plant you recently involved. Click <a href=\'javascript:void(0)\' onclick=\'comment_process(\"lists\",\"1\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-23 20:49:44',1),(65,32,42,0,'A user commented on the plant you recently involved. Click <a href=\'javascript:void(0)\' onclick=\'comment_process(\"lists\",\"1\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-23 20:49:44',1),(66,32,59,1,'A user submitted a plant request for you to check. Click <a href=\'profile.php?pass_param=request\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-23 21:06:21',1),(67,32,28,1,'A user submitted a plant request for a health professional to check.','2017-08-23 21:06:21',0),(68,32,0,0,'A user commented on the plant you recently involved. Click <a href=\'javascript:void(0)\' onclick=\'comment_process(\"lists\",\"1\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-23 21:18:49',1),(69,32,0,0,'A user commented on the plant you recently involved. Click <a href=\'javascript:void(0)\' onclick=\'comment_process(\"lists\",\"1\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-23 21:18:49',1),(70,32,0,0,'A user commented on the plant you recently involved. Click <a href=\'javascript:void(0)\' onclick=\'comment_process(\"lists\",\"1\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-23 21:18:49',1),(71,32,42,0,'A user commented on the plant you recently involved. Click <a href=\'javascript:void(0)\' onclick=\'comment_process(\"lists\",\"1\")\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-08-23 21:18:49',1),(72,53,32,1,'A user submitted a plant request for you to check. Click <a href=\'profile.php?pass_param=request\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-09-10 13:08:33',0),(73,53,28,1,'A user submitted a plant request for a health professional to check.','2017-09-10 13:08:33',1),(74,53,59,1,'A user submitted a plant request for you to check. Click <a href=\'profile.php?pass_param=request\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-09-10 13:22:39',1),(75,53,28,1,'A user submitted a plant request for a health professional to check.','2017-09-10 13:22:39',1),(76,53,59,1,'A user submitted a plant request for you to check. Click <a href=\'profile.php?pass_param=request\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-09-10 13:23:24',1),(77,53,28,1,'A user submitted a plant request for a health professional to check.','2017-09-10 13:23:24',1),(78,53,59,1,'A user submitted a plant request for you to check. Click <a href=\'profile.php?pass_param=request\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-09-10 13:24:10',1),(79,53,28,1,'A user submitted a plant request for a health professional to check.','2017-09-10 13:24:10',1),(80,32,28,1,'A user submitted a plant request for a health professional to check.','2017-09-10 13:36:46',1),(81,53,32,1,'A user submitted a plant request for you to check. Click <a href=\'profile.php?pass_param=request\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-09-10 13:37:35',0),(82,53,28,1,'A user submitted a plant request for a health professional to check.','2017-09-10 13:37:35',1),(83,32,53,1,'Health Professional approved the plant you submitted. Click <a href=\'profile.php?pass_param=submitted_request\' style=\'color:green;font-weight:bold;\'>here</a> to see','2017-09-10 13:37:54',1);

/*Table structure for table `plants` */

DROP TABLE IF EXISTS `plants`;

CREATE TABLE `plants` (
  `plant_id` int(11) NOT NULL AUTO_INCREMENT,
  `plant_name` varchar(30) NOT NULL,
  `description` varchar(500) NOT NULL,
  `plant_image` varchar(50) NOT NULL,
  `plant_specie` char(20) NOT NULL,
  `sci_name` varchar(30) NOT NULL,
  `common_name` varchar(30) NOT NULL,
  `admin_id` int(6) NOT NULL,
  `ailment_id` varchar(10) NOT NULL,
  `comment_counter` int(255) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status` int(255) DEFAULT '0',
  `approver` int(255) DEFAULT '0',
  `submitted_by` int(255) DEFAULT '0',
  PRIMARY KEY (`plant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;

/*Data for the table `plants` */

insert  into `plants`(`plant_id`,`plant_name`,`description`,`plant_image`,`plant_specie`,`sci_name`,`common_name`,`admin_id`,`ailment_id`,`comment_counter`,`update_time`,`status`,`approver`,`submitted_by`) values (1,'Abaniko','An abaniko (from the Spanish word abanico, meaning fan) is a type of hand-held fan that originated from the Philippines. The abaniko, together with the baro\'t saya, is a part of a lady\'s attire. Various ways of using and holding the abaniko may convey different meanings. For example, an open abaniko that covers the chest area is a sign of modesty while rapid fan movements express the lady\'s displeasure.[1]','abaniko.jpg','','(Asaasas)','',0,'',4,'2017-08-02 11:18:49',0,32,53),(2,'Acacia','Acacia, commonly known as the wattles or acacias, is a large genus of shrubs, lianas and trees in the subfamily Mimosoideae of the pea family Fabaceae','acacia.jpg','','','',0,'',0,'2017-08-02 11:18:49',0,32,53),(3,'Ampalaya','','','','','',0,'',0,'2017-08-02 11:18:49',0,32,53),(4,'Azucena','','','','','',0,'',0,'2017-08-02 11:18:49',0,32,53),(5,'Banaba','','','','','',0,'',0,'2017-08-02 11:18:49',0,32,53),(6,'Bawang','','','','','',0,'',0,'2017-08-02 11:18:49',0,32,53),(7,'Bayabas','','','','','',0,'',0,'2017-08-02 11:18:49',0,32,53),(8,'Caimito','is a tropical tree of the family Sapotaceae. It is native to the Greater Antilles and the West Indies. It has spread to the lowlands of Central America and is now is grown throughout the tropics, including Southeast Asia.[1] It grows rapidly and reaches 20 m in height.','caimito.jpg','','','',0,'',0,'2017-08-23 21:51:13',0,32,53),(9,'Chico','','','','','',0,'',0,'2017-08-23 21:51:06',0,32,53),(10,'Dalandan','','','','','',0,'',0,'2017-08-02 11:18:49',0,32,53),(11,'Dama de noche','','','','','',0,'',0,'2017-08-02 11:18:49',0,32,53),(12,'Eucalyptus','','','','','',0,'',0,'2017-08-02 11:18:49',0,32,53),(13,'Everlasting','','','','','',0,'',0,'2017-08-02 11:18:49',0,32,53),(14,'Garlic vine','','','','','',0,'',0,'2017-08-02 11:18:49',0,32,53),(15,'Gumamela','','','','','',0,'',0,'2017-08-02 11:18:49',0,32,53),(16,'Guyabano','','','','','',0,'',0,'2017-08-02 11:18:49',0,32,53),(17,'Hagonoi','','','','','',0,'',0,'2017-08-02 11:18:49',0,32,53),(18,'Hydrangea','','','','','',0,'',0,'2017-08-02 11:18:49',0,32,53),(19,'Iba','','','','','',0,'',0,'2017-08-02 11:18:49',0,32,53),(20,'Ipil-ipil','','','','','',0,'',0,'2017-08-02 11:18:49',0,32,53),(21,'Kalachuchi','as','','','','',0,'',0,'2017-08-02 11:18:49',0,32,53),(22,'Labanos','','','','','',0,'',0,'2017-08-02 11:18:49',0,32,53),(23,'Lagundi','','','','','',0,'',0,'2017-08-02 11:18:49',0,32,53),(25,'Mais','','','','','',0,'',0,'2017-08-02 11:18:49',0,32,53),(26,'Mahogany','','','','','',0,'',0,'2017-08-02 11:18:49',0,32,53),(27,'Tsaang gubat','dfdf','','','','',0,'',0,'2017-08-02 11:18:49',0,32,53),(82,'Avocado','Avocados are commercially valuable and are cultivated in tropical and Mediterranean climates throughout the world.[3] They have a green-skinned, fleshy body that may be pear-shaped, egg-shaped, or spherical','10754.jpg','','abokado','abokado',0,'',3,'2017-08-02 11:18:49',0,32,53),(83,'Coconut','The coconut tree (Cocos nucifera) is a member of the family Arecaceae (palm family) and the only species of the genus Cocos. The term coconut can refer to the whole coconut palm or the seed, or the fruit, which, botanically, is a drupe, not a nut.','26714.jpg','','Cocos nucifera','Lubi or Cocunot',0,'',0,'2017-08-02 11:18:49',0,32,53),(84,'BAYABAS','DAhon sa bayabas','20327.jpg','','Bayabas','Bayabas',0,'',0,'2017-08-02 11:18:49',0,32,53),(86,'Kamunggay','hahahha','5410.png','','k','k',0,'',0,'2017-08-02 11:18:49',0,32,53),(87,'Dragon Fruit','Dragon fruit is a vining, terrestrial or epiphytic cactus with succulent three-winged, green stems, reaching up to 20 feet long. Wings are up to 50 millimeters wide with modulate margins, spine are 1 millimeter long. Plant may climb trees via aerial roots. Flowers are fragrant, white, up to 35 centimeters long, blooming at night. Fruit is round, red, pink or yellow, with prominent scales.','1830.jpg','','Belle of the night (Engl.)','Cereus undatus Pfeiff',0,'',0,'2017-08-02 11:18:49',0,32,53),(88,'Alovera','Aloe vera is a plant species of the genus Aloe. It grows wild in tropical climates around the world and is cultivated for agricultural and medicinal uses. Aloe is also used for decorative purposes and grows successfully indoors as a potted plant','2699.jpg','','Aloe','Aloe vera',0,'',0,'2017-08-02 11:18:49',0,32,53),(89,'arc','arc','170728061431_arc.PNG','','arc','arc',0,'',0,'2017-08-02 10:08:09',0,32,53),(90,'ahoi','ahoi','','','ahoi','ahoi',0,'',0,'2017-08-02 10:08:09',0,32,53),(91,'hydra','hydra','170801085606_Hydrangeas.jpg','','hydra','hydra',0,'',0,'2017-08-02 10:08:09',0,32,53),(92,'Chrysanthemum','Chrysanthemum','170802040316_Chrysanthemum.jpg','','Chrysanthemum','Chrysanthemum',0,'',0,'2017-08-02 10:08:09',0,32,53),(93,'Jellyfish','Jellyfish','170802041217_Jellyfish.jpg','','Jellyfish','Jellyfish',0,'',0,'2017-08-02 10:15:07',0,32,53),(94,'Devil\'s Plant','Devil','16171.jpg','','Dev','IL',0,'',0,'2017-08-06 11:17:37',0,0,0),(100,'PinaCOlada','haha','170910073646_slow.PNG','','haha','haha',0,'',0,'0000-00-00 00:00:00',0,32,32),(101,'myrequest','myrequest','170910073735_arc.PNG','','myrequest','myrequest',0,'',0,'2017-09-10 13:37:54',0,32,53);

/*Table structure for table `posts` */

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id_p` int(11) NOT NULL,
  `status` text NOT NULL,
  `status_image` varchar(255) NOT NULL,
  `status_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `posts` */

insert  into `posts`(`post_id`,`user_id_p`,`status`,`status_image`,`status_time`) values (1,1,'hellow','NULL','2017-07-16 03:24:34'),(2,1,'kumusta','NULL','2017-07-16 03:24:40');

/*Table structure for table `product` */

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(1000) NOT NULL,
  `product_desc` varchar(1000) NOT NULL,
  `user_id` int(255) NOT NULL,
  `product_image` varchar(1000) NOT NULL,
  `plant_name` varchar(1000) NOT NULL,
  `status` int(255) DEFAULT '1' COMMENT '0 - Approve | 1 - Pending',
  `related_plant` int(255) DEFAULT '0',
  `approved_by` int(255) DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `product` */

insert  into `product`(`product_id`,`product_name`,`product_desc`,`user_id`,`product_image`,`plant_name`,`status`,`related_plant`,`approved_by`) values (1,'Strawhat Abaniko','Smooth',32,'170805034658_images.png','',0,1,28),(2,'Abaniko Shampoo','',32,'Bayabas.jpg','Abaniko',0,1,28),(7,'Arc','Arc',32,'170805030641_arc.PNG','',0,94,28),(8,'Arc','Arc',32,'170812092455_arc.PNG','',1,88,28);

/*Table structure for table `reviews` */

DROP TABLE IF EXISTS `reviews`;

CREATE TABLE `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reviewer` int(255) DEFAULT NULL,
  `professional` int(255) DEFAULT NULL,
  `content` varchar(1000) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `reviews` */

insert  into `reviews`(`id`,`reviewer`,`professional`,`content`,`created_at`) values (1,53,32,'Wai klaro ni sya mo advice','2017-08-01 15:18:45'),(5,53,32,'wai au','2017-08-01 21:15:53'),(6,53,32,'che','2017-08-01 21:56:57');

/*Table structure for table `statistics` */

DROP TABLE IF EXISTS `statistics`;

CREATE TABLE `statistics` (
  `ailement_id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `age` int(11) NOT NULL,
  PRIMARY KEY (`ailement_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `statistics` */

insert  into `statistics`(`ailement_id`,`user`,`age`) values (1,45,10),(2,34,10),(3,67,23);

/*Table structure for table `status` */

DROP TABLE IF EXISTS `status`;

CREATE TABLE `status` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `status` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `status` */

insert  into `status`(`id`,`status`,`date`) values (1,'Vote in the Google India Impact Challenge for the 4 NGOs that you think are changing the world. Each of the 4 NGOs that win will receive a Global Impact Award worth approximately $500K USD, as well as support from Google to make their vision a reality. Visit g.co/indiachallenge to vote now.','2013-10-21 04:18:38');

/*Table structure for table `surveys` */

DROP TABLE IF EXISTS `surveys`;

CREATE TABLE `surveys` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `respondent` text,
  `age` int(255) DEFAULT '1',
  `gender` varchar(1000) DEFAULT NULL,
  `ailments` varchar(1000) DEFAULT NULL,
  `type_used` varchar(1000) DEFAULT NULL,
  `name_of_medicine` varchar(1000) DEFAULT NULL,
  `plant_id` int(255) DEFAULT NULL,
  `satisfaction_rate` varchar(1000) DEFAULT NULL,
  `survey_type` int(255) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

/*Data for the table `surveys` */

insert  into `surveys`(`id`,`respondent`,`age`,`gender`,`ailments`,`type_used`,`name_of_medicine`,`plant_id`,`satisfaction_rate`,`survey_type`) values (1,NULL,19,'Female','Arthritis','Herbal Medicinal Plants','Herba Buena',NULL,'e',0),(2,NULL,37,'Female','Asthma','Alternative Medicine','Xanthone Juice',NULL,'e',0),(3,NULL,56,'Female','Asthma','Herbal Medicinal Plants','Euphorbia hirta',NULL,'e',0),(4,NULL,11,'Female','Boils','Herbal Medicinal Plants','Katakataka',NULL,'e',0),(5,NULL,38,'Female','Boils','Herbal Medicinal Plants','Katakataka',NULL,'vg',0),(6,NULL,7,'Female','Burn','Herbal Medicinal Plants','Aloevera',NULL,'e',0),(7,NULL,17,'Female','Burn','Herbal Medicinal Plants','Aloevera',NULL,'vg',0),(8,NULL,16,'Female','Chicken pox','Herbal Medicinal Plants','Balimbing',NULL,'vg',0),(9,NULL,29,'Female','Constipation','Alternative Medicine','MX3 Tea',NULL,'vg',0),(10,NULL,7,'Female','Contact Dermatitis\r\n','Herbal Medicinal Plants\r\n','Guava Leaves\r\n',NULL,'vg',0),(11,NULL,22,'Female','Cough\r\n\r\n','Herbal Medicinal Plants\r\n','Kalamansi\r\n',NULL,'vg',0),(12,NULL,6,'Female','Cough\r\n\r\n','Herbal Medicinal Plants\r\n','Lagundi',NULL,'e',0),(13,NULL,8,'Female','Cough\r\n\r\n','Herbal Medicinal Plants\r\n','Lagundi',NULL,'e',0),(14,NULL,20,'Female','Cough\r\n\r\n','Herbal Medicinal Plants\r\n','Lagundi',NULL,'e',0),(15,NULL,22,'Female','Cough\r\n\r\n','Herbal Medicinal Plants\r\n','Lagundi',NULL,'vg',0),(16,NULL,23,'Female','Cough\r\n\r\n','Herbal Medicinal Plants\r\n','Lagundi',NULL,'vg',0),(17,NULL,24,'Female','Cough\r\n\r\n','Herbal Medicinal Plants\r\n','Lagundi',NULL,'vg',0),(18,NULL,27,'Female','Cough\r\n\r\n','Herbal Medicinal Plants\r\n','Lagundi',NULL,'vg',0),(19,NULL,35,'Female','Cough\r\n\r\n','Herbal Medicinal Plants\r\n','Lagundi',NULL,'vg',0),(20,NULL,40,'Female','Cough\r\n\r\n','Herbal Medicinal Plants\r\n','Lagundi',NULL,'g',0),(21,NULL,57,'Female','Diabetes\r\n','Alternative Medicine\r\n','Ampalaya Plus\r\n',NULL,'vg',0),(22,NULL,72,'Female','Diabetes\r\n','Alternative Medicine\r\n','Ampalaya Plus\r\n',NULL,'vg',0),(23,NULL,73,'Female','Diabetes\r\n','Alternative Medicine\r\n','Ampalaya Plus\r\n',NULL,'vg',0),(24,NULL,22,'Female','Fever\r\n','Herbal Medicinal Plants\r\n','Lagundi\r\n',NULL,'g',0),(25,NULL,7,'Female','Hard Cough\r\n','Herbal Medicinal Plants\r\n','Lagundi\r\n',NULL,'vg',0),(26,NULL,14,'Male','Hyperacidity\r\n','Herbal Medicinal Plants\r\n','Malunggay Leaves\r\n',NULL,'e',0),(27,NULL,35,'Male','Hyperacidity\r\n','Herbal Medicinal Plants\r\n','Malunggay Leaves\r\n',NULL,'vg',0),(28,NULL,72,'Female','Hypertension\r\n\r\n','Herbal Medicinal Plants\r\n','Lemon Grass\r\n',NULL,'e',0),(29,NULL,39,'Male','Hypertension\r\n\r\n','Herbal Medicinal Plants\r\n','Garlic\r\n',NULL,'vg',0),(30,NULL,56,'Male','Hypertension\r\n\r\n','Herbal Medicinal Plants\r\n','Garlic\r\n',NULL,'vg',0),(31,NULL,58,'Male','Hypertension\r\n\r\n','Herbal Medicinal Plants\r\n','Garlic\r\n',NULL,'vg',0),(32,NULL,68,'Male','Hypertension\r\n\r\n','Herbal Medicinal Plants\r\n','Garlic\r\n',NULL,'vg',0),(33,NULL,38,'Male','Kidney infection\r\n\r\n','Alternative Medicine\r\n','Sambong Capsule\r\n',NULL,'vg',0),(34,NULL,27,'Male','Kidney infection\r\n\r\n','Herbal Medicinal Plants\r\n','Sambong Leaves\r\n',NULL,'e',0),(35,NULL,38,'Male','Kidney infection\r\n\r\n','Herbal Medicinal Plants\r\n','Sambong Leaves\r\n',NULL,'vg',0),(36,NULL,16,'Male','Measles\r\n','Herbal Medicinal Plants\r\n','Lagundi\r\n',NULL,'e',0),(37,NULL,18,'Male','Mouth sores\r\n','Herbal Medicinal Plants\r\n','Guava Leaves\r\n',NULL,'e',0),(38,NULL,19,'Male','Mumps\r\n','Herbal Medicinal Plants\r\n','Katuray\r\n',NULL,'e',0),(39,NULL,46,'Male','Poor eyesight\r\n','Alternative Medicine\r\n','Lutein\r\n',NULL,'vg',0),(40,NULL,25,'Male','Runny nose\r\n','Alternative Medicine\r\n','MX3 Capsule\r\n',NULL,'e',0),(41,NULL,21,'Male','Sinusitis\r\n','Herbal Medicinal Plants\r\n','Mayana\r\n',NULL,'g',0),(42,NULL,15,'Male','Stomach ache\r\n\r\n','Herbal Medicinal Plants\r\n','Ginger\r\n',NULL,'e',0),(43,NULL,1,'Male','Stomach ache\r\n\r\n','Alternative Medicine\r\n','Power Cell\r\n',NULL,'e',0),(44,NULL,36,'Male','Tonsillitis\r\n','Herbal Medicinal Plants\r\n','Ginger\r\n',NULL,'vg',0),(45,NULL,21,'Male','Tooth ache\r\n\r\n','Herbal Medicinal Plants\r\n','Avocado\r\n',NULL,'e',0),(46,NULL,26,'Male','Tooth ache\r\n\r\n','Herbal Medicinal Plants\r\n','Garlic\r\n',NULL,'e',0),(47,NULL,32,'Male','Tooth ache\r\n\r\n','Herbal Medicinal Plants\r\n','Garlic\r\n',NULL,'e',0),(48,NULL,39,'Male','Weak Immune System\r\n','Alternative Medicine\r\n','MX3 Capsule\r\n',NULL,'vg',0),(49,NULL,68,'Male','Hypertension\r\n\r\n','Herbal Medicinal Plants\r\n','Garlic\r\n',NULL,'vg',0),(50,NULL,20,'Male','Asthma\r\n\r\n','Herbal Medicinal Plants\r\n','Euphorbia hirta\r\n',NULL,'e',0);

/*Table structure for table `tbl_comment` */

DROP TABLE IF EXISTS `tbl_comment`;

CREATE TABLE `tbl_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `url` varchar(225) NOT NULL,
  `message` text NOT NULL,
  `date_posted` varchar(225) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_comment` */

insert  into `tbl_comment`(`id`,`name`,`email`,`url`,`message`,`date_posted`) values (2,'as','as@yahoo.com','asa','asa','July 15, 2017'),(3,'fg','fg@yahoo.com','fg','fg','July 15, 2017'),(4,'Renato','renatofernandez919@gmail.com','sd','sdsdssdds','July 15, 2017'),(5,'Renato','er@yahoo.com','er','ere','July 15, 2017');

/*Table structure for table `tutorials` */

DROP TABLE IF EXISTS `tutorials`;

CREATE TABLE `tutorials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(1000) DEFAULT NULL,
  `content` varchar(1000) DEFAULT NULL,
  `video` varchar(1000) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `tutorials` */

insert  into `tutorials`(`id`,`title`,`content`,`video`,`created_at`,`created_by`) values (1,'Ailments','Just watch and Learn','zRGL1nvqCAk','2017-08-06 12:34:02',32),(2,'Herbs for Life: LAGUNDI (Cough)','Lagundi preparation as cough medicine','tLmo5LTO670','2017-08-06 12:35:46',32);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(40) NOT NULL,
  `last_name` varchar(55) NOT NULL,
  `profile_image` varchar(255) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`user_id`,`username`,`password`,`first_name`,`last_name`,`profile_image`) values (1,'aizaz.dinho','098f6bcd4621d373cade4e832627b4f6','aizaz','dinho','images/profile.jpg'),(2,'renato','12345678','renz54','Fernandez','');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
