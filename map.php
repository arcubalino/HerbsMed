<?php
        require("db_credentials.php");
        $locations=array();
        
        $uname=$db_user;
        $pass=$db_password;
        $servername=$db_host;
        $dbname=$db_db;
        $db=new mysqli($servername,$uname,$pass,$dbname);
        $query =  $db->query("SELECT markers.*, plants.* from markers,plants where plants.plant_name=markers.name AND plants.plant_name= '".$_GET['query']."' ");
        
        while( $row = $query->fetch_assoc() ){
            $name = $row['name'];
            $longitude = $row['lng'];                              
            $latitude = $row['lat'];
            $link=$row['link'];
            /* Each row is added as a new array */
            $locations[]=array( 'name'=>$name, 'lat'=>$latitude, 'lng'=>$longitude, 'lnk'=>$link );
        }
       
    ?>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCygzQFxhWZtlHLKePLpAQ0swOB7-LKVJ4"></script> 
    <script type="text/javascript">
    var map;
    var Markers = {};
    var infowindow;
    var locations = [
        <?php for($i=0;$i<sizeof($locations);$i++){ $j=$i+1;?>
        [
            'AMC Service',
            '<p><a href="<?php echo $locations[0]['lnk'];?>">Visit this Place</a></p>',
            <?php echo $locations[$i]['lat'];?>,
            <?php echo $locations[$i]['lng'];?>,
            0
        ]<?php if($j!=sizeof($locations))echo ","; }?>
    ];
    var origin = new google.maps.LatLng(locations[0][2], locations[0][3]);
    function initialize() {
      var mapOptions = {
        zoom: 10,
        center: origin
      };
      map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        infowindow = new google.maps.InfoWindow();
        for(i=0; i<locations.length; i++) {
            var position = new google.maps.LatLng(locations[i][2], locations[i][3]);
            var marker = new google.maps.Marker({
                position: position,
                map: map,
            });
            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locations[i][1]);
                    infowindow.setOptions({maxWidth: 200});
                    infowindow.open(map, marker);
                }
            }) (marker, i));
            Markers[locations[i][4]] = marker;
        }
        locate(0);
    }
    function locate(marker_id) {
        var myMarker = Markers[marker_id];
        var markerPosition = myMarker.getPosition();
        map.setCenter(markerPosition);
        google.maps.event.trigger(myMarker, 'click');
    }
    google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    <div id="map-canvas" style="width:100%;height:400px;">
	 </div>
	 <br><br>