<?php
  require("db_credentials.php");
  include('session.php');
  include('head.php');
  include('navigation.php');
 ?>
 <br><br><br>
 <?php
  
    mysql_connect($db_host, $db_user, $db_password) or die("Error connecting to database: ".mysql_error());
    mysql_select_db($db_db) or die(mysql_error());
    
?>
<html>
<head>
    <title>Search results</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="css/style5.css"/>
	<link href="css/templatemo_style.css" rel="stylesheet" type="text/css" />	 
	<script src="js/jquery-2.1.1.min.js"></script>
    
    <script src="js/tinymce.min.js"></script>
    <script>  tinymce.init({  selector: 'textarea',  // change this value according to your HTML
     branding: false, menubar: false
     });
     </script>

	
</head>
<style> 
	input[type=text] {
    width: 800px;
    box-sizing: border-box;
    border: 2px solid #ccc;
    border-radius: 4px;
    font-size: 16px;
	color: black;
    background-color: white;
    padding: 12px 20px 12px 40px;
    

}

#plant-list{  
	position: fixed;
	background-color: #F8F8FF;
	color: black;
	text-align: left;
	left:250px;
    right:299px;
	
	
}

</style>
<body>
<script>

$(document).ready(function(){
	$("#search-box").keyup(function(){
		$.ajax({
		type: "POST",
		url: "readhome.php",
		data:'keyword='+$(this).val(),
		beforeSend: function(){
			$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 820px");
		},
		success: function(data){
			$("#suggesstion-box1").show();
			$("#suggesstion-box1").html(data);
			$("#search-box").css("background","#FFF");
		}
		});
	});
});

function selectPlant(val) {
$("#search-box").val(val);
$("#suggesstion-box1").hide();
}


function myMap() {
var mapProp= {
    center:new google.maps.LatLng(51.508742,-0.120850),
    zoom:5,
};
var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
}

</script>	


<?php
    $query = $_GET['query']; 
    // gets value sent over search form
     
    $min_length = 3;
    // you can set minimum length of the query if you want
     
    if(strlen($query) >= $min_length){ // if query length is more or equal minimum length then
         
        $query = htmlspecialchars($query); 
        // changes characters used in html to their equivalents, for example: < to &gt;
         
        $query = mysql_real_escape_string($query);
        // makes sure nobody uses SQL injection
         
        $raw_results = mysql_query("SELECT * FROM plants
            WHERE (`plant_name` LIKE '%".$query."%') OR (`plant_name` LIKE '%".$query."%')") or die(mysql_error());       
       ?>
        
<div id="doc">
  <div id="hd"">
    <div id="header">
	<h1>
	<form method="GET" action="search.php">
			<input type="text"  placeholder="Search.." autocomplete="off" name="query" id="search-box" required/>	    
			<button type="submit" id="myBtn" class="btn"><span class="search-icon"></span></button>			
			<div style="text-align:left;" id="suggesstion-box1"></div>			
	</form>
	</h1>	
	</div>
  </div>
  <br>
  <div id="bd">
    <div id="yui-main">
      <div class="yui-b">
        <div class="yui-ge">
		<?php
        if(mysql_num_rows($raw_results) > 0){ // if one or more rows are returned do following
             
            while($results = mysql_fetch_array($raw_results)){
            // $results = mysql_fetch_array($raw_results) puts data from database into array, while it's valid it does the loop
           ?>
		   
<div class="yui-u first">
            
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Description</a></li>
    <li><a data-toggle="tab" href="#menu1">Usage</a></li>
    <li><a data-toggle="tab" href="#menu2">Common Name</a></li>
    <li><a data-toggle="tab" href="#menu3">Comments</a></li>
  </ul>
 	
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <h3><?php echo $results['plant_name']; ?></h3>
	  <h5><i><b><?php echo $results['sci_name']; ?></h5></b></i>
      <p><?php echo $results['description']; ?></p>
    </div>
    <div id="menu1" class="tab-pane fade">
      <h3>Menu 1</h3>
      <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    </div>
    <div id="menu2" class="tab-pane fade">
      <h3>Menu 2</h3>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
    </div>
    <div id="menu3" class="tab-pane fade">
    <i class="fa fa-info-circle"></i> <label>Comments</label>
    <?php include('comments.php'); ?>
    </select>
    </div>
  </div>          
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
       <div class="yui-u">

	<img src="plant/<?php echo $results['plant_image'];?>" style="width:240px;height:295px;"></div>
          </div>
            
		<?php			          
            }
             
			}
			else{ // if there is no matching rows do following
            echo "No results";
			}
         
			}
			else{ // if query length is less than minimum
			echo "Minimum length is ".$min_length;
			}
			?>
         
        </div>
      </div>
    </div>
	<br><br>
    <div class="yui-b">
      <div id="secondary"><h3>Places Usually Found</h3>
	  <?php require_once('map.php') ?>
	 </div>
    </div>
  <br><br><br><br><br><br><br>
  <div class="yui-b">
<h3>Related Products</h3>
  <script type="text/javascript" src="js/jquery.min.js"></script>

  <script type="text/javascript">
 
ddsmoothmenu.init({
	mainmenuid: "templatemo_menu", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

</script> 

<!-- Load the CloudCarousel JavaScript file -->

 
<script type="text/javascript">
$(document).ready(function(){
						   
	// This initialises carousels on the container elements specified, in this case, carousel1.
	$("#carousel1").CloudCarousel(		
		{			
			reflHeight: 40,
			reflGap: 2,
			titleBox: $('#da-vinci-title'),
			altBox: $('#da-vinci-alt'),
			buttonLeft: $('#slider-left-but'),
			buttonRight: $('#slider-right-but'),
			yRadius: 30,
			xPos: 480,
			yPos: 32,
			speed:0.15,
			autoRotate: "yes",
			autoRotateDelay: 1500
		}
	);
});
 
</script>
<script type="text/JavaScript" src="js/cloud-carousel.1.0.5.js"></script>
<?php
include('dbConfig.php');

$query2 =  $db->query("SELECT product.*, plants.* from product,plants where plants.plant_name=product.plant_name AND plants.plant_name= '".$_GET['query']."' ");
 ?>       
        
			
   



	<div id="templatemo_header_wrapper">
	<h1><a href="#"></a></h1></div>
     <div id="templatemo_menu" class="ddsmoothmenu">
        <ul> </ul>
            
    </div> <!-- end of templatemo_menu -->
   
<!-- END of templatemo_header_wrapper -->

<div id="templatemo_slider">
	<!-- This is the container for the carousel. -->
    <div id = "carousel1" style="width:960px; height:280px;background:none;overflow:scroll; margin-top: 20px">            
        <!-- All images with class of "cloudcarousel" will be turned into carousel items -->
        <!-- You can place links around these images -->
		<?php	
		while( $row = $query2->fetch_assoc() ){
          ?>  		
        <a rel="lightbox"><img class="cloudcarousel" src="images/slider/<?php echo $row['product_image'];?> " alt="" height="180" width="240"/></a>		
		<?php                    
        }
     ?> 
  											     
    </div>

  <!-- Define left and right buttons. -->
    <center>
    <input id="slider-left-but" type="button" value="" />
    <input id="slider-right-but" type="button" value="" />
    </center>

</div>
</div>	
</div>
 </div>
 	
	<!-- bootstrap -->
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<!-- main js -->
	<script src="assets/js/main.js"></script>
 
	
	
	
</body>
</html>

