<?php
  require_once('head.php');
  include('navigation.php');
 
  
 ?>

 <!DOCTYPE HTML>
	
<html>
	<head>
		<title>About us</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="css/aboutus.css" />
		
	</head>
	<body>

		<!-- Header -->
			<section id="header" class="dark">
				<header>

				</header>
				<footer>
					<a href="#first" class="button scrolly">OUR TEAM</a>
				</footer>
			</section>

		<!-- First -->
			<section id="first" class="main">
				<header>
					<div class="container">
						<h2>Team Herbsmed</h2>
					</div>
				</header>
				<div class="content dark style1 featured">
					<div class="container">
					</div>
				</div>
				<section id="two" class="wrapper">
				<div class="inner alt">
					<section class="spotlight">
						<div class="image"><img src="images/renz.jpg" alt="" /></div>
						<div class="content">
							<h3>Renato Fernandez</h3>
							<p>Morbi mattis ornare ornare. Duis quam turpis, gravida at leo elementum elit fusce accumsan dui libero, quis vehicula lectus ultricies eu. In convallis amet leo non sapien iaculis efficitur consequat lorem ipsum.</p>
						</div>
					</section>
					<section class="spotlight">
						<div class="image"><img src="images/jonahty.jpg" alt="" /></div>
						<div class="content">
							<h3>Jonahty Alivio</h3>
							<p>Morbi mattis ornare ornare. Duis quam turpis, gravida at leo elementum elit fusce accumsan dui libero, quis vehicula lectus ultricies eu. In convallis amet leo non sapien iaculis efficitur consequat lorem ipsum.</p>
						</div>
					</section>
					<section class="spotlight">
						<div class="image"><img src="images/christian.jpg" alt="" /></div>
						<div class="content">
							<h3>Christian Earl Oppus</h3>
							<p>Morbi mattis ornare ornare. Duis quam turpis, gravida at leo elementum elit fusce accumsan dui libero, quis vehicula lectus ultricies eu. In convallis amet leo non sapien iaculis efficitur consequat lorem ipsum.</p>
						</div>
					</section>
					<section class="spotlight">
						<div class="image"><img src="images/ofelia.jpg" alt="" /></div>
						<div class="content">
							<h3>Ofelia Tindugan</h3>
							<p>Morbi mattis ornare ornare. Duis quam turpis, gravida at leo elementum elit fusce accumsan dui libero, quis vehicula lectus ultricies eu. In convallis amet leo non sapien iaculis efficitur consequat lorem ipsum.</p>
						</div>
					</section>
					<section class="spotlight">
						<div class="image"><img src="images/arjun.jpg" alt="" /></div>
						<div class="content">
							<h3>Arjun Amoloria</h3>
							<p>Morbi mattis ornare ornare. Duis quam turpis, gravida at leo elementum elit fusce accumsan dui libero, quis vehicula lectus ultricies eu. In convallis amet leo non sapien iaculis efficitur consequat lorem ipsum.</p>
						</div>
					</section>
			</section>

 <script src="js/jquery-2.1.1.min.js"></script>
 <script src="assets/bootstrap/js/bootstrap.min.js"></script>
	</body>
</html>