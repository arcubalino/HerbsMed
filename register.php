<?php
require('bundle.php');

if(!empty($_POST["submit-register"])) {
	// $filename=$_FILES['userFile']['name'];
	// $target_Path = "images/profile";
	// $target_Path = $target_Path.basename( $_FILES['userFile']['name'] );
	// move_uploaded_file( $_FILES['userFile']['tmp_name'], $target_Path );
	

	/* Form Required Field Validation */
	foreach($_POST as $key=>$value) {
		if(empty($_POST[$key])) {
		$error_message = "All Fields are required";
		break;
		}
	}
	/* Password Matching Validation */
	
	if($_POST['password'] != $_POST['confirm_password']){ 
	$error_message = 'The Password should be the same<br>'; 
	}

	/* Email Validation */
	if(!isset($error_message)) {
		if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
		$error_message = "Invalid Email Address";
		}
	}

	/* Validation to check if gender is selected */
	if(!isset($error_message)) {
	if(!isset($_POST["gender"])) {
	$error_message = " Please Select a Gender";
	}
	}
	
	//if
if(!isset($error_message)){
require('dbConfig.php');
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$username=$_POST['username'];
$handler = $dbh->prepare("SELECT mem_username FROM member WHERE mem_username = :username");
$handler->bindParam(':username', $username);
$handler->execute();

if($handler->rowCount() > 0){	
     echo'<script type="text/javascript">alert("Username ' . $username . ' already registered");</script>';
  }
else{
		$mem_type = $_POST["type"];
		if($mem_type == "health"){
			$mem_location = "healthprof";
			$mem_msg = "Account is pending for approval!";
			$mem_stat = 2;
		}else{
			$mem_msg = "You have registered successfully!";
			$mem_location = "profile";
			$mem_stat = 1;
		}

		$session_id = date('ymdhis');
		$file=$_FILES['userFile']['tmp_name'];	
		$name=$_FILES['userFile']['name'];
		$split_point = '.';
		$stringpos = strrpos($name, $split_point, -1);
		$finalstr = substr($name,0,$stringpos);
		$FinalName="".$session_id."_".$finalstr."";

		$image= addslashes(@file_get_contents($_FILES['userFile']['tmp_name']));
		$image_name= addslashes($_FILES['userFile']['name']);
		$image_size= @getimagesize($_FILES['userFile']['tmp_name']);
		$splitName = explode(".", $image_name); //split the file name by the dot
		$fileExt = end($splitName); //get the file extension
		$newFileName  = ucwords($FinalName.'.'.$fileExt); //join file name and ext.
		move_uploaded_file($_FILES["userFile"]["tmp_name"],"images/".$mem_location."/".$newFileName);

		$avatar_location=$newFileName;	

	   require_once("dbcontroller.php");
		$db_handle = new DBController();
		$query = "INSERT INTO member (mem_username,mem_pass,mem_fname,mem_lname,mem_add,mem_contact,mem_email,mem_gender,mem_type, mem_image,status) VALUES
		('" . $_POST["username"] . "', '" . md5($_POST["password"]) . "', '" . $_POST["fname"] . "', '" . $_POST["lname"] . "', '" . $_POST["address"] . "', '". $_POST["contact"] ."', '". $_POST["email"] ."', '". $_POST["gender"] ."', '". $mem_type . "','".$avatar_location."',$mem_stat)";
		 $result = $db_handle->insertQuery($query);
		$error_message = "";
		$success_message = $mem_msg;	
		unset($_POST);	
}	
}
}
	
?>
<html>
<head>

<title>Register</title>
<link rel="shortcut icon" href="assets/img/logo2.png">
		<link rel="stylesheet" type="text/css" href="css/style2.css" />
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/user-styles.css">
	<!-- 	<script src="js/jquery.min.js"></script> -->
		<script src="js/bootstrap.min.js"></script>
		<script src="js/script.js"></script>
</head>
	<body>
<div class="container">
          
            <header>
                <h1>"Our PRIORITY is your SAFETY" <span>HERBSMED</span></h1>
				
            </header>
            
            
                         
        <div class="login-sample">
          <form action="register.php" method="post" enctype="multipart/form-data">
              <h3><span class="glyphicon glyphicon-user"></span> SIGN UP</h3>
			   
			    <?php if(!empty($success_message)) { ?>	
					<div class="success-message"><?php if(isset($success_message)) echo $success_message; ?></div>
					<?php } ?>
					<?php if(!empty($error_message)) { ?>	
					<div class="error-message"><?php if(isset($error_message)) echo $error_message; ?></div>
					<?php } ?>

              <div class="row">
              <div class="col-sm-7">
                 <div class="form-group">
                  <input type="text" class="form-control" name="username" onkeyup="register_process('check_username',this)" placeholder="Username"value="<?php if(isset($_POST['username'])) echo $_POST['username']; ?>"></td>
                  	<div class="alert alert-danger" id="username_error" style="display:none;">
					  <strong>Error!</strong> username exist.
					</div>
                </div>
                <div class="form-group">
                  <input type="password" class="form-control" name="password" id="password" placeholder="Password" value="<?php if(isset($_POST['password'])) echo $_POST['password']; ?>"></td>
                </div>
                <div class="form-group">
                  <input type="password" id="confirm_password" class="form-control" name="confirm_password" onkeyup="register_process('check_confirm_pass',this)" placeholder="Confirm Password"  value="<?php if(isset($_POST['confirm_password'])) echo $_POST['confirm_password']; ?>"></td> 
                  	<div class="alert alert-danger" id="pass_error" style="display:none;">
					  <strong>Error!</strong> password doesn't match.
					</div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="email" onkeyup="register_process('check_email',this)" placeholder="Email" value="<?php if(isset($_POST['email'])) echo $_POST['email']; ?>"></td>
                  	<div class="alert alert-danger" id="email_error" style="display:none;">
					  <strong>Error!</strong> email already exist.
					</div>
                </div>
                <div class="form-group">
               <input type="radio" name="gender" value="Male" >Male
			   <input type="radio" name="gender" value="Female">Female
                  
				</div>
			   <div class="form-group">
			   <input type="radio" name="type" value="ordinary"/> Ordinary
			   <input type="radio" name="type" value="health"/>Health Enthusiast
			   </div>
              </div>
               <div class="col-sm-5">
                <div class="form-group">
                  <input type="text" class="form-control" name="fname" placeholder="Firstname" >
                  </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="lname" placeholder="Lastname"  >
                </div>
                 <div class="form-group">
                  <input type="text" class="form-control" name="address" placeholder="Address" >
                </div>
<!--                 <div class="form-group">
                  <input type="text" class="form-control" name="age" placeholder="Age" >
                </div> -->
                <div class="form-group">
                  <input type="number" class="form-control" name="contact" placeholder="Contact Number">
                </div>
				<div class="form-group">
				   <input type="file" name="userFile" id="userFile"/>
				</div> 

              </div>

              </div>
				
                  <div class="form-group">
                    </br><input type="submit" class="btn btn-warning center-block" name="submit-register" id="submit_reg"><br>
                    <p class="text-center">Already have an account? <a href="login_form2.php">Login</a></p>
                  </div>

          </form>
        </div>
      </div>
   </head>
</body>	  
</html>
   
	  
   